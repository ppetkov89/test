export * from "../../original/js/components/password-visibility";

// soon to be depricated.
import { PasswordVisibility } from "../../original/js/components/password-visibility";

const PasswordVisibilityTrigger = el => {
  new PasswordVisibility(el);
};

window.orbit = window.orbit || {};
window.orbit.passwordVisibility = PasswordVisibilityTrigger;

export default PasswordVisibility;
