export * from "../../original/js/components/password-validator";

// soon to be depricated.
import { PasswordValidator } from "../../original/js/components/password-validator";

const PasswordValidatorTrigger = el => {
  new PasswordValidator(el);
};

window.orbit = window.orbit || {};
window.orbit.passwordValidator = PasswordValidatorTrigger;

export default PasswordValidator;
