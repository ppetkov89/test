import Cookies from "js-cookie";
import ElementHelpers from "../../original/js/components/element-helpers";

/**
 * Declare constants
 */
const attributeBase = "data-bluex-favorite";

export class Favorites {
  constructor(element) {
    this.element = element;
    // Icon is filled.
    this.active = this.element.classList.contains(this.classes.active);
    this.jobId = this.element.getAttribute(this.attributes.jobId);
    // Job fields.
    this.payload = JSON.parse(this.element.getAttribute(this.attributes.payload));

    this.init();
  }

  get url() {
    return window.location.origin + "/api/my-randstad/user/saved-jobs";
  }

  /**
   * Declare attribute constants
   */
  get attributes() {
    return {
      jobId: `${attributeBase}-job-id`,
      payload: `${attributeBase}-payload`
    };
  }

  /**
   * Declare classes
   */
  get classes() {
    return {
      active: "icon__toggler--active",
    };
  }
  


  /**
   * Declare classes
   */
  get cookie() {
    return 'favJobs'
  }

  init() {
    if (!this.jobId || !this.payload) {
      console.error("Favorites: Missing jobId or payload");

      return false;
    }

    this.element.addEventListener("click", () => {
      if (this.active) {
        if (this.isAuth()) {
          this.removeWithApi();
        } else {
          this.removeWithLocalStorage();
          this.toggleIcon(false);
          this.dispatchEvent("");
        }
      } else {
        if (this.isAuth()) {
          this.addWithApi();
        } else {
          this.addWithLocalStorage();
          this.toggleIcon(true);
        }
      }
    });
  }

  addWithApi() {
    const payload = this.payload;
    if (payload["userId"]) delete payload["userId"];
    if (payload["created_timestamp"]) delete payload["created_timestamp"];
    if (payload["jobId"]) delete payload["jobId"];

    fetch(this.url + "/" + this.jobId, {
      method: "PUT",
      body: JSON.stringify(payload),
      headers: {
        "Content-Type": "application/json"
      }
    }).then(data => {
      if (data.error) {
        console.log(data.error);
      }
      if (data.status === 204) {
        this.toggleIcon(true);
      }
    });
  }

  removeWithApi() {
    fetch(this.url + "/" + this.jobId, {
      method: "DELETE"
    }).then(data => {
      if (data.error) {
        console.log(data.error);
      }
      if (data.status === 204) {
        this.toggleIcon(false);
      }
    });
  }

  addWithLocalStorage() {
    const jobs = JSON.parse(localStorage.getItem(this.cookie)) || {};
    jobs[this.jobId] = this.payload;

    localStorage.setItem(this.cookie, JSON.stringify(jobs));

    const searchCookie = Cookies.get(this.cookie);
    if (searchCookie) {
      const parsedCookie = JSON.parse(searchCookie);
      parsedCookie.push(this.jobId);

      Cookies.set(this.cookie, JSON.stringify(parsedCookie), { path: "/" });
    }
  }

  removeWithLocalStorage() {
    const jobs = JSON.parse(localStorage.getItem(this.cookie)) || {};
    delete jobs[this.jobId];

    localStorage.setItem(this.cookie, JSON.stringify(jobs));
    const searchCookie = Cookies.get(this.cookie);
    if (searchCookie) {
      const parsedCookie = JSON.parse(searchCookie);
      const newJobs = parsedCookie.filter(i => i !== this.jobId);

      Cookies.set(this.cookie, JSON.stringify(newJobs), { path: "/" });
    }
  }

  /**
   * Check if user is logged,
   */
  isAuth() {
    return window && window.myRandClient ? window.myRandClient.isLoggedIn() : false;
  }

  /**
   * Change icon state.
   */
  toggleIcon(active) {
    this.active = active;
    if (active) {
      this.element.classList.add(this.classes.active);
    } else {
      this.element.classList.remove(this.classes.active);
    }
  }

  /**
   * Dispatch event
   */
  dispatchEvent(e) {
    const event = ElementHelpers.createEvent(e);
    window.dispatchEvent(event);
    this.element.dispatchEvent(event);
  }

  /**
   * Get selector
   */
  static getSelector() {
    return `[${attributeBase}]`;
  }
}
