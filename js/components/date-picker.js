export * from '../overrides/date-picker';

// soon to be deprecated.
import { DatePicker } from '../overrides/date-picker';

const datePickerTrigger = el => {
  new DatePicker(el);
};

window.orbit = window.orbit || {};
window.orbit.datePicker = datePickerTrigger;