export * from "../overrides/auto-suggest";

// Soon to be depricated.
import { AutoSuggest as OverrideAutoSuggest } from "../overrides/auto-suggest"

const autoSuggestTrigger = el => {
  new OverrideAutoSuggest(el);
};

window.orbit = window.orbit || {};
window.orbit.autoSuggest = autoSuggestTrigger;

export default OverrideAutoSuggest;
