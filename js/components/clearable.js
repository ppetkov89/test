export * from '../../original/js/components/clearable';

// soon to be depricated.
import { Clearable } from '../../original/js/components/clearable';

const clearableTrigger = el => {
  new Clearable(el);
};

window.orbit = window.orbit || {};
window.orbit.clearable = clearableTrigger;

export default Clearable;

