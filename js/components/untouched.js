export * from "../../original/js/components/untouched";

// soon to be depricated.
import { Untouched } from "../../original/js/components/untouched";

const untouchedTrigger = el => {
  new Untouched(el);
}

window.orbit = window.orbit || {}
window.orbit.untouched = untouchedTrigger;

export default Untouched
