// TODO: Deprecated file. Soon to be removed.

/**
 * import dependencies
 */
import ElementHelpers from '../../original/js/components/element-helpers';


export class TabBar {
  /**
   * insert fade elements at start and end of list items
   */
  setFadeElements() {
    // IE conflict with foreach NodeList.
    [].forEach.call(this.items, (item, index, array) => {
      if (index === 1) {
        item.insertAdjacentHTML('beforebegin', this.fadeLeft);
      }
      if (index === array.length - 1) {
        item.insertAdjacentHTML('afterend', this.fadeRight);
      }
    });
  }

  /**
   * add event listeners to all Tab-bar elements
   */
  addEventListeners() {
    this.iconLeft = ElementHelpers.getElementByAttributeWithinElement(this.element, this.attributes.iconLeft);
    this.iconRight = ElementHelpers.getElementByAttributeWithinElement(this.element, this.attributes.iconRight);

    // scroll left on click
    this.iconLeft.addEventListener('click', () => {
      this.element.scrollBy({ left: -(this.element.clientWidth / 2), behavior: 'smooth' });
    });
    // scroll right on click
    this.iconRight.addEventListener('click', () => {
      this.element.scrollBy({ left: this.element.clientWidth / 2, behavior: 'smooth' });
    });
    //reset position fadeElement/chevron on scroll
    this.element.addEventListener('scroll', () => {
      this.toggleFadeElements();
    });
    //reset position fadeElement/chevron on resize
    window.addEventListener('resize', () => {
      this.toggleFadeElements();
      this.scrollToActive();
    });
    //loop items and add click event to call setActiveClass function
    // IE conflict with foreach NodeList.
    [].forEach.call(this.items, item => {
      item.addEventListener('click', event => {
        event.preventDefault;
        this.setActiveClass(event);
      });
    });
  }

  /**
   * Set or remove active state on list item
   */
  setActiveClass(event) {
    // IE conflict with foreach NodeList.
    [].forEach.call(this.items, item => {
      item.classList.remove(this.classes.active);
    });
    event.currentTarget.classList.add(this.classes.active);
    this.scrollToActive();
  }
}
