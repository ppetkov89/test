
/**
 * sticky.js
 * toggles class on scroll
 *
 */

import ElementHelpers from '../../original/js/components/element-helpers';

/**
 * Declare constants
 */
const attributeBase = 'data-rs-sticky';

export class Sticky {

  constructor(element) {
    this.element = element;
    this.stick = false;
    this.animation = false;

    this.elementHeight = this.element.offsetHeight;
    this.handleScroll = this.handleScroll.bind(this);
    this.stickPoint = this.getOffset(this.element);
    this.stickAnimate = element.getAttribute(this.attributes.animate)
    this.stickyGhostBox = document.createElement("div");
    this.stickyGhostBox.classList.add('sticky-ghost-box');
    this.element.parentNode.insertBefore(this.stickyGhostBox, this.element);

    if (this.stickAnimate) {
      this.stickPoint = this.stickPoint + this.elementHeight;
      this.element.classList.add(this.classes.animate);
    }

    if (element.getAttribute(this.attributes.background)) {
      const colorClass = `bg-brand--${element.getAttribute(this.attributes.background)}`;
      this.stickyGhostBox.classList.add(colorClass);
    }

    this.handleScroll();
    this.addEventHandlers();
  }

  /**
   * Declare classes
   */
  get classes() {
    return {
      sticky: 'sticky',
      animate: 'sticky--animate'
    }
  }

  /**
	 * Declare attribute constants
	 */
	get attributes() {
		return {
      animate: `${attributeBase}-animate`,
      background: `${attributeBase}-bg`,
    };
	}

  /**
   * Add event handlers
   */
  addEventHandlers() {
    if ("wheel" in window) {
      document.addEventListener('wheel', this.handleScroll, true);
    } else {
      document.addEventListener('scroll', this.handleScroll, true);
    }
  }

  /**
   * Handle scroll event.
   */
  handleScroll(e) {
    let offset = 0

    if (this.stickAnimate) {
      offset = 35

      if(!ElementHelpers.isDesktop()) {
        offset = 30;
      }
    }

    if (this.stickPoint - offset < window.pageYOffset && !this.stick) {
      this.stick = true;

      this.element.classList.add(this.classes.sticky);
      this.stickyGhostBox.style.height = this.elementHeight + 'px';
    }
    if (this.stickPoint - offset > window.pageYOffset && this.stick) {
      this.stick = false;
      this.element.classList.remove(this.classes.sticky);
      this.element.classList.remove("drop-down");
      this.element.classList.remove("drop-up");
      this.stickyGhostBox.style.height = '0px';
    }

    if (this.stickPoint < window.pageYOffset && !this.animation) {
      this.animation = true;
      this.element.classList.add("drop-down");
      this.element.classList.remove("drop-up");
    }

    if (this.stickPoint > window.pageYOffset && this.animation) {
      this.animation = false;
      this.element.classList.remove("drop-down");
      this.element.classList.add("drop-up");
    }

  }

  getOffset(element) {
    const self = this;

    if(!element) return 0;
    return self.getOffset(element.offsetParent) +  element.offsetTop
  }

  /**
   * Get selector
   */
  static getSelector() {
    return `[${attributeBase}]`;
  }
}



