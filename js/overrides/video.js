/**
 * video.js
 *
 */

import ElementHelpers from '../../original/js/components/element-helpers';
import { Video as OrbitBase } from "../../original/js/components/video";

/**
* Declare constants
*/
const attributeBase = 'data-rs-video';

export class Video extends OrbitBase {

  constructor(element) {
    super(element)
    this.trigger = ElementHelpers.getElementsByAttribute(this.attributes.trigger, this.element.getAttribute(attributeBase));

    this.addAdditionalEventHandlers();
  }

	/**
	* Declare attribute constants
	*/
  get attributes() {
    return {
      overlay: `${attributeBase}-overlay`,
      modal: `${attributeBase}-modal`,
      iframe: `${attributeBase}-iframe`,
      trigger: `${attributeBase}-trigger`
    }
  }

	/**
	 * Add event handlers
	 */
  addAdditionalEventHandlers() {
    if (this.trigger.length > 0) {
      [...this.trigger].map(trigger => {
        trigger.addEventListener('click', (event) => {
          event.preventDefault();
          this.scrollPosition = window.pageYOffset;
          this.toggleModal();
        });
      })
    }
  }

  /**
   * issue in jira - RBD-3863
   * Overriding toggleModal() to remove body margin addition.
   * Toggle modal
   */
  toggleModal() {
    if (this.modal.classList.contains(this.classes.modalActive)) {
      // stop playing video
      this.iframe.setAttribute('src', this.iframe.getAttribute('src'));

      // remove width of scrollbar to body and modal
      document.body.style.paddingRight = '0px';
      this.modal.style.paddingRight = '';
    }
    else {
      //
      // document.body.style.marginTop = `${(this.bodyMarginTop - this.scrollPosition)}px`;
      // set padding right to width of scrollbar
      document.body.style.paddingRight = `${this.scrollbarWidth}px`;
      this.modal.style.paddingRight = `${this.scrollbarWidth}px`;
    }
    document.querySelector('html').classList.toggle(this.classes.modalOpen);
    this.modal.classList.toggle(this.classes.modalActive);
  }

  /**
   * issue in jira - RBD-3863
   * Overriding addEventHandlers() to remove body margin addition.
   * Add event handlers
   */
  addEventHandlers() {
    this.element.addEventListener('click', (event) => {
      event.preventDefault();
      this.scrollPosition = window.pageYOffset;
      this.toggleModal();
    });
    this.modal.addEventListener('click', (event) => {
      event.preventDefault();
      this.toggleModal();
      // document.body.style.marginTop = `${this.bodyMarginTop}px`;
      window.scrollTo(0, this.scrollPosition);
    });
  }
}
