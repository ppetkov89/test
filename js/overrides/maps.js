/**
 * maps.js
 * load google maps with markers and related popups
 */
// TODO: remove commented code after while.
// import { PopupWrapper } from "../../original/js/components/maps/popup-wrapper";
import { Maps as OrbitBase } from "../../original/js/components/maps";
import { GoogleMapsApi } from "../../original/js/components/maps/google-maps-api";

/**
 * Declare constants
 */
// let Popup = null;

export class Maps extends OrbitBase {
  /**
   * Get API key. (This is default UK key)
   */
  get apiKey() {
    let apiKey = this.element.getAttribute('data-rs-maps-api-key');
    if (apiKey) {
      return apiKey;
    } else {
      return 'AIzaSyBp0Micp_k8bk_JkaFSwKOE87xqfAqR7RA';
    }
  }

  /**
   * Load google maps API
   */
  initializeMap() {
    // create map, controls and markers after the API is ready
    const gmapApi = new GoogleMapsApi(this.apiKey);
    gmapApi.load().then(() => {
      this.createMap();
      this.customZoomControl(this.mapPlaceholderElement);
      this.handleMarkers();
    }).catch((reason) => {
      console.error(reason);
    });
  }

  /**
   * Create Popup class if not already exists
   * always AFTER the map is created
	 *
	 * DON'T REMOVE FAIL TO INITIALIZE.
   */
  // createPopup() {
  //   if (Popup === null) {
  //     console.log(PopupWrapper.popupClass);
  //     Popup = PopupWrapper.popupClass;
  //   }
  // }
}
