/**
 * carousel.js
 * Script to create carousel components
 */

import ElementHelpers from '../../original/js/components/element-helpers';
import { Carousel as OrbitBase } from "../../original/js/components/carousel";
import $ from 'jquery';
import 'slick-carousel';

/**
* Declare constants
*/
const attributeBase = 'data-rs-carousel';

export class Carousel extends OrbitBase {

	constructor(element) {
		super(element)

		this.buttonId = element.getAttribute(this.attributes.buttonId)
	}

	/**
	* Initialize carousel
	*/
	initializeCarousel() {

		let defaultSlickOptions = {
			arrows: false,
			infinite: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			mobileFirst: true,
			swipeToSlide: true,
			responsive: [{
				breakpoint: ElementHelpers.maxWidthMobileViewports(),
				settings: {
					appendArrows: $(this.element).parents(`[${this.attributes.wrapper}]`),
					arrows: true,
					draggable: false,
					slidesToShow: 3,
					slidesToScroll: 1
				}
			}]
		};

		if (!this.element.classList.contains(this.classes.slickInitialized)) {
			this.removeHiddenClass();
			if (this.variant === this.attributes.blogOverviewCarousel) {
				defaultSlickOptions.responsive[0].settings.slidesToShow = 2;
				$(this.element).slick(defaultSlickOptions);
			}
			// Added application process init - ppetkov
			else if (this.variant === this.attributes.applicationProcess) {
				defaultSlickOptions.responsive[0].settings.slidesToShow = 1;
				$(this.element).slick(defaultSlickOptions);
			}
			else if (this.variant === this.attributes.blogOverviewGrid) {
				defaultSlickOptions.responsive[0].settings = 'unslick';
				$(this.element).slick(defaultSlickOptions);
			}
			else if (this.variant === this.attributes.blogOverviewTopic) {
				$(this.element).slick(defaultSlickOptions);
			}
			else if (this.variant === this.attributes.photo) {
				this.positionDots();

				defaultSlickOptions.dots = true;
				defaultSlickOptions.appendDots = this.sliderDots;
				defaultSlickOptions.responsive = [{
					breakpoint: ElementHelpers.maxWidthMobileViewports(),
					settings: {
						swipe: false
					}
				}];
				$(this.element).slick(defaultSlickOptions);
        if (this.slides > 1) {
          const slickDots = ElementHelpers.getElementByClassWithinElement(this.sliderDots, this.classes.slickDots);
          slickDots.style.bottom = '0px';
        }
			}
			else if (this.variant === this.attributes.quote) {
				defaultSlickOptions.dots = true;
				defaultSlickOptions.responsive = [{
					breakpoint: ElementHelpers.maxWidthMobileViewports(),
					settings: {
						swipe: false
					}
				}];
				$(this.element).slick(defaultSlickOptions);
			}
			else if (this.variant === this.attributes.cards) {
				defaultSlickOptions.responsive[0].settings = 'unslick';
				$(this.element).slick(defaultSlickOptions);
			}
		}
		//if slider is carousel or topic, set arrow heights equal to images in slider on resize window
		if (this.variant === this.attributes.blogOverviewCarousel || this.variant === this.attributes.blogOverviewTopic || this.variant === this.attributes.applicationProcess) {
			const slickArrowNextButton = this.element.parentElement.querySelector(`.${this.classes.slickNext}.${this.classes.slickArrow}`);
			const slickArrowPrevButton = this.element.parentElement.querySelector(`.${this.classes.slickPrev}.${this.classes.slickArrow}`);

			const image = ElementHelpers.getElementByAttributeWithinElement(this.element, this.attributes.image);
			if (slickArrowNextButton !== null && slickArrowPrevButton !== null && image !== null) {
				// Add button id. - ppetkov
				if (this.buttonId) {
					slickArrowNextButton.id = `button-next-${this.buttonId}`;
					slickArrowPrevButton.id = `button-prev-${this.buttonId}`;
				}

				setTimeout(function () {
					slickArrowNextButton.style.height = `${image.offsetHeight}px`;
					slickArrowPrevButton.style.height = `${image.offsetHeight}px`;
				}, 100);
			}
		}
	}

	/**
	* Declare attribute constants
	*/
	get attributes() {
		return {
      image: `${attributeBase}-image`,
      wrapper: `${attributeBase}-wrapper`,
      dotsHeight: `${attributeBase}-dots-height`,
      slide: `${attributeBase}-slide`,
      blogOverviewCarousel: "blog-overview--carousel",
      blogOverviewGrid: "blog-overview--grid",
      blogOverviewTopic: "blog-overview--topic",
      photo: "photo",
      quote: "quote",
      applicationProcess: "application-process",
      buttonId: `${attributeBase}-button-id`,
      cards: "cards"
    };
	}

	/**
	 * Position dots appended to photo or quote slider
	 */
	positionDots() {
    this.sliderDots = this.element.nextElementSibling;
    setTimeout(() => {
      this.dotsHeight = parseInt(ElementHelpers.getElementByAttributeWithinElement(this.element, this.attributes.dotsHeight).offsetHeight);
      this.sliderDotsOffsetHeight = parseInt(this.sliderDots.offsetHeight);
      this.offsetTop = parseInt(this.element.offsetTop);
      this.element.height = $(this.element).find('.banner__media').height();
      this.sliderDots.style.top = `${this.element.height - this.sliderDotsOffsetHeight}px`;
    }, 150);
	}
}
