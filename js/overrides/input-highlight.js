/**
 * input-overlay.js
 * Adds overlay when element is focused.
 * The input element's attribute must be data-rs-input-highlight.
 * Also data-rs-input-highlight-wrapper attribute must be added to a parent element.
 * This way you can highlight only one element or a group of elements.
 */

import ElementHelpers from "../../original/js/components/element-helpers";

/**
 * Declare constants
 */

const attributeBase = 'data-rs-input-highlight';

export class InputHighlight {

  constructor(element) {
    this.element = element;
    this.overlayWrapper = element.closest(`[${this.attributes.overlayWrapper}]`);

    // Append overlay if there is parent wrapper.
    if (this.overlayWrapper) {
      this.bindOverlay();
      this.addEventHandlers();
    } else {
      console.log('There is no data-rs-input-highlight-wrapper found.')
    }
  }

  /**
	 * Declare key codes
	 */
	get keyCodes() {
		return {
			Tab: 'Tab'
		}
	}

  /**
   * Add event handlers
   */
  addEventHandlers() {
    // on input and focus, add overlay
    ['input', 'focus'].forEach(eventType => {
      this.element.addEventListener(eventType, () => {
          this.openOverlay();
      });
    });

    this.element.addEventListener('blur', (e) => {
      if (e.relatedTarget === null) {
        this.closeOverlay();
      }
    });

    // RBD-4542: Prevent inputhightlight to be active when value is selected from autosuggest.
    window.addEventListener('autoSuggestCloseDropdown', () => {
      this.closeOverlay();
    })

    // RBD-3494: Unable to close to overlay when tabbed wrong - UAT.
    this.element.addEventListener("keydown", keydownEvent => {
      if (ElementHelpers.getKeyCodeOnKeyDownEvent(keydownEvent) == this.keyCodes.Tab) {
        this.closeOverlay();
      }
    });
  }

  /**
   * Declare attributes
   */
  get attributes() {
    return {
      overlayWrapper: `${attributeBase}-wrapper`,
    };
  }

  /**
   * Declare classes
   */
  get classes() {
    return {
      inputOverlay: 'input-overlay',
      overlayWrapperActive: 'input-highlight-wrapper--active',
      modalOverlay: 'modal__overlay',
      modalOverlayActive: 'modal__overlay--active',
    }
  }

  /**
   * Bind overlay to DOM.
   */
  bindOverlay() {
    this.overlay = document.querySelector(`.${this.classes.inputOverlay}`);

    if (!this.overlay) {
      this.overlay = document.createElement("div");
      this.overlay.classList.add(this.classes.inputOverlay);
      this.overlay.classList.add(this.classes.modalOverlay);
      this.overlay.style.zIndex = "-1";
      this.overlayWrapper.appendChild(this.overlay);
    }
  }

  /**
   * Handle overlay open.
   */
  openOverlay() {
    if (!this.overlayWrapper) return;

    this.overlay.classList.add(this.classes.modalOverlayActive);
    this.overlayWrapper.classList.add(this.classes.overlayWrapperActive);
    this.overlayWrapper.style.zIndex = "11";
  }

  /**
   * Handle overlay close.
   */
  closeOverlay() {
    if (!this.overlayWrapper) return;

    this.overlay.classList.remove(this.classes.modalOverlayActive);
    this.overlayWrapper.classList.remove(this.classes.overlayWrapperActive);
    this.overlayWrapper.removeAttribute('style');
  }

  /**
   * Get selector
   */
  static getSelector() {
    return `[${attributeBase}]`;
  }
}
