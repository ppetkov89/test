import { DatePicker as OrbitBase } from '../../original/js/components/datepicker';
import ElementHelpers from '../../original/js/components/element-helpers';
import { German } from 'flatpickr/dist/l10n/de';
import { Dutch } from 'flatpickr/dist/l10n/nl';
import { French } from 'flatpickr/dist/l10n/fr';
import { Portuguese } from 'flatpickr/dist/l10n/pt';
import { Spanish } from 'flatpickr/dist/l10n/es';

/**
 * date-picker.js
 * set date picker on an input field
 */

export class DatePicker extends OrbitBase {
  constructor(element) {
    super(element)

    this.language = this.input.dataset.rsDatepickerLanguage;
    this.disableFutureDates = this.input.dataset.rsDatepickerDisableFutureDates;
    this.disablePastDates = this.input.dataset.rsDatepickerDisablePastDates;

    if (this.disableFutureDates === "true") {
      this.maxDate = 'today'
    }

    if (this.disablePastDates === "true") {
      this.minDate = 'today'
    }

    this.setExtendedLocalisation()
    
    // Initialize datepicker with new settings. DON`T REMOVE IT.
    const options = this.flatOptions()

		// Create flatpickr instance.
    this.input.flatpickr(
      options
    );
  }

  // Мръсен хак!!! Remove default date picker init. DON`T REMOVE IT.
  createFlatDatePicker() {
    // Keep it empty !!!
	}
  
  /**
   * Set language/localisation for instance
   */
  setExtendedLocalisation() {
    this.locale = {
      firstDayOfWeek: this.sundayCheck(),
      weekAbbreviation: '#',
      weekdays: {
        shorthand: this.shortWeeks.split(', ')
      },
      months: {
        shorthand: this.shortMonths.split(', '),
        longhand: this.longMonths.split(', ')
      }
    }

    switch (this.language) {
      case 'de':
        this.locale = Object.assign(this.locale, German);        
      this.locale = Object.assign(this.locale, German);
        this.locale = Object.assign(this.locale, German);        
        break;

      case 'nl':
        this.locale = Object.assign(this.locale, Dutch);        
      this.locale = Object.assign(this.locale, Dutch);
        this.locale = Object.assign(this.locale, Dutch);        
        break;

      case 'fr':
        this.locale = Object.assign(this.locale, French);        
      this.locale = Object.assign(this.locale, French);      
        this.locale = Object.assign(this.locale, French);        
        break;

      case 'pt':
        this.locale = Object.assign(this.locale, Portuguese);        
        break;

      case 'es':
        this.locale = Object.assign(this.locale, Spanish);        
        break;
    
      default:
        break;
    }    
	}

  //set the input.value on change
	flatOnChange(dateStr) {
		this.input.setAttribute('value', dateStr);
    
    const event = ElementHelpers.createEvent('onFlatDatePickerChange')
    window.dispatchEvent(event);
  }
  
  /**
	 * Open the datepicker instance
	 */
	openDatePicker(instance) {
		return (e) => {
      // RBR-430: Datepicker icon is not clickable.
      // Prevent bubling to trigger flatpicker click and hide datepicker.
			e.stopPropagation()
			instance.open();
		};
	}
}
