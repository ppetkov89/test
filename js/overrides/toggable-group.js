/**
 * toggable-group.js
 * toggles class  of group of items on click or keyup (spacebar)
 *
 */

// ---------------------------------------
// OVERRIDE THE WHOLE BECAUSE TAGRGET IS NOT EXPORT PROPERLY - ORBIT BUG!!!!

import ElementHelpers from "../../original/js/components/element-helpers"
import { Target } from "./target"

/**
 * Declare constants
 */
const attributeBase = "data-rs-toggable-group"

export class ToggableGroup {
	constructor(element) {
		this.element = element
		this.items = [...ElementHelpers.getElementsByAttributeWithinElement(this.element, this.attributes.item)]
		this.classToToggle = this.element.getAttribute(attributeBase)
		this.addEventHandlers()
	}

	/**
	 * Declare attribute constants
	 */
	get attributes() {
		return {
			item: `${attributeBase}-item`,
			active: "active",
		}
	}

	/**
	 * Add event handlers
	 */
	addEventHandlers() {
		this.items.forEach((item) => {
			item.addEventListener("click", (clickEvent) => {
				clickEvent.preventDefault()

				// get active toggable group item => remove attribute and class
				let activeItem = ElementHelpers.getElementByAttributeWithinElement(this.element, this.attributes.item, this.attributes.active)
				activeItem.setAttribute(this.attributes.item, "")
				activeItem.classList.remove(this.classToToggle)
				Target.toggle(activeItem, false)

				// set attribute and class to clicked item
				item.setAttribute(this.attributes.item, this.attributes.active)
				item.classList.add(this.classToToggle)
				Target.toggle(item, true)

				// RBD-3339.
				this.clearSiblingClass()

				if (clickEvent.target.nextSibling !== null && clickEvent.target.nextSibling.classList.contains("toggable-group__item")) {
					clickEvent.target.nextSibling.classList.add("toggable-group__item--next")
				}

				if (clickEvent.target.previousSibling !== null && clickEvent.target.previousSibling.classList.contains("toggable-group__item")) {
					clickEvent.target.previousSibling.classList.add("toggable-group__item--prev")
				}
			})
		})

		// get active toggable group item => remove attribute and class
		let activeItem = ElementHelpers.getElementByAttributeWithinElement(this.element, this.attributes.item, this.attributes.active)
		Target.toggle(activeItem, true)
	}

	clearSiblingClass() {
		this.items.forEach((item) => {
			item.classList.remove("toggable-group__item--prev")
			item.classList.remove("toggable-group__item--next")
		})
	}

	/**
	 * Get selector
	 */
	static getSelector() {
		return `[${attributeBase}]`
	}
}
