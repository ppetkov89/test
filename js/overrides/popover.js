import { Popover as OrbitBase } from "../../original/js/components/popover";
import ElementHelpers from "../../original/js/components/element-helpers";


export class Popover extends OrbitBase {

	constructor(element) {
		super(element)

		this.addAdditionalEventHandlers()
	}
	/**
   * Declare classes
   */
	get classes() {
		return {
			popoverActive: 'popover--active',
			modalOverlayActive: 'modal__overlay--active',
			triggerActive: 'trigger--active'
		}
	}

  /**
	 * Add event handlers
	 */

	addAdditionalEventHandlers() {

		if (this.trigger !== null) {
			this.trigger.addEventListener('click', e => {

				// Edit by ppetkov - RBD-3308: Prevent multiple active popovers.
				let activeTriggers = document.querySelectorAll(
          `[${this.attributes.trigger}].${this.classes.triggerActive}`
        );

				activeTriggers.forEach(el => {

					let activePopover = ElementHelpers.getElementByAttribute(
            "data-rs-popover",
            el.getAttribute(this.attributes.trigger)
					);
					activePopover.classList.remove(this.classes.popoverActive);
					el.classList.remove(this.classes.triggerActive);
				})

				// Edit by ppetkov - RBD-3181: toggle active class on trigger.
				if (this.overlay.classList.contains(this.classes.modalOverlayActive)) {
					this.trigger.classList.add(this.classes.triggerActive)
        } else {
					this.trigger.classList.remove(this.classes.triggerActive)
				}
			});
		}
		if (this.overlay !== null) {
			this.overlay.addEventListener('click', () => {

				// Edit by ppetkov - RBD-3181: toggle active class on trigger.
				this.trigger.classList.remove(this.classes.triggerActive)
			});
		}
	}
}
