/**
 * share-price.js
 * Wrap base SharePrice class with extended settings.
 */

import { SharePrice as OrbitBase } from "../../original/js/components/share-price"

/**
 * Declare global vars.
 */
const attributeBase = "data-rs-share-price"

export class SharePrice extends OrbitBase {
	constructor(element) {
		super(element)

		this.url = element.getAttribute(this.attributes.url)
		this.settings = JSON.parse(element.getAttribute(this.attributes.settings))
		this.symbol = this.settings.currency_symbol || "€"
		this.feed = this.getFeedUrl()
	}

	/**
	 * Declare attributes
	 */
	get attributes() {
		return {
			value: `${attributeBase}-value`,
			url: `${attributeBase}-url`,
			settings: `${attributeBase}-settings`
		}
	}

	/**
	 * render results to placeholder elements
	 */
	renderResult() {
		const changeValue = this.getResultValue("Change")
		// add class to container if change is up or down
		this.element.classList.add(OrbitBase.changeIsUp(changeValue) ? this.classes.change.up : this.classes.change.down)
		// render seperate items
		this.renderResultItem("last", this.getResultValue("Last", this.formatCurrency))
		const change = `${OrbitBase.formatChange(changeValue)} (${this.getResultValue("ChangePercent", OrbitBase.formatNumber)} %)`
		this.renderResultItem("change", change)
		this.renderResultItem("low", this.getResultValue("Low", this.formatCurrency))
		this.renderResultItem("high", this.getResultValue("High", this.formatCurrency))
		this.renderResultItem("symbol", this.getResultValue("Symbol"))
		this.renderResultItem("date", this.getResultValue("Date", OrbitBase.formatDate))
	}

	/**
	 * get feed url
	 * @return {string} url
	 */
	getFeedUrl() {
		if (!this.url) {
			console.error("Missing feed url!")
			return ""
		}

		const { company, currency } = this.settings

		return `${this.url}?companycode=${company}&currency=${currency || "eur"}`
	}

	/**
	 * format currency
	 * @param {string} value
	 */
	formatCurrency(value) {
		return `${this.symbol} ${SharePrice.formatNumber(value)}`
	}
}
