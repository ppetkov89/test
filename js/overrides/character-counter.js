import { CharacterCounter as OrbitBase } from "../../original/js/components/character-counter"

export class CharacterCounter extends OrbitBase {

	/**
	 * Set counter output
	 */
	setCounterOutput() {
		const labels = JSON.parse(this.element.getAttribute('data-rs-character-counter-labels'));

		if (this.counterOutput) {
      let {characters, charactersLeft} = labels || {};

			const characterCount = this.maxLength - this.element.value.length
			if (characterCount >= 0) {
				charactersLeft = charactersLeft.replace('@characters', characterCount);

				this.counterOutput.classList.remove(this.classes.textNegative)
				this.counterOutput.innerText = charactersLeft
			} else {
				this.counterOutput.classList.add(this.classes.textNegative)
				this.counterOutput.innerText = `${characterCount} ${characters}`
			}
		}
	}
}
