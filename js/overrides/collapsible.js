/**
 * collapsible.js
 * Script to expand and collapse elements
 */

import ElementHelpers from '../../original/js/components/element-helpers';
import { Collapsible as OrbitBase } from "../../original/js/components/collapsible";

export class Collapsible extends OrbitBase {

	handleExpandedElement() {
		// ADDED BY PPETKOV.
		const filter = ElementHelpers.getElementByAttribute('data-rs-filter');
		const contentHeight = this.content.scrollHeight;

		if (ElementHelpers.isDesktop()) {
			if (this.content.style.maxHeight !== `${contentHeight}px`) {
				this.expandContent(contentHeight);
				this.expandTrigger();
			}
			if (this.initialExpanded === false) {
				this.content.setAttribute(this.attributes.content, this.attributes.filter);
				this.element.setAttribute(this.attributes.expanded, this.states.expanded);
			}
		} else {
			// do not collapse on mobile devices if in filter.
			// EDITED BY PPETKOV.
			if (filter !== undefined && filter.contains(this.element)) {
				if (this.content.style.maxHeight !== `${contentHeight}px`) {
					this.expandContent(contentHeight);
					this.expandTrigger();
				}
			} else {
				this.collapseTrigger();
				this.collapseContent();
			}
		}
	}

	collapseTrigger() {
		const siblings = this.element.parentElement.querySelectorAll(
			`[${this.attributes.ariaExpanded}]:not(.collapsible__trigger)`
		);

		this.element.classList.remove(this.classes.collapsibleTriggerExpanded);
		this.element.setAttribute(this.attributes.ariaExpanded, 'false');
		this.element.removeAttribute(this.attributes.expanded);

		// Handle collapse in siblings.
		if (siblings) {
			siblings.forEach(el => {
				el.setAttribute(this.attributes.ariaExpanded, 'false');
			});
		}
	}

	/**
	* Handles element on expand - add class and set aria expanded attribute
	*/
	expandTrigger() {
		const siblings = this.element.parentElement.querySelectorAll(
			`[${this.attributes.ariaExpanded}]:not(.collapsible__trigger)`
		);

		this.element.classList.add(this.classes.collapsibleTriggerExpanded);
		this.element.setAttribute(this.attributes.ariaExpanded, 'true');

		// Handle collapse in siblings.
		if (siblings) {
			siblings.forEach(el => {
				el.setAttribute(this.attributes.ariaExpanded, 'true');
			});
		}
	}
}
