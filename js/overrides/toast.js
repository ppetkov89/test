/**
 * toast.js
 * show toast and handle click + fadeout
 *
 */
import { Toast as OrbitBase } from "../../original/js/components/toast";

/**
* Declare constants
*/
const attributeBase = 'data-rs-toast';

export class Toast extends OrbitBase {

	constructor(element) {
		super(element)
		this.anchor = element.getAttribute(this.attributes.anchor);
		this.addAdditionalEventHandlers()
	}

	/**
	* Declare attribute constants
	*/
	get attributes() {
		return {
			show: `${attributeBase}-show`,
			anchor: `${attributeBase}-anchor`,
		}
	}

	addAdditionalEventHandlers() {
		// Add anchor trigger.
		if (this.anchor) {
			const pageAnchor = this.getUrlParam('toast');

			if (pageAnchor && this.anchor == pageAnchor) {
				this.show();
				this.destroyCloseTimeout();
				this.createCloseTimeOut();
			}
		}
	}

	/**
	 * Add event handlers
	 */
	addEventHandlers() {
		// handle click trigger element.
		// Check if triggerElement fix.
		if (this.triggerElement) {
			this.triggerElement.addEventListener('click', (clickEvent) => {
				clickEvent.preventDefault();
				this.show();
				this.destroyCloseTimeout();
				this.createCloseTimeOut();
			});
		}

		this.element.addEventListener("mouseenter", () => {
			this.destroyCloseTimeout();
		});
		this.element.addEventListener("mouseleave", () => {
			this.closeTimeout = this.createCloseTimeOut();
		});
	}

	getUrlParam(param) {
		var params = window.location.href.split("#");
		var paramsMap = {};
		params.forEach(function (p) {
			var v = p.split("=");
			paramsMap[v[0]] = decodeURIComponent(v[1]);
		});

		return paramsMap[param] !== null ? paramsMap[param] : '';
	}
}
