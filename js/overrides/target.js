// ---------------------------------------
// OVERRIDE THE WHOLE BECAUSE TAGRGET IS NOT EXPORT PROPERLY - ORBIT BUG!!!!

let attributeBase = 'data-rs-toggable-target'

export class Target {

	constructor() { }

	static toggle(element, enable) {
		if (typeof element === 'undefined' || !element) {
			return;
		}
		let targetSelector = element.getAttribute(attributeBase);
		if (typeof targetSelector !== 'undefined' && targetSelector) {
			let targetElements = document.querySelectorAll(targetSelector);
			targetElements.forEach(enable === true
				? Target.enable
				: Target.disable);
		}
	}

	static enable(element) {
		element.removeAttribute('hidden');
	}

	static disable(element) {
		element.setAttribute('hidden', '');
	}
}
