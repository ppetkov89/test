/**
 * modal.js
 * Script to toggle modal
 */

import ElementHelpers from '../../original/js/components/element-helpers';
import { Modal as OrbitBase } from "../../original/js/components/modal";

/**
* Declare constants
*/
const attributeBase = 'data-rs-modal';

export class Modal extends OrbitBase {

	constructor(element) {
		super(element);

		this.openTriggers = ElementHelpers.getElementsByAttribute(this.attributes.trigger, this.element.getAttribute(attributeBase));

		this.isInit = element.getAttribute('data-rs-modal-init') || false;
		this.isInit === 'true' && this.openModal(false)
		this.addAdditionalHandlers();
	}

	/**
	 * Add additional event handlers
	 */
	addAdditionalHandlers() {
		let triggers = [...this.openTriggers];

		const currentTrigerIndex = triggers.indexOf(this.openTrigger);
		triggers = triggers.filter((el, key) => key !== currentTrigerIndex)

		triggers.map(el => {
			el.addEventListener("click", event => {
				event.preventDefault();
				this.scrollPosition = window.pageYOffset;
				this.toggleModal(!this.disableHistory);
			});
		});
	}

	/**
	 * Add event handlers
	 */
	addEventHandlers() {

		this.openTrigger && this.openTrigger.addEventListener('click', event => {
      event.preventDefault();
      this.scrollPosition = window.pageYOffset;
      this.toggleModal(!this.disableHistory);
    });
		// close/toggle modal on click close trigger
		this.closeTrigger.addEventListener('click', (event) => {
			event.preventDefault();
			this.toggleModal();
			// document.body.style.marginTop = `${this.bodyMarginTop}px`;
			// window.scrollTo(0, this.scrollPosition);
		});
		// if clicked outside modal/on overlay close/toggle modal
		this.element.addEventListener('mousedown', clickEvent => {
      // Edit by Nikola Atanasov
      // Modal shouldn't close, when click on dynamically created autocomplete.
      // Add check for two elements, that can be selected, for existing class.
      // Add the variable to if statement. TODO: better check for existing class.
      var targetAttr = clickEvent.target.hasAttribute('data-rs-auto-suggest-list-value');
      var parentTargetAttr = clickEvent.target.parentElement
        ? clickEvent.target.parentElement.hasAttribute('data-rs-auto-suggest-list-value')
        : false;
      var closeModal = !(targetAttr || parentTargetAttr);

      if (clickEvent.target !== this.dialog && !this.dialog.contains(clickEvent.target) && closeModal) {
        this.toggleModal();
        // document.body.style.marginTop = `${this.bodyMarginTop}px`;
        // window.scrollTo(0, this.scrollPosition);
      }
    });
		// on resize, handle dividers of modal
		window.addEventListener('resize', () => {
			this.handleDividers();
		});
		// on scroll, check position to set/unset dividers
		this.main.addEventListener('scroll', () => {
			this.checkPosition();
		}, { passive: true });
		// on popstate event (i.e. back button in browser), handle querystring parameters
		window.addEventListener('popstate', () => {
			this.handleQueryStringParameters(false);
		});
		// handle key down event - close modal if escape is pressed
		document.addEventListener('keydown', (keydownEvent) => {
			this.handleButtonKeys(keydownEvent);
		})
	}

	/**
	 * Close modal - closes modal, set styling and edit pushstate
	 * @param {boolean} changePushState - determines if history push state should
	 *   be adjusted
	 */
	closeModal(changePushState) {
		// remove width of scrollbar to body and modal
		// document.body.style.paddingRight = '0px';
		// this.element.style.paddingRight = '';

		// if changePushState is true, remove querystring parameters from url
		if (changePushState) {
			window.history.pushState({}, '', window.location.pathname);
		}
		document.querySelector('html').classList.remove(this.classes.modalOpen);
		this.element.setAttribute(this.attributes.hidden, '');
		this.element.classList.remove(this.classes.modalActive);
	}

	/**
	 * Open modal - opens modal, set styling and edit pushstate
	 * @param {boolean} changePushState - determines if history push state should
	 *   be adjusted
	 */
	openModal(changePushState) {
		// document.body.style.marginTop = `${(this.bodyMarginTop - this.scrollPosition)}px`;
		// document.body.style.paddingRight = `${this.scrollbarWidth}px`;
		// this.element.style.paddingRight = `${this.scrollbarWidth}px`;

		// if changePushState is true, add querystring parameters to url
		if (changePushState) {
			window.history.pushState({}, '', `${window.location.pathname}?${this.attributes.modalOpen}=${this.element.getAttribute(attributeBase)}`);
		}
		document.querySelector('html').classList.add(this.classes.modalOpen);
		this.element.removeAttribute(this.attributes.hidden);
		this.element.classList.add(this.classes.modalActive);
	}

	/**
	* Handle button keys - handle key presses escape
	*
	* @param {Event} event - event triggered
	*/
	handleButtonKeys(event) {
		if (event != null) {
			switch (ElementHelpers.getKeyCodeOnKeyDownEvent(event)) {
				case this.keyCodes.Escape:
					event.preventDefault();
					this.closeModal(true);
					// Edited by ppetkov.
					// document.body.style.marginTop = `${this.bodyMarginTop}px`;
					// window.scrollTo(0, this.scrollPosition);
					break;
			}
		}
	}
}
