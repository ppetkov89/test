/**
 * auto-suggest.js
 *
 */

import ElementHelpers from '../../original/js/components/element-helpers';
import { AutoSuggest as OrbitBase } from "../../original/js/components/auto-suggest";

/**
* Declare constants
*/

export class AutoSuggest extends OrbitBase {

	constructor(element) {
		super(element)

		this.submit = element.parentNode.querySelector('[type="submit"]')
		this.numericValueRegex = this.list.dataset.rsNumericValueRegex
		
	}

	/**
	 * Add event handlers
	 */
	addEventHandlers() {

		// This line should be in constructor, but addEventHandlers is loaded first, because of class extends.
		// RBD-9338: Allow auto-suggest to disable input event, because user is handling manually to load data async and cause conflict.
		this.allowInputEvent = this.input.getAttribute('data-rs-auto-suggest-allow-input-event') === ''
		const events = ['focus', 'keydown'];

		if (this.allowInputEvent) {
			events.push('input')
		}

		// on input and focus, create list if user input length > minimum length
		events.forEach(eventType => {
			this.element.addEventListener(eventType, this.onInput.bind(this), {
				capture: true
			});
		});
		document.addEventListener('click', this.clickOutside.bind(this));
	}

	onInput(event) {

		if (event.target.hasAttribute(this.attributes.input)) {
			if (event.type === 'input' || event.type === 'focus') {
				this.numberOfListItems = 0;
				// only process if input is equal to or longer then minimum length
				if (this.input.value.length >= this.minimumLength) {
					// if previous input length is less then minimum length OR if previous input does not contain current input -> reset the data source
					if (this.previousInput.length < this.minimumLength
						|| this.input.value.toLowerCase().indexOf(this.previousInput.toLowerCase()) === -1) {
						this.dataSource = this.getDataSource();
					}
					this.dataSource = this.getDataSource();
					this.createList();
				} else {

					// FFW - RBD-1967
					if (this.container.classList.contains(this.classes.autosuggestOpen)) {
						this.closeList();
					}
				}
				this.previousInput = this.input.value;
			} else if (event.type === 'keydown') {
				// if keydown is tab and item is preselected, select item and close list
				if (ElementHelpers.getKeyCodeOnKeyDownEvent(event) == this.keyCodes.Tab) {
					if (this.selectedIndex > -1) {
						this.setSelectedValue(this.selectedIndex);
					}
					this.closeList();
				}
				// if keydown is escape, close list and do not select item
				if (ElementHelpers.getKeyCodeOnKeyDownEvent(event) == this.keyCodes.Escape) {
					this.closeList();
				}
				this.handleButtonKeys(event);
				this.additionalHandleButtonKeys(event);
			}
		}
	}

	clickOutside(event) {
		// if clicked outside input field or list, close the list and do not select item
		if (!this.list.contains(event.target) && !this.input.contains(event.target)) {
			if (this.container.classList.contains(this.classes.autosuggestOpen)) {
				this.closeList();
			}
		}
	}

	/**
	 * Handle button keys - handle key presses down, up, enter, page down and page up
   *
   * @param {Event} event - event triggered
	 *
	 */
	additionalHandleButtonKeys(event) {
		if (event != null
			&& this.selectedIndex > -1) {
			switch (ElementHelpers.getKeyCodeOnKeyDownEvent(event)) {

				case this.keyCodes.Enter:
					event.preventDefault();
					this.closeList();
					this.input.blur();

					if (this.submit) {
						this.submit.focus();
					}
					break;
			}
		}
	}

	/**
	 * Close list - closes the list with suggestions and resets selected index and selected value
	 */
	closeList() {
		let event = new CustomEvent('autoSuggestCloseDropdown')
		window.dispatchEvent(event);

		this.container.classList.remove(this.classes.autosuggestOpen);
		this.list.innerHTML = '';

		this.selectedValue = '';
		this.selectedIndex = -1;
	}

 /**
	* Create list - get matched items from data source and put them in list with li elements
	*/
	createList() {
		let list = '';
		let listItemIndex = 0;
		// set result list equal to data source
		this.resultList = JSON.parse(JSON.stringify(this.dataSource));
		// --- FFW EDIT ---
			// This line should be in constructor, but addEventHandlers is loaded first, because of class extends.
			// RBR-452: Ignore default Orbit filter and display all returned data.
			this.useExactValues = this.list.dataset.rsUseExactValues === "true"
		// --- END EDIT ---
		this.dataSource.forEach((listItem) => {
			// check for each list item in the data source if it matches with input from user
			// --- FFW EDIT ---
			if (listItem.toLowerCase().indexOf(this.input.value.toLowerCase()) > -1 || this.useExactValues) {
			// --- END EDIT ---
				// if item matches, add mark to matched part to create highlighted text

				// --- FFW EDIT ---
				// RBD-8705: Escape special symbols.
					const regexSafeValue = this.input.value.replace(/[-\[\]/{}()*+?.\\^$|]/g, '\\$&')
					let regex = new RegExp(regexSafeValue, 'gi');
				// --- END EDIT ---
				let response = listItem.replace(regex, function (value) {
					return `<mark>${value}</mark>`;
				});
				// create li element for matched item
				let li = `<li class="${this.classes.autosuggestItem}" ${this.attributes.listValue} ${this.attributes.index}="${listItemIndex}" >${response}</li>`;

				// add li elements to list variable
				list = list + li;
				this.numberOfListItems++;
				listItemIndex++;
			}
			else {
				// if list item does not match input from user -> remove list item from result list
				let index = this.resultList.indexOf(listItem);
				if (index > -1) {
					this.resultList.splice(index, 1);
				}
			}
		})
		// set data source equal to result list
		this.dataSource = JSON.parse(JSON.stringify(this.resultList));
		// if number of list items > 0, bind result list
		if (this.numberOfListItems > 0) {
			this.timeout = setTimeout(() => this.bindResultList(list), this.threshold);
		}
		else {
			let noResults = `<li class="${this.classes.autosuggestNoResult}">${this.noResultMessage || 'no results'} </li>`;
			this.bindResultList(noResults);
		}
	}

	/**
	 * Set selected value
	 *
	 * @param {number} index - index of selected item
	 */
	setSelectedValue(index) {
		// get preselected item with index, set value to input field and close the list
		let preSelectedItem = ElementHelpers.getElementByAttribute(this.attributes.index, index);
		if (preSelectedItem != null) {
			let value = preSelectedItem.innerText

			if (this.numericValueRegex && Number.parseInt(this.input.value)) {
				const regex = new RegExp(this.numericValueRegex, 'g')
				const match = preSelectedItem.innerText.match(regex)
				if (match) {
					match.forEach((val) => {
						// Sanitize values.
						val = val.replace(/[()]/g, '');
						
						if (Number.parseInt(val)) {
							value = val
							return;
						}
					})
				}
			}

			// Trigger change event on value select to allow React to update state.
			let nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
			nativeInputValueSetter.call(this.input, value);
			let event = new CustomEvent('input', { bubbles: true});
			this.input.dispatchEvent(event);
			this.closeList();
		}
	}
}
