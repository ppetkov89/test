"use strict";

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (e, t) {
  for (var i in t) {
    e[i] = t[i];
  }
}(exports, function (e) {
  var t = {};function i(a) {
    if (t[a]) return t[a].exports;var o = t[a] = { i: a, l: !1, exports: {} };return e[a].call(o.exports, o, o.exports, i), o.l = !0, o.exports;
  }return i.m = e, i.c = t, i.d = function (e, t, a) {
    i.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: a });
  }, i.r = function (e) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(e, "__esModule", { value: !0 });
  }, i.t = function (e, t) {
    if (1 & t && (e = i(e)), 8 & t) return e;if (4 & t && "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e && e.__esModule) return e;var a = Object.create(null);if (i.r(a), Object.defineProperty(a, "default", { enumerable: !0, value: e }), 2 & t && "string" != typeof e) for (var o in e) {
      i.d(a, o, function (t) {
        return e[t];
      }.bind(null, o));
    }return a;
  }, i.n = function (e) {
    var t = e && e.__esModule ? function () {
      return e.default;
    } : function () {
      return e;
    };return i.d(t, "a", t), t;
  }, i.o = function (e, t) {
    return Object.prototype.hasOwnProperty.call(e, t);
  }, i.p = "", i(i.s = 254);
}({ 13: function _(e, t, i) {
    "use strict";
    i.r(t), i.d(t, "GoogleMapsApi", function () {
      return a;
    });
    var a = function () {
      function a(e) {
        _classCallCheck(this, a);

        this.apiKey = e;
      }

      _createClass(a, [{
        key: "load",
        value: function load() {
          var _this = this;

          return new Promise(function (e) {
            if (_this.resolve = e, void 0 === window.humanForward && (window.humanForward = {}), window.humanForward.GoogleMapsApi || (_this.callbackName = "humanForward.GoogleMapsApi.mapReady", window.humanForward.GoogleMapsApi = _this, window.humanForward.GoogleMapsApi.mapReady = _this.mapReady.bind(_this)), void 0 !== window.google && void 0 !== window.google.maps || void 0 === _this.callbackName) _this.resolve();else {
              _this.loaded = !0;var _e = document.createElement("script");_e.src = "//maps.googleapis.com/maps/api/js?key=" + _this.apiKey + "&callback=" + _this.callbackName, _e.async = !0, document.body.appendChild(_e);
            }
          });
        }
      }, {
        key: "mapReady",
        value: function mapReady() {
          this.extendApi(), this.resolve();
        }
      }, {
        key: "extendApi",
        value: function extendApi() {
          google.maps.Map.prototype.panToWithOffset = function (e, t, i) {
            var a = this,
                o = new google.maps.OverlayView();o.onAdd = function () {
              var o = this.getProjection(),
                  r = o.fromLatLngToContainerPixel(e);r.x = r.x + t, r.y = r.y + i, a.panTo(o.fromContainerPixelToLatLng(r));
            }, o.draw = function () {}, o.setMap(this);
          };
        }
      }]);

      return a;
    }();
  }, 254: function _(e, t, i) {
    e.exports = i(255);
  }, 255: function _(e, t, i) {
    "use strict";
    i.r(t);var a = i(79);var o = document.querySelectorAll(a.Maps.getSelector());for (var _e2 = 0; _e2 < o.length; _e2++) {
      new a.Maps(o[_e2]);
    }
  }, 28: function _(e, t, i) {
    "use strict";
    i.r(t), i.d(t, "MapStyle", function () {
      return a;
    });
    var a = function () {
      function a() {
        _classCallCheck(this, a);
      }

      _createClass(a, null, [{
        key: "custom",
        get: function get() {
          return [{ featureType: "administrative.land_parcel", stylers: [{ visibility: "off" }] }, { featureType: "administrative.locality", elementType: "labels", stylers: [{ visibility: "simplified" }] }, { featureType: "administrative.neighborhood", stylers: [{ visibility: "off" }] }, { featureType: "administrative.province", elementType: "labels", stylers: [{ visibility: "off" }] }, { featureType: "landscape.natural", elementType: "geometry.fill", stylers: [{ color: "#f7f5f0" }] }, { featureType: "poi", elementType: "labels.text", stylers: [{ visibility: "off" }] }, { featureType: "poi.business", stylers: [{ visibility: "off" }] }, { featureType: "road", elementType: "labels", stylers: [{ visibility: "off" }] }, { featureType: "road", elementType: "labels.icon", stylers: [{ visibility: "off" }] }, { featureType: "road.arterial", stylers: [{ visibility: "off" }] }, { featureType: "road.highway", elementType: "labels", stylers: [{ visibility: "off" }] }, { featureType: "road.local", stylers: [{ visibility: "off" }] }, { featureType: "transit", stylers: [{ visibility: "off" }] }, { featureType: "water", elementType: "labels.text", stylers: [{ visibility: "off" }] }];
        }
      }]);

      return a;
    }();
  }, 29: function _(e, t, i) {
    "use strict";
    i.r(t), i.d(t, "PopupWrapper", function () {
      return a;
    });
    var a = function () {
      function a() {
        _classCallCheck(this, a);
      }

      _createClass(a, null, [{
        key: "popupClass",
        get: function get() {
          function e(e, t, i) {
            this.position = e, this.content = t, this.marker = i, this.content.classList.add("popup-bubble"), this.containerDiv = document.createElement("div"), this.containerDiv.classList.add("popup-bubble__container"), this.containerDiv.appendChild(t), google.maps.OverlayView.preventMapHitsAndGesturesFrom(this.containerDiv);
          }return e.prototype = Object.create(google.maps.OverlayView.prototype), e.prototype.onAdd = function () {
            this.getPanes().floatPane.appendChild(this.containerDiv), this.show();
          }, e.prototype.onRemove = function () {
            this.containerDiv.parentElement && this.containerDiv.parentElement.removeChild(this.containerDiv);
          }, e.prototype.draw = function () {
            var e = this.getProjection().fromLatLngToDivPixel(this.position),
                t = Math.abs(e.x) < 4e3 && Math.abs(e.y) < 4e3 ? "block" : "none";"block" === t && (this.containerDiv.style.left = e.x + "px", this.containerDiv.style.top = e.y + "px"), this.containerDiv.style.display !== t && (this.containerDiv.style.display = t);
          }, e.prototype.height = function () {
            return getComputedStyle(this.content).getPropertyValue("height");
          }, e.prototype.hide = function () {
            var _this2 = this;

            this.content.addEventListener("transitionend", function () {
              getComputedStyle(_this2.content).getPropertyValue("opacity") <= .1 && _this2.setMap(null);
            }), this.content.classList.remove("active");
          }, e.prototype.show = function () {
            var _this3 = this;

            setTimeout(function () {
              _this3.content.classList.add("active"), setTimeout(function () {
                var e = parseInt(getComputedStyle(_this3.content).getPropertyValue("height"));if (!isNaN(e)) {
                  var _t = -1 * (e / 2 + _this3.marker.icon.size.height);_this3.marker.map.panToWithOffset(_this3.marker.getPosition(), 0, _t);
                }
              });
            }, 50);
          }, e;
        }
      }]);

      return a;
    }();
  }, 79: function _(e, t, i) {
    "use strict";
    i.r(t), i.d(t, "Maps", function () {
      return n;
    });var a = i(13),
        o = i(28),
        r = i(29);var s = null;
    var n = function () {
      function n(e) {
        _classCallCheck(this, n);

        this.map = null, this.element = e, this.markers = [], this.marker = null, this.popup = null, this.googleMapsKey = this.element.hasAttribute(this.attributes.googleMapsKey) && this.element.getAttribute(this.attributes.googleMapsKey), this.markerUrlPath = this.element.hasAttribute(this.attributes.markerUrl) ? this.element.getAttribute(this.attributes.markerUrl) : this.variables.defaultMarkerUrl, this.markerIsNotClickable = this.element.hasAttribute(this.attributes.markerIsNotClickable) || !1, this.mapPlaceholderElement = this.element.querySelector(this.attributes.mapsPlaceholderElement), this.mapElement = this.mapPlaceholderElement.appendChild(document.createElement("div")), this.initializeMap();
      }

      _createClass(n, [{
        key: "initializeMap",
        value: function initializeMap() {
          var _this4 = this;

          new a.GoogleMapsApi(this.googleMapsKey).load().then(function () {
            _this4.createMap(), _this4.customZoomControl(_this4.mapPlaceholderElement), _this4.handleMarkers();
          }).catch(function (e) {
            console.error(e);
          });
        }
      }, {
        key: "createMap",
        value: function createMap() {
          var e = parseInt(this.element.getAttribute("data-rs-maps-zoom"));isNaN(e) && (e = 6);
          var _element$getAttribute = this.element.getAttribute("data-rs-maps").split(","),
              _element$getAttribute2 = _slicedToArray(_element$getAttribute, 2),
              t = _element$getAttribute2[0],
              i = _element$getAttribute2[1];

          this.map = new google.maps.Map(this.mapPlaceholderElement, { center: new google.maps.LatLng(t, i), zoom: e, minZoom: 4, maxZoom: 17, disableDefaultUI: !0, panControl: !0, zoomControl: !1, scaleControl: !0, streetViewControl: !1, styles: o.MapStyle.custom, mapTypeId: google.maps.MapTypeId.ROADMAP });
        }
      }, {
        key: "createPopup",
        value: function createPopup() {
          null === s && (s = r.PopupWrapper.popupClass);
        }
      }, {
        key: "handleMarkers",
        value: function handleMarkers() {
          var _this5 = this;

          this.createPopup(), this.markers = [].slice.call(this.element.querySelectorAll("[data-rs-map-marker]")).map(function (e) {
            var _e$getAttribute$split = e.getAttribute("data-rs-map-marker").split(","),
                _e$getAttribute$split2 = _slicedToArray(_e$getAttribute$split, 2),
                t = _e$getAttribute$split2[0],
                i = _e$getAttribute$split2[1];

            var a = {};return a.lat = t.trim(), a.lng = i.trim(), a.content = document.createElement("div"), a.content.innerHTML = e.innerHTML, _this5.addMarker(a), a;
          }), google.maps.event.addListener(this.map, "click", function () {
            null !== _this5.marker && void 0 !== _this5.marker && (_this5.marker.open = !1, _this5.marker.customPopup.hide());
          });
        }
      }, {
        key: "addMarker",
        value: function addMarker(e) {
          var _this6 = this;

          var t = { url: this.markerUrlPath, size: new google.maps.Size(26, 32) },
              i = new s(new google.maps.LatLng(e.lat, e.lng), e.content);var a = new google.maps.Marker({ position: new google.maps.LatLng(e.lat, e.lng), icon: t, map: this.map, customPopup: i });return i.marker = a, this.markerIsNotClickable || google.maps.event.addListener(a, "click", function () {
            var e = !1;!0 !== a.open && (e = !0), null !== _this6.marker && a !== _this6.marker && (_this6.marker.open = !1, _this6.marker.customPopup.hide()), _this6.marker = a, e ? (_this6.marker.customPopup.setMap(_this6.marker.map), _this6.marker.open = !0) : (_this6.marker.customPopup.hide(), _this6.marker.open = !1);
          }), a;
        }
      }, {
        key: "customZoomControl",
        value: function customZoomControl(e) {
          var _this7 = this;

          var t = e.appendChild(document.createElement("div"));t.classList.add("gmaps__control-container"), t.classList.add("gmaps__control-container--loading");var i = document.createElement("div"),
              a = document.createElement("div");i.classList.add("gmaps__button--zoom"), i.classList.add("gmaps__button--zoom-in"), a.classList.add("gmaps__button--zoom"), a.classList.add("gmaps__button--zoom-out"), t.appendChild(i), t.appendChild(a), google.maps.event.addDomListener(i, "click", function () {
            _this7.map.setZoom(_this7.map.getZoom() + 1);
          }), google.maps.event.addDomListener(a, "click", function () {
            _this7.map.setZoom(_this7.map.getZoom() - 1);
          });
        }
      }, {
        key: "attributes",
        get: function get() {
          return { markerUrl: "data-rs-maps-marker-url", markerIsNotClickable: "data-rs-maps-marker-is-not-clickable", mapsPlaceholderElement: "[data-rs-maps-placeholder]", googleMapsKey: "data-rs-maps-google-maps-key" };
        }
      }, {
        key: "variables",
        get: function get() {
          return { defaultMarkerUrl: "../_assets/image/marker.png" };
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[data-rs-maps]";
        }
      }]);

      return n;
    }();
  } }));