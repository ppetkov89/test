"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (t, e) {
  for (var r in e) {
    t[r] = e[r];
  }
}(exports, function (t) {
  var e = {};function r(n) {
    if (e[n]) return e[n].exports;var i = e[n] = { i: n, l: !1, exports: {} };return t[n].call(i.exports, i, i.exports, r), i.l = !0, i.exports;
  }return r.m = t, r.c = e, r.d = function (t, e, n) {
    r.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: n });
  }, r.r = function (t) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 });
  }, r.t = function (t, e) {
    if (1 & e && (t = r(t)), 8 & e) return t;if (4 & e && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t && t.__esModule) return t;var n = Object.create(null);if (r.r(n), Object.defineProperty(n, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t) for (var i in t) {
      r.d(n, i, function (e) {
        return t[e];
      }.bind(null, i));
    }return n;
  }, r.n = function (t) {
    var e = t && t.__esModule ? function () {
      return t.default;
    } : function () {
      return t;
    };return r.d(e, "a", e), e;
  }, r.o = function (t, e) {
    return Object.prototype.hasOwnProperty.call(t, e);
  }, r.p = "", r(r.s = 268);
}({ 268: function _(t, e, r) {
    t.exports = r(83);
  }, 83: function _(t, e, r) {
    "use strict";
    r.r(e), r.d(e, "PasswordValidator", function () {
      return i;
    });var n = "data-rs-password-validator";
    var i = function () {
      function i(t) {
        _classCallCheck(this, i);

        this.element = t, this.input = this.element.querySelector('input[type="password"]'), this.checklist = this.getChecklist, this.addEventHandlers();
      }

      _createClass(i, [{
        key: "addEventHandlers",
        value: function addEventHandlers() {
          var _this = this;

          this.input.addEventListener("keyup", function () {
            _this.validateInput();
          }), this.input.addEventListener("blur", function () {
            _this.validateInput();
          }), this.input.addEventListener("focus", function () {
            _this.showChecklist();
          });
        }
      }, {
        key: "validateInput",
        value: function validateInput() {
          var _iteratorNormalCompletion = true;
          var _didIteratorError = false;
          var _iteratorError = undefined;

          try {
            for (var _iterator = this.checklist[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
              var _t = _step.value;
              this.input.value.match(this.validators[_t.name]) ? _t.element.classList.add(this.classes.valid) : _t.element.classList.remove(this.classes.valid);
            }
          } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion && _iterator.return) {
                _iterator.return();
              }
            } finally {
              if (_didIteratorError) {
                throw _iteratorError;
              }
            }
          }
        }
      }, {
        key: "showChecklist",
        value: function showChecklist() {
          this.element.parentNode.querySelector("[" + this.attributes.checklist + "]").removeAttribute("hidden");
        }
      }, {
        key: "attributes",
        get: function get() {
          return { validate: n + "-validate", checklist: n + "-checklist" };
        }
      }, {
        key: "classes",
        get: function get() {
          return { valid: "valid" };
        }
      }, {
        key: "validators",
        get: function get() {
          return { minSign: /.{8,}/, useUpper: /[A-Z]/, useDigit: /[0-9]/, useChar: /[a-z]/, noSymbol: /^([^ /\\,#&<>:';"?=%]+)$/ };
        }
      }, {
        key: "getChecklist",
        get: function get() {
          var t = [],
              e = this.element.parentNode.querySelectorAll("[" + this.attributes.validate + "]");var _iteratorNormalCompletion2 = true;
          var _didIteratorError2 = false;
          var _iteratorError2 = undefined;

          try {
            for (var _iterator2 = e[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
              var _r = _step2.value;
              t.push({ name: _r.dataset.rsPasswordValidatorValidate, element: _r });
            }
          } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion2 && _iterator2.return) {
                _iterator2.return();
              }
            } finally {
              if (_didIteratorError2) {
                throw _iteratorError2;
              }
            }
          }

          return t;
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[" + n + "]";
        }
      }]);

      return i;
    }();
  } }));