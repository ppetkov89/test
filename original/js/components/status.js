"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (t, e) {
  for (var i in e) {
    t[i] = e[i];
  }
}(exports, function (t) {
  var e = {};function i(n) {
    if (e[n]) return e[n].exports;var o = e[n] = { i: n, l: !1, exports: {} };return t[n].call(o.exports, o, o.exports, i), o.l = !0, o.exports;
  }return i.m = t, i.c = e, i.d = function (t, e, n) {
    i.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: n });
  }, i.r = function (t) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 });
  }, i.t = function (t, e) {
    if (1 & e && (t = i(t)), 8 & e) return t;if (4 & e && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t && t.__esModule) return t;var n = Object.create(null);if (i.r(n), Object.defineProperty(n, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t) for (var o in t) {
      i.d(n, o, function (e) {
        return t[e];
      }.bind(null, o));
    }return n;
  }, i.n = function (t) {
    var e = t && t.__esModule ? function () {
      return t.default;
    } : function () {
      return t;
    };return i.d(e, "a", e), e;
  }, i.o = function (t, e) {
    return Object.prototype.hasOwnProperty.call(t, e);
  }, i.p = "", i(i.s = 286);
}({ 0: function _(t, e) {
    t.exports = function () {
      function _class() {
        _classCallCheck(this, _class);
      }

      _createClass(_class, null, [{
        key: "triggerEvent",
        value: function triggerEvent(t, e) {
          var i = void 0;"function" == typeof Event ? i = new Event(e) : (i = document.createEvent("Event"), i.initEvent(e, !0, !0)), t.dispatchEvent(i);
        }
      }, {
        key: "getElementByAttribute",
        value: function getElementByAttribute(t, e) {
          return e ? document.querySelector("[" + t + "=\"" + e + "\"]") : document.querySelector("[" + t + "]");
        }
      }, {
        key: "getElementsByAttribute",
        value: function getElementsByAttribute(t, e) {
          return e ? document.querySelectorAll("[" + t + "=\"" + e + "\"]") : document.querySelectorAll("[" + t + "]");
        }
      }, {
        key: "getElementByAttributeWithinElement",
        value: function getElementByAttributeWithinElement(t, e, i) {
          return i ? t.querySelector("[" + e + "=\"" + i + "\"]") : t.querySelector("[" + e + "]");
        }
      }, {
        key: "isMouseOver",
        value: function isMouseOver(t) {
          return Array.prototype.slice.call(t.parentElement.querySelectorAll(":hover")).filter(function () {
            return t[0] == this;
          }).length > 0;
        }
      }, {
        key: "getElementsByAttributeWithinElement",
        value: function getElementsByAttributeWithinElement(t, e, i) {
          return i ? t.querySelectorAll("[" + e + "=\"" + i + "\"]") : t.querySelectorAll("[" + e + "]");
        }
      }, {
        key: "getElementByClassWithinElement",
        value: function getElementByClassWithinElement(t, e) {
          return t.querySelector("." + e);
        }
      }, {
        key: "maxWidthMobileViewports",
        value: function maxWidthMobileViewports() {
          return 940;
        }
      }, {
        key: "createEvent",
        value: function createEvent(t) {
          var e = void 0;return "function" == typeof Event ? e = new Event(t) : (e = document.createEvent("Event"), e.initEvent(t, !0, !0)), e;
        }
      }, {
        key: "isDesktop",
        value: function isDesktop() {
          return Math.max(document.documentElement.clientWidth, window.innerWidth || 0) > this.maxWidthMobileViewports();
        }
      }, {
        key: "isIE11",
        value: function isIE11() {
          return -1 !== navigator.userAgent.indexOf("MSIE") || navigator.appVersion.indexOf("Trident/") > -1;
        }
      }, {
        key: "getKeyCodeOnKeyDownEvent",
        value: function getKeyCodeOnKeyDownEvent(t) {
          var e = void 0;if (null != t) if (void 0 === t.code) switch (t.keyCode) {case 13:
              e = "Enter";break;case 38:
              e = "ArrowUp";break;case 40:
              e = "ArrowDown";break;case 9:
              e = "Tab";break;case 27:
              e = "Escape";break;case 33:
              e = "PageUp";break;case 34:
              e = "PageDown";break;default:
              e = void 0;} else e = t.code;return e;
        }
      }]);

      return _class;
    }();
  }, 286: function _(t, e, i) {
    t.exports = i(90);
  }, 90: function _(t, e, i) {
    "use strict";
    i.r(e), i.d(e, "Status", function () {
      return r;
    });var n = i(0),
        o = i.n(n);
    var r = function () {
      function r(t) {
        _classCallCheck(this, r);

        this.element = t, this.activeElement = o.a.getElementByAttributeWithinElement(this.element, this.attributes.active), this.tooltipPointer = o.a.getElementByAttributeWithinElement(this.element, this.attributes.tooltipPointer), this.tooltip = o.a.getElementByAttributeWithinElement(this.element, this.attributes.tooltip), this.tooltipContent = o.a.getElementByAttributeWithinElement(this.element, this.attributes.tooltipContent), this.progressBarContent = o.a.getElementByAttributeWithinElement(this.element, this.attributes.progressBarContent), this.tooltipWidth = this.tooltipContent.offsetWidth, this.middleActiveElement = this.activeElement.offsetLeft + this.activeElement.offsetWidth / 2, this.setTooltip();
      }

      _createClass(r, [{
        key: "setTooltip",
        value: function setTooltip() {
          this.middleActiveElement - this.tooltipWidth / 2 > 0 ? this.middleActiveElement - this.tooltipWidth / 2 + this.tooltipWidth <= this.progressBarContent.offsetWidth ? (this.tooltip.style.left = this.middleActiveElement - this.tooltipWidth / 2 + "px", this.setTooltipPointer(this.positions.middle)) : (this.tooltip.style.left = this.progressBarContent.offsetWidth - this.tooltipWidth + "px", this.setTooltipPointer(this.positions.right)) : this.setTooltipPointer(this.positions.left);
        }
      }, {
        key: "setTooltipPointer",
        value: function setTooltipPointer(t) {
          var e = this.tooltipPointer.getBoundingClientRect();t === this.positions.middle ? this.tooltipPointer.style.left = this.tooltipContent.offsetWidth / 2 - e.width / 2 + "px" : t === this.positions.left ? this.tooltipPointer.style.left = this.middleActiveElement - e.width / 2 + "px" : t === this.positions.right && (this.tooltipPointer.style.left = this.middleActiveElement - e.width / 2 - (this.progressBarContent.offsetWidth - this.tooltipWidth) + "px");
        }
      }, {
        key: "attributes",
        get: function get() {
          return { active: "data-rs-status-active", tooltipPointer: "data-rs-status-tooltip-pointer", tooltip: "data-rs-status-tooltip", tooltipContent: "data-rs-status-tooltip-content", progressBarContent: "data-rs-status-progress-bar-content" };
        }
      }, {
        key: "positions",
        get: function get() {
          return { left: "left", right: "right", middle: "middle" };
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[data-rs-status]";
        }
      }]);

      return r;
    }();
  } }));