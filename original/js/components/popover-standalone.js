"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (e, t) {
  for (var r in t) {
    e[r] = t[r];
  }
}(exports, function (e) {
  var t = {};function r(s) {
    if (t[s]) return t[s].exports;var i = t[s] = { i: s, l: !1, exports: {} };return e[s].call(i.exports, i, i.exports, r), i.l = !0, i.exports;
  }return r.m = e, r.c = t, r.d = function (e, t, s) {
    r.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: s });
  }, r.r = function (e) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(e, "__esModule", { value: !0 });
  }, r.t = function (e, t) {
    if (1 & t && (e = r(e)), 8 & t) return e;if (4 & t && "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e && e.__esModule) return e;var s = Object.create(null);if (r.r(s), Object.defineProperty(s, "default", { enumerable: !0, value: e }), 2 & t && "string" != typeof e) for (var i in e) {
      r.d(s, i, function (t) {
        return e[t];
      }.bind(null, i));
    }return s;
  }, r.n = function (e) {
    var t = e && e.__esModule ? function () {
      return e.default;
    } : function () {
      return e;
    };return r.d(t, "a", t), t;
  }, r.o = function (e, t) {
    return Object.prototype.hasOwnProperty.call(e, t);
  }, r.p = "", r(r.s = 272);
}({ 0: function _(e, t) {
    e.exports = function () {
      function _class() {
        _classCallCheck(this, _class);
      }

      _createClass(_class, null, [{
        key: "triggerEvent",
        value: function triggerEvent(e, t) {
          var r = void 0;"function" == typeof Event ? r = new Event(t) : (r = document.createEvent("Event"), r.initEvent(t, !0, !0)), e.dispatchEvent(r);
        }
      }, {
        key: "getElementByAttribute",
        value: function getElementByAttribute(e, t) {
          return t ? document.querySelector("[" + e + "=\"" + t + "\"]") : document.querySelector("[" + e + "]");
        }
      }, {
        key: "getElementsByAttribute",
        value: function getElementsByAttribute(e, t) {
          return t ? document.querySelectorAll("[" + e + "=\"" + t + "\"]") : document.querySelectorAll("[" + e + "]");
        }
      }, {
        key: "getElementByAttributeWithinElement",
        value: function getElementByAttributeWithinElement(e, t, r) {
          return r ? e.querySelector("[" + t + "=\"" + r + "\"]") : e.querySelector("[" + t + "]");
        }
      }, {
        key: "isMouseOver",
        value: function isMouseOver(e) {
          return Array.prototype.slice.call(e.parentElement.querySelectorAll(":hover")).filter(function () {
            return e[0] == this;
          }).length > 0;
        }
      }, {
        key: "getElementsByAttributeWithinElement",
        value: function getElementsByAttributeWithinElement(e, t, r) {
          return r ? e.querySelectorAll("[" + t + "=\"" + r + "\"]") : e.querySelectorAll("[" + t + "]");
        }
      }, {
        key: "getElementByClassWithinElement",
        value: function getElementByClassWithinElement(e, t) {
          return e.querySelector("." + t);
        }
      }, {
        key: "maxWidthMobileViewports",
        value: function maxWidthMobileViewports() {
          return 940;
        }
      }, {
        key: "createEvent",
        value: function createEvent(e) {
          var t = void 0;return "function" == typeof Event ? t = new Event(e) : (t = document.createEvent("Event"), t.initEvent(e, !0, !0)), t;
        }
      }, {
        key: "isDesktop",
        value: function isDesktop() {
          return Math.max(document.documentElement.clientWidth, window.innerWidth || 0) > this.maxWidthMobileViewports();
        }
      }, {
        key: "isIE11",
        value: function isIE11() {
          return -1 !== navigator.userAgent.indexOf("MSIE") || navigator.appVersion.indexOf("Trident/") > -1;
        }
      }, {
        key: "getKeyCodeOnKeyDownEvent",
        value: function getKeyCodeOnKeyDownEvent(e) {
          var t = void 0;if (null != e) if (void 0 === e.code) switch (e.keyCode) {case 13:
              t = "Enter";break;case 38:
              t = "ArrowUp";break;case 40:
              t = "ArrowDown";break;case 9:
              t = "Tab";break;case 27:
              t = "Escape";break;case 33:
              t = "PageUp";break;case 34:
              t = "PageDown";break;default:
              t = void 0;} else t = e.code;return t;
        }
      }]);

      return _class;
    }();
  }, 272: function _(e, t, r) {
    e.exports = r(273);
  }, 273: function _(e, t, r) {
    "use strict";
    r.r(t);var s = r(85);var i = document.querySelectorAll(s.Popover.getSelector());for (var _e = 0; _e < i.length; _e++) {
      new s.Popover(i[_e]);
    }
  }, 85: function _(e, t, r) {
    "use strict";
    r.r(t), r.d(t, "Popover", function () {
      return n;
    });var s = r(0),
        i = r.n(s);
    var n = function () {
      function n(e) {
        _classCallCheck(this, n);

        this.element = e, this.trigger = i.a.getElementByAttribute(this.attributes.trigger, this.element.getAttribute("data-rs-popover")), this.overlay = i.a.getElementByAttribute(this.attributes.overlay), this.disableOverlay = this.element.hasAttribute(this.attributes.disableOverlay), this.addEventHandlers();
      }

      _createClass(n, [{
        key: "addEventHandlers",
        value: function addEventHandlers() {
          var _this = this;

          null !== this.trigger && this.trigger.addEventListener("click", function () {
            _this.element.classList.contains(_this.classes.popoverActive) ? (_this.element.classList.remove(_this.classes.popoverActive), null === _this.overlay || _this.disableOverlay || _this.overlay.classList.remove(_this.classes.modalOverlayActive)) : (_this.element.classList.add(_this.classes.popoverActive), null === _this.overlay || _this.disableOverlay || _this.overlay.classList.add(_this.classes.modalOverlayActive)), _this.toggleActiveClassOnTrigger();
          }), null === this.overlay || this.disableOverlay ? window.addEventListener("click", function (e) {
            _this.trigger.contains(e.target) || !_this.element.classList.contains(_this.classes.popoverActive) || _this.element.contains(e.target) || _this.element.classList.remove(_this.classes.popoverActive), _this.toggleActiveClassOnTrigger();
          }) : this.overlay.addEventListener("click", function () {
            _this.element.classList.remove(_this.classes.popoverActive), _this.overlay.classList.remove(_this.classes.modalOverlayActive), _this.toggleActiveClassOnTrigger();
          });
        }
      }, {
        key: "toggleActiveClassOnTrigger",
        value: function toggleActiveClassOnTrigger() {
          this.trigger.classList.contains(this.classes.active) ? this.trigger.classList.remove(this.classes.active) : this.trigger.classList.add(this.classes.active);
        }
      }, {
        key: "classes",
        get: function get() {
          return { popoverActive: "popover--active", modalOverlayActive: "modal__overlay--active", active: "active" };
        }
      }, {
        key: "attributes",
        get: function get() {
          return { overlay: "data-rs-popover-overlay", trigger: "data-rs-popover-trigger", disableOverlay: "data-rs-popover-disable-overlay" };
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[data-rs-popover]";
        }
      }]);

      return n;
    }();
  } }));