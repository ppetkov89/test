"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (e, t) {
  for (var s in t) {
    e[s] = t[s];
  }
}(exports, function (e) {
  var t = {};function s(r) {
    if (t[r]) return t[r].exports;var n = t[r] = { i: r, l: !1, exports: {} };return e[r].call(n.exports, n, n.exports, s), n.l = !0, n.exports;
  }return s.m = e, s.c = t, s.d = function (e, t, r) {
    s.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: r });
  }, s.r = function (e) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(e, "__esModule", { value: !0 });
  }, s.t = function (e, t) {
    if (1 & t && (e = s(e)), 8 & t) return e;if (4 & t && "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e && e.__esModule) return e;var r = Object.create(null);if (s.r(r), Object.defineProperty(r, "default", { enumerable: !0, value: e }), 2 & t && "string" != typeof e) for (var n in e) {
      s.d(r, n, function (t) {
        return e[t];
      }.bind(null, n));
    }return r;
  }, s.n = function (e) {
    var t = e && e.__esModule ? function () {
      return e.default;
    } : function () {
      return e;
    };return s.d(t, "a", t), t;
  }, s.o = function (e, t) {
    return Object.prototype.hasOwnProperty.call(e, t);
  }, s.p = "", s(s.s = 294);
}({ 0: function _(e, t) {
    e.exports = function () {
      function _class() {
        _classCallCheck(this, _class);
      }

      _createClass(_class, null, [{
        key: "triggerEvent",
        value: function triggerEvent(e, t) {
          var s = void 0;"function" == typeof Event ? s = new Event(t) : (s = document.createEvent("Event"), s.initEvent(t, !0, !0)), e.dispatchEvent(s);
        }
      }, {
        key: "getElementByAttribute",
        value: function getElementByAttribute(e, t) {
          return t ? document.querySelector("[" + e + "=\"" + t + "\"]") : document.querySelector("[" + e + "]");
        }
      }, {
        key: "getElementsByAttribute",
        value: function getElementsByAttribute(e, t) {
          return t ? document.querySelectorAll("[" + e + "=\"" + t + "\"]") : document.querySelectorAll("[" + e + "]");
        }
      }, {
        key: "getElementByAttributeWithinElement",
        value: function getElementByAttributeWithinElement(e, t, s) {
          return s ? e.querySelector("[" + t + "=\"" + s + "\"]") : e.querySelector("[" + t + "]");
        }
      }, {
        key: "isMouseOver",
        value: function isMouseOver(e) {
          return Array.prototype.slice.call(e.parentElement.querySelectorAll(":hover")).filter(function () {
            return e[0] == this;
          }).length > 0;
        }
      }, {
        key: "getElementsByAttributeWithinElement",
        value: function getElementsByAttributeWithinElement(e, t, s) {
          return s ? e.querySelectorAll("[" + t + "=\"" + s + "\"]") : e.querySelectorAll("[" + t + "]");
        }
      }, {
        key: "getElementByClassWithinElement",
        value: function getElementByClassWithinElement(e, t) {
          return e.querySelector("." + t);
        }
      }, {
        key: "maxWidthMobileViewports",
        value: function maxWidthMobileViewports() {
          return 940;
        }
      }, {
        key: "createEvent",
        value: function createEvent(e) {
          var t = void 0;return "function" == typeof Event ? t = new Event(e) : (t = document.createEvent("Event"), t.initEvent(e, !0, !0)), t;
        }
      }, {
        key: "isDesktop",
        value: function isDesktop() {
          return Math.max(document.documentElement.clientWidth, window.innerWidth || 0) > this.maxWidthMobileViewports();
        }
      }, {
        key: "isIE11",
        value: function isIE11() {
          return -1 !== navigator.userAgent.indexOf("MSIE") || navigator.appVersion.indexOf("Trident/") > -1;
        }
      }, {
        key: "getKeyCodeOnKeyDownEvent",
        value: function getKeyCodeOnKeyDownEvent(e) {
          var t = void 0;if (null != e) if (void 0 === e.code) switch (e.keyCode) {case 13:
              t = "Enter";break;case 38:
              t = "ArrowUp";break;case 40:
              t = "ArrowDown";break;case 9:
              t = "Tab";break;case 27:
              t = "Escape";break;case 33:
              t = "PageUp";break;case 34:
              t = "PageDown";break;default:
              t = void 0;} else t = e.code;return t;
        }
      }]);

      return _class;
    }();
  }, 294: function _(e, t, s) {
    e.exports = s(93);
  }, 93: function _(e, t, s) {
    "use strict";
    s.r(t), s.d(t, "Toast", function () {
      return i;
    });var r = s(0),
        n = s.n(r);
    var i = function () {
      function i(e) {
        _classCallCheck(this, i);

        this.element = e, this.id = this.element.getAttribute("data-rs-toast"), this.triggerElement = n.a.getElementByAttribute(this.attributes.show, this.id), this.addEventHandlers(), this.closeTimeout;
      }

      _createClass(i, [{
        key: "addEventHandlers",
        value: function addEventHandlers() {
          var _this = this;

          this.triggerElement.addEventListener("click", function (e) {
            e.preventDefault(), _this.show(), _this.destroyCloseTimeout(), _this.createCloseTimeOut();
          }), this.element.addEventListener("mouseenter", function () {
            _this.destroyCloseTimeout();
          }), this.element.addEventListener("mouseleave", function () {
            _this.closeTimeout = _this.createCloseTimeOut();
          });
        }
      }, {
        key: "show",
        value: function show() {
          this.element.classList.remove(this.classes.closableClosed), this.element.classList.add(this.classes.show), this.element.classList.add(this.classes.toastActive);
        }
      }, {
        key: "hide",
        value: function hide() {
          this.element.classList.remove(this.classes.show), this.element.classList.remove(this.classes.toastActive), this.element.classList.add(this.classes.closableClosed);
        }
      }, {
        key: "createCloseTimeOut",
        value: function createCloseTimeOut() {
          var _this2 = this;

          this.closeTimeout = setTimeout(function () {
            n.a.isMouseOver(_this2.element) || _this2.hide();
          }, 1e4);
        }
      }, {
        key: "destroyCloseTimeout",
        value: function destroyCloseTimeout() {
          clearTimeout(this.closeTimeout);
        }
      }, {
        key: "attributes",
        get: function get() {
          return { show: "data-rs-toast-show" };
        }
      }, {
        key: "classes",
        get: function get() {
          return { image: "data-rs-toast-image", show: "show", closableClosed: "closable--closed", toastActive: "toast--active" };
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[data-rs-toast]";
        }
      }]);

      return i;
    }();
  } }));