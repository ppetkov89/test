"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (e, t) {
  for (var r in t) {
    e[r] = t[r];
  }
}(exports, function (e) {
  var t = {};function r(n) {
    if (t[n]) return t[n].exports;var o = t[n] = { i: n, l: !1, exports: {} };return e[n].call(o.exports, o, o.exports, r), o.l = !0, o.exports;
  }return r.m = e, r.c = t, r.d = function (e, t, n) {
    r.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: n });
  }, r.r = function (e) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(e, "__esModule", { value: !0 });
  }, r.t = function (e, t) {
    if (1 & t && (e = r(e)), 8 & t) return e;if (4 & t && "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e && e.__esModule) return e;var n = Object.create(null);if (r.r(n), Object.defineProperty(n, "default", { enumerable: !0, value: e }), 2 & t && "string" != typeof e) for (var o in e) {
      r.d(n, o, function (t) {
        return e[t];
      }.bind(null, o));
    }return n;
  }, r.n = function (e) {
    var t = e && e.__esModule ? function () {
      return e.default;
    } : function () {
      return e;
    };return r.d(t, "a", t), t;
  }, r.o = function (e, t) {
    return Object.prototype.hasOwnProperty.call(e, t);
  }, r.p = "", r(r.s = 280);
}([, function (e, t, r) {
  var n = r(103),
      o = r(18),
      a = /[T ]/,
      u = /:/,
      i = /^(\d{2})$/,
      s = [/^([+-]\d{2})$/, /^([+-]\d{3})$/, /^([+-]\d{4})$/],
      f = /^(\d{4})/,
      c = [/^([+-]\d{4})/, /^([+-]\d{5})/, /^([+-]\d{6})/],
      l = /^-(\d{2})$/,
      v = /^-?(\d{3})$/,
      g = /^-?(\d{2})-?(\d{2})$/,
      d = /^-?W(\d{2})$/,
      p = /^-?W(\d{2})-?(\d{1})$/,
      m = /^(\d{2}([.,]\d*)?)$/,
      h = /^(\d{2}):?(\d{2}([.,]\d*)?)$/,
      x = /^(\d{2}):?(\d{2}):?(\d{2}([.,]\d*)?)$/,
      D = /([Z+-].*)$/,
      M = /^(Z)$/,
      T = /^([+-])(\d{2})$/,
      y = /^([+-])(\d{2}):?(\d{2})$/;function S(e, t, r) {
    t = t || 0, r = r || 0;var n = new Date(0);n.setUTCFullYear(e, 0, 4);var o = 7 * t + r + 1 - (n.getUTCDay() || 7);return n.setUTCDate(n.getUTCDate() + o), n;
  }e.exports = function (e, t) {
    if (o(e)) return new Date(e.getTime());if ("string" != typeof e) return new Date(e);var r = (t || {}).additionalDigits;r = null == r ? 2 : Number(r);var Y = function (e) {
      var t,
          r = {},
          n = e.split(a);u.test(n[0]) ? (r.date = null, t = n[0]) : (r.date = n[0], t = n[1]);if (t) {
        var o = D.exec(t);o ? (r.time = t.replace(o[1], ""), r.timezone = o[1]) : r.time = t;
      }return r;
    }(e),
        b = function (e, t) {
      var r,
          n = s[t],
          o = c[t];if (r = f.exec(e) || o.exec(e)) {
        var a = r[1];return { year: parseInt(a, 10), restDateString: e.slice(a.length) };
      }if (r = i.exec(e) || n.exec(e)) {
        var u = r[1];return { year: 100 * parseInt(u, 10), restDateString: e.slice(u.length) };
      }return { year: null };
    }(Y.date, r),
        O = b.year,
        w = function (e, t) {
      if (null === t) return null;var r, n, o, a;if (0 === e.length) return (n = new Date(0)).setUTCFullYear(t), n;if (r = l.exec(e)) return n = new Date(0), o = parseInt(r[1], 10) - 1, n.setUTCFullYear(t, o), n;if (r = v.exec(e)) {
        n = new Date(0);var u = parseInt(r[1], 10);return n.setUTCFullYear(t, 0, u), n;
      }if (r = g.exec(e)) {
        n = new Date(0), o = parseInt(r[1], 10) - 1;var i = parseInt(r[2], 10);return n.setUTCFullYear(t, o, i), n;
      }if (r = d.exec(e)) return a = parseInt(r[1], 10) - 1, S(t, a);if (r = p.exec(e)) {
        a = parseInt(r[1], 10) - 1;var s = parseInt(r[2], 10) - 1;return S(t, a, s);
      }return null;
    }(b.restDateString, O);if (w) {
      var I,
          F = w.getTime(),
          H = 0;if (Y.time && (H = function (e) {
        var t, r, n;if (t = m.exec(e)) return (r = parseFloat(t[1].replace(",", "."))) % 24 * 36e5;if (t = h.exec(e)) return r = parseInt(t[1], 10), n = parseFloat(t[2].replace(",", ".")), r % 24 * 36e5 + 6e4 * n;if (t = x.exec(e)) {
          r = parseInt(t[1], 10), n = parseInt(t[2], 10);var o = parseFloat(t[3].replace(",", "."));return r % 24 * 36e5 + 6e4 * n + 1e3 * o;
        }return null;
      }(Y.time)), Y.timezone) I = 6e4 * function (e) {
        var t, r;if (t = M.exec(e)) return 0;if (t = T.exec(e)) return r = 60 * parseInt(t[2], 10), "+" === t[1] ? -r : r;if (t = y.exec(e)) return r = 60 * parseInt(t[2], 10) + parseInt(t[3], 10), "+" === t[1] ? -r : r;return 0;
      }(Y.timezone);else {
        var N = F + H,
            W = new Date(N);I = n(W);var k = new Date(N);k.setDate(W.getDate() + 1);var R = n(k) - n(W);R > 0 && (I += R);
      }return new Date(F + H + I);
    }return new Date(e);
  };
},,, function (e, t, r) {
  var n = r(1),
      o = r(5);e.exports = function (e) {
    var t = n(e),
        r = t.getFullYear(),
        a = new Date(0);a.setFullYear(r + 1, 0, 4), a.setHours(0, 0, 0, 0);var u = o(a),
        i = new Date(0);i.setFullYear(r, 0, 4), i.setHours(0, 0, 0, 0);var s = o(i);return t.getTime() >= u.getTime() ? r + 1 : t.getTime() >= s.getTime() ? r : r - 1;
  };
}, function (e, t, r) {
  var n = r(14);e.exports = function (e) {
    return n(e, { weekStartsOn: 1 });
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e);return t.setHours(0, 0, 0, 0), t;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e),
        o = Number(t);return r.setDate(r.getDate() + o), r;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e).getTime(),
        o = Number(t);return new Date(r + o);
  };
}, function (e, t, r) {
  var n = r(4),
      o = r(5);e.exports = function (e) {
    var t = n(e),
        r = new Date(0);return r.setFullYear(t, 0, 4), r.setHours(0, 0, 0, 0), o(r);
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e).getTime(),
        o = n(t).getTime();return r < o ? -1 : r > o ? 1 : 0;
  };
},,,, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = t && Number(t.weekStartsOn) || 0,
        o = n(e),
        a = o.getDay(),
        u = (a < r ? 7 : 0) + a - r;return o.setDate(o.getDate() - u), o.setHours(0, 0, 0, 0), o;
  };
}, function (e, t, r) {
  var n = r(6);e.exports = function (e, t) {
    var r = n(e),
        o = n(t),
        a = r.getTime() - 6e4 * r.getTimezoneOffset(),
        u = o.getTime() - 6e4 * o.getTimezoneOffset();return Math.round((a - u) / 864e5);
  };
}, function (e, t, r) {
  var n = r(1),
      o = r(19);e.exports = function (e, t) {
    var r = n(e),
        a = Number(t),
        u = r.getMonth() + a,
        i = new Date(0);i.setFullYear(r.getFullYear(), u, 1), i.setHours(0, 0, 0, 0);var s = o(i);return r.setMonth(u, Math.min(s, r.getDate())), r;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e),
        o = n(t);return r.getTime() - o.getTime();
  };
}, function (e, t) {
  e.exports = function (e) {
    return e instanceof Date;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e),
        r = t.getFullYear(),
        o = t.getMonth(),
        a = new Date(0);return a.setFullYear(r, o + 1, 0), a.setHours(0, 0, 0, 0), a.getDate();
  };
}, function (e, t, r) {
  var n = r(7);e.exports = function (e, t) {
    var r = Number(t);return n(e, 7 * r);
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e).getTime(),
        o = n(t).getTime();return r > o ? -1 : r < o ? 1 : 0;
  };
}, function (e, t, r) {
  var n = r(1),
      o = r(38),
      a = r(10);e.exports = function (e, t) {
    var r = n(e),
        u = n(t),
        i = a(r, u),
        s = Math.abs(o(r, u));return r.setMonth(r.getMonth() - i * s), i * (s - (a(r, u) === -i));
  };
}, function (e, t, r) {
  var n = r(17);e.exports = function (e, t) {
    var r = n(e, t) / 1e3;return r > 0 ? Math.floor(r) : Math.ceil(r);
  };
}, function (e, t, r) {
  var n = r(116),
      o = r(117);e.exports = { distanceInWords: n(), format: o() };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e);return t.setHours(23, 59, 59, 999), t;
  };
}, function (e, t, r) {
  var n = r(1),
      o = r(5),
      a = r(9);e.exports = function (e) {
    var t = n(e),
        r = o(t).getTime() - a(t).getTime();return Math.round(r / 6048e5) + 1;
  };
}, function (e, t, r) {
  var n = r(14);e.exports = function (e, t, r) {
    var o = n(e, r),
        a = n(t, r);return o.getTime() === a.getTime();
  };
},,, function (e, t, r) {
  var n = r(8);e.exports = function (e, t) {
    var r = Number(t);return n(e, 36e5 * r);
  };
}, function (e, t, r) {
  var n = r(4),
      o = r(32);e.exports = function (e, t) {
    var r = Number(t);return o(e, n(e) + r);
  };
}, function (e, t, r) {
  var n = r(1),
      o = r(9),
      a = r(15);e.exports = function (e, t) {
    var r = n(e),
        u = Number(t),
        i = a(r, o(r)),
        s = new Date(0);return s.setFullYear(u, 0, 4), s.setHours(0, 0, 0, 0), (r = o(s)).setDate(r.getDate() + i), r;
  };
}, function (e, t, r) {
  var n = r(8);e.exports = function (e, t) {
    var r = Number(t);return n(e, 6e4 * r);
  };
}, function (e, t, r) {
  var n = r(16);e.exports = function (e, t) {
    var r = Number(t);return n(e, 3 * r);
  };
}, function (e, t, r) {
  var n = r(8);e.exports = function (e, t) {
    var r = Number(t);return n(e, 1e3 * r);
  };
}, function (e, t, r) {
  var n = r(16);e.exports = function (e, t) {
    var r = Number(t);return n(e, 12 * r);
  };
}, function (e, t, r) {
  var n = r(4);e.exports = function (e, t) {
    return n(e) - n(t);
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e),
        o = n(t);return 12 * (r.getFullYear() - o.getFullYear()) + (r.getMonth() - o.getMonth());
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e);return Math.floor(t.getMonth() / 3) + 1;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e),
        o = n(t);return r.getFullYear() - o.getFullYear();
  };
}, function (e, t, r) {
  var n = r(1),
      o = r(15),
      a = r(10);e.exports = function (e, t) {
    var r = n(e),
        u = n(t),
        i = a(r, u),
        s = Math.abs(o(r, u));return r.setDate(r.getDate() - i * s), i * (s - (a(r, u) === -i));
  };
}, function (e, t, r) {
  var n = r(31);e.exports = function (e, t) {
    var r = Number(t);return n(e, -r);
  };
}, function (e, t, r) {
  var n = r(21),
      o = r(1),
      a = r(23),
      u = r(22),
      i = r(24);e.exports = function (e, t, r) {
    var s = r || {},
        f = n(e, t),
        c = s.locale,
        l = i.distanceInWords.localize;c && c.distanceInWords && c.distanceInWords.localize && (l = c.distanceInWords.localize);var v,
        g,
        d = { addSuffix: Boolean(s.addSuffix), comparison: f };f > 0 ? (v = o(e), g = o(t)) : (v = o(t), g = o(e));var p,
        m = a(g, v),
        h = g.getTimezoneOffset() - v.getTimezoneOffset(),
        x = Math.round(m / 60) - h;if (x < 2) return s.includeSeconds ? m < 5 ? l("lessThanXSeconds", 5, d) : m < 10 ? l("lessThanXSeconds", 10, d) : m < 20 ? l("lessThanXSeconds", 20, d) : m < 40 ? l("halfAMinute", null, d) : l(m < 60 ? "lessThanXMinutes" : "xMinutes", 1, d) : 0 === x ? l("lessThanXMinutes", 1, d) : l("xMinutes", x, d);if (x < 45) return l("xMinutes", x, d);if (x < 90) return l("aboutXHours", 1, d);if (x < 1440) return l("aboutXHours", Math.round(x / 60), d);if (x < 2520) return l("xDays", 1, d);if (x < 43200) return l("xDays", Math.round(x / 1440), d);if (x < 86400) return l("aboutXMonths", p = Math.round(x / 43200), d);if ((p = u(g, v)) < 12) return l("xMonths", Math.round(x / 43200), d);var D = p % 12,
        M = Math.floor(p / 12);return D < 3 ? l("aboutXYears", M, d) : D < 9 ? l("overXYears", M, d) : l("almostXYears", M + 1, d);
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = t && Number(t.weekStartsOn) || 0,
        o = n(e),
        a = o.getDay(),
        u = 6 + (a < r ? -7 : 0) - (a - r);return o.setDate(o.getDate() + u), o.setHours(23, 59, 59, 999), o;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e),
        r = t.getMonth();return t.setFullYear(t.getFullYear(), r + 1, 0), t.setHours(23, 59, 59, 999), t;
  };
}, function (e, t, r) {
  var n = r(1),
      o = r(47),
      a = r(15);e.exports = function (e) {
    var t = n(e);return a(t, o(t)) + 1;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e),
        r = new Date(0);return r.setFullYear(t.getFullYear(), 0, 1), r.setHours(0, 0, 0, 0), r;
  };
}, function (e, t, r) {
  var n = r(18);e.exports = function (e) {
    if (n(e)) return !isNaN(e);throw new TypeError(toString.call(e) + " is not an instance of Date");
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e).getFullYear();return t % 400 == 0 || t % 4 == 0 && t % 100 != 0;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e).getDay();return 0 === t && (t = 7), t;
  };
}, function (e, t, r) {
  var n = r(52);e.exports = function (e, t) {
    var r = n(e),
        o = n(t);return r.getTime() === o.getTime();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e);return t.setMinutes(0, 0, 0), t;
  };
}, function (e, t, r) {
  var n = r(27);e.exports = function (e, t) {
    return n(e, t, { weekStartsOn: 1 });
  };
}, function (e, t, r) {
  var n = r(9);e.exports = function (e, t) {
    var r = n(e),
        o = n(t);return r.getTime() === o.getTime();
  };
}, function (e, t, r) {
  var n = r(56);e.exports = function (e, t) {
    var r = n(e),
        o = n(t);return r.getTime() === o.getTime();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e);return t.setSeconds(0, 0), t;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e),
        o = n(t);return r.getFullYear() === o.getFullYear() && r.getMonth() === o.getMonth();
  };
}, function (e, t, r) {
  var n = r(59);e.exports = function (e, t) {
    var r = n(e),
        o = n(t);return r.getTime() === o.getTime();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e),
        r = t.getMonth(),
        o = r - r % 3;return t.setMonth(o, 1), t.setHours(0, 0, 0, 0), t;
  };
}, function (e, t, r) {
  var n = r(61);e.exports = function (e, t) {
    var r = n(e),
        o = n(t);return r.getTime() === o.getTime();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e);return t.setMilliseconds(0), t;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e),
        o = n(t);return r.getFullYear() === o.getFullYear();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = t && Number(t.weekStartsOn) || 0,
        o = n(e),
        a = o.getDay(),
        u = 6 + (a < r ? -7 : 0) - (a - r);return o.setHours(0, 0, 0, 0), o.setDate(o.getDate() + u), o;
  };
}, function (e, t, r) {
  var n = r(1),
      o = r(19);e.exports = function (e, t) {
    var r = n(e),
        a = Number(t),
        u = r.getFullYear(),
        i = r.getDate(),
        s = new Date(0);s.setFullYear(u, a, 15), s.setHours(0, 0, 0, 0);var f = o(s);return r.setMonth(a, Math.min(i, f)), r;
  };
},,,,,,,,,,,,,,,,,,,,,,, function (e, t, r) {
  "use strict";
  r.r(t), r.d(t, "SharePrice", function () {
    return o;
  });var n = r(88);
  var o = function () {
    function o(e) {
      var _this = this;

      _classCallCheck(this, o);

      this.feed = "https://tools.eurolandir.com/tools/pricefeed/?companycode=nl-rand", this.element = e, this.getFeed().then(function (e) {
        _this.result = e, _this.renderResult();
      }).catch(function (e) {
        console.error(e);
      });
    }

    _createClass(o, [{
      key: "getFeed",
      value: function getFeed() {
        var _this2 = this;

        return new Promise(function (e, t) {
          var r = new XMLHttpRequest();r.onreadystatechange = function () {
            if (4 === r.readyState) if (200 === r.status) {
              var _n = null;try {
                _n = new DOMParser().parseFromString(r.responseText, "text/xml");
              } catch (e) {
                t(e);
              }e(_n);
            } else t(r.statusText);
          }, r.open("GET", _this2.feed, !0), r.send();
        });
      }
    }, {
      key: "renderResult",
      value: function renderResult() {
        var e = this.getResultValue("Change");this.element.classList.add(o.changeIsUp(e) ? this.classes.change.up : this.classes.change.down), this.renderResultItem("last", this.getResultValue("Last", o.formatCurrency));var t = o.formatChange(e) + " (" + this.getResultValue("ChangePercent", o.formatNumber) + " %)";this.renderResultItem("change", t), this.renderResultItem("low", this.getResultValue("Low", o.formatCurrency)), this.renderResultItem("high", this.getResultValue("High", o.formatCurrency)), this.renderResultItem("symbol", this.getResultValue("Symbol")), this.renderResultItem("date", this.getResultValue("Date", o.formatDate));
      }
    }, {
      key: "getResultValue",
      value: function getResultValue(e, t) {
        var r = this.result.getElementsByTagName(e)[0].textContent;return "function" == typeof t && (r = t.call(this, r)), r;
      }
    }, {
      key: "renderResultItem",
      value: function renderResultItem(e, t) {
        this.element.querySelector("[" + this.attributes.value + "='" + e + "']").innerText = t;
      }
    }, {
      key: "attributes",
      get: function get() {
        return { value: "data-rs-share-price-value" };
      }
    }, {
      key: "classes",
      get: function get() {
        return { change: { up: "share-price--change-up", down: "share-price--change-down" } };
      }
    }], [{
      key: "changeIsUp",
      value: function changeIsUp(e) {
        return -1 === e.indexOf("-");
      }
    }, {
      key: "formatChange",
      value: function formatChange(e) {
        return (-1 === e.indexOf("-") ? "+" : "") + " " + o.formatNumber(e);
      }
    }, {
      key: "formatNumber",
      value: function formatNumber(e) {
        return e.substring(0, e.length - 2);
      }
    }, {
      key: "formatCurrency",
      value: function formatCurrency(e) {
        return "€ " + o.formatNumber(e);
      }
    }, {
      key: "formatDate",
      value: function formatDate(e) {
        return e = e.replace(/-/g, "/"), Object(n.format)(new Date(e), "D MMM, HH:mm") + " CET";
      }
    }, {
      key: "getSelector",
      value: function getSelector() {
        return "[data-rs-share-price]";
      }
    }]);

    return o;
  }();
}, function (e, t, r) {
  e.exports = { addDays: r(7), addHours: r(30), addISOYears: r(31), addMilliseconds: r(8), addMinutes: r(33), addMonths: r(16), addQuarters: r(34), addSeconds: r(35), addWeeks: r(20), addYears: r(36), areRangesOverlapping: r(104), closestIndexTo: r(105), closestTo: r(106), compareAsc: r(10), compareDesc: r(21), differenceInCalendarDays: r(15), differenceInCalendarISOWeeks: r(107), differenceInCalendarISOYears: r(37), differenceInCalendarMonths: r(38), differenceInCalendarQuarters: r(108), differenceInCalendarWeeks: r(109), differenceInCalendarYears: r(40), differenceInDays: r(41), differenceInHours: r(110), differenceInISOYears: r(111), differenceInMilliseconds: r(17), differenceInMinutes: r(112), differenceInMonths: r(22), differenceInQuarters: r(113), differenceInSeconds: r(23), differenceInWeeks: r(114), differenceInYears: r(115), distanceInWords: r(43), distanceInWordsStrict: r(119), distanceInWordsToNow: r(120), eachDay: r(121), endOfDay: r(25), endOfHour: r(122), endOfISOWeek: r(123), endOfISOYear: r(124), endOfMinute: r(125), endOfMonth: r(45), endOfQuarter: r(126), endOfSecond: r(127), endOfToday: r(128), endOfTomorrow: r(129), endOfWeek: r(44), endOfYear: r(130), endOfYesterday: r(131), format: r(132), getDate: r(133), getDay: r(134), getDayOfYear: r(46), getDaysInMonth: r(19), getDaysInYear: r(135), getHours: r(136), getISODay: r(50), getISOWeek: r(26), getISOWeeksInYear: r(137), getISOYear: r(4), getMilliseconds: r(138), getMinutes: r(139), getMonth: r(140), getOverlappingDaysInRanges: r(141), getQuarter: r(39), getSeconds: r(142), getTime: r(143), getYear: r(144), isAfter: r(145), isBefore: r(146), isDate: r(18), isEqual: r(147), isFirstDayOfMonth: r(148), isFriday: r(149), isFuture: r(150), isLastDayOfMonth: r(151), isLeapYear: r(49), isMonday: r(152), isPast: r(153), isSameDay: r(154), isSameHour: r(51), isSameISOWeek: r(53), isSameISOYear: r(54), isSameMinute: r(55), isSameMonth: r(57), isSameQuarter: r(58), isSameSecond: r(60), isSameWeek: r(27), isSameYear: r(62), isSaturday: r(155), isSunday: r(156), isThisHour: r(157), isThisISOWeek: r(158), isThisISOYear: r(159), isThisMinute: r(160), isThisMonth: r(161), isThisQuarter: r(162), isThisSecond: r(163), isThisWeek: r(164), isThisYear: r(165), isThursday: r(166), isToday: r(167), isTomorrow: r(168), isTuesday: r(169), isValid: r(48), isWednesday: r(170), isWeekend: r(171), isWithinRange: r(172), isYesterday: r(173), lastDayOfISOWeek: r(174), lastDayOfISOYear: r(175), lastDayOfMonth: r(176), lastDayOfQuarter: r(177), lastDayOfWeek: r(63), lastDayOfYear: r(178), max: r(179), min: r(180), parse: r(1), setDate: r(181), setDay: r(182), setDayOfYear: r(183), setHours: r(184), setISODay: r(185), setISOWeek: r(186), setISOYear: r(32), setMilliseconds: r(187), setMinutes: r(188), setMonth: r(64), setQuarter: r(189), setSeconds: r(190), setYear: r(191), startOfDay: r(6), startOfHour: r(52), startOfISOWeek: r(5), startOfISOYear: r(9), startOfMinute: r(56), startOfMonth: r(192), startOfQuarter: r(59), startOfSecond: r(61), startOfToday: r(193), startOfTomorrow: r(194), startOfWeek: r(14), startOfYear: r(47), startOfYesterday: r(195), subDays: r(196), subHours: r(197), subISOYears: r(42), subMilliseconds: r(198), subMinutes: r(199), subMonths: r(200), subQuarters: r(201), subSeconds: r(202), subWeeks: r(203), subYears: r(204) };
},,,,,,,,,,,,,,, function (e, t) {
  e.exports = function (e) {
    var t = new Date(e.getTime()),
        r = t.getTimezoneOffset();return t.setSeconds(0, 0), 6e4 * r + t.getTime() % 6e4;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t, r, o) {
    var a = n(e).getTime(),
        u = n(t).getTime(),
        i = n(r).getTime(),
        s = n(o).getTime();if (a > u || i > s) throw new Error("The start of the range cannot be after the end of the range");return a < s && i < u;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    if (!(t instanceof Array)) throw new TypeError(toString.call(t) + " is not an instance of Array");var r,
        o,
        a = n(e).getTime();return t.forEach(function (e, t) {
      var u = n(e),
          i = Math.abs(a - u.getTime());(void 0 === r || i < o) && (r = t, o = i);
    }), r;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    if (!(t instanceof Array)) throw new TypeError(toString.call(t) + " is not an instance of Array");var r,
        o,
        a = n(e).getTime();return t.forEach(function (e) {
      var t = n(e),
          u = Math.abs(a - t.getTime());(void 0 === r || u < o) && (r = t, o = u);
    }), r;
  };
}, function (e, t, r) {
  var n = r(5);e.exports = function (e, t) {
    var r = n(e),
        o = n(t),
        a = r.getTime() - 6e4 * r.getTimezoneOffset(),
        u = o.getTime() - 6e4 * o.getTimezoneOffset();return Math.round((a - u) / 6048e5);
  };
}, function (e, t, r) {
  var n = r(39),
      o = r(1);e.exports = function (e, t) {
    var r = o(e),
        a = o(t);return 4 * (r.getFullYear() - a.getFullYear()) + (n(r) - n(a));
  };
}, function (e, t, r) {
  var n = r(14);e.exports = function (e, t, r) {
    var o = n(e, r),
        a = n(t, r),
        u = o.getTime() - 6e4 * o.getTimezoneOffset(),
        i = a.getTime() - 6e4 * a.getTimezoneOffset();return Math.round((u - i) / 6048e5);
  };
}, function (e, t, r) {
  var n = r(17);e.exports = function (e, t) {
    var r = n(e, t) / 36e5;return r > 0 ? Math.floor(r) : Math.ceil(r);
  };
}, function (e, t, r) {
  var n = r(1),
      o = r(37),
      a = r(10),
      u = r(42);e.exports = function (e, t) {
    var r = n(e),
        i = n(t),
        s = a(r, i),
        f = Math.abs(o(r, i));return r = u(r, s * f), s * (f - (a(r, i) === -s));
  };
}, function (e, t, r) {
  var n = r(17);e.exports = function (e, t) {
    var r = n(e, t) / 6e4;return r > 0 ? Math.floor(r) : Math.ceil(r);
  };
}, function (e, t, r) {
  var n = r(22);e.exports = function (e, t) {
    var r = n(e, t) / 3;return r > 0 ? Math.floor(r) : Math.ceil(r);
  };
}, function (e, t, r) {
  var n = r(41);e.exports = function (e, t) {
    var r = n(e, t) / 7;return r > 0 ? Math.floor(r) : Math.ceil(r);
  };
}, function (e, t, r) {
  var n = r(1),
      o = r(40),
      a = r(10);e.exports = function (e, t) {
    var r = n(e),
        u = n(t),
        i = a(r, u),
        s = Math.abs(o(r, u));return r.setFullYear(r.getFullYear() - i * s), i * (s - (a(r, u) === -i));
  };
}, function (e, t) {
  e.exports = function () {
    var e = { lessThanXSeconds: { one: "less than a second", other: "less than {{count}} seconds" }, xSeconds: { one: "1 second", other: "{{count}} seconds" }, halfAMinute: "half a minute", lessThanXMinutes: { one: "less than a minute", other: "less than {{count}} minutes" }, xMinutes: { one: "1 minute", other: "{{count}} minutes" }, aboutXHours: { one: "about 1 hour", other: "about {{count}} hours" }, xHours: { one: "1 hour", other: "{{count}} hours" }, xDays: { one: "1 day", other: "{{count}} days" }, aboutXMonths: { one: "about 1 month", other: "about {{count}} months" }, xMonths: { one: "1 month", other: "{{count}} months" }, aboutXYears: { one: "about 1 year", other: "about {{count}} years" }, xYears: { one: "1 year", other: "{{count}} years" }, overXYears: { one: "over 1 year", other: "over {{count}} years" }, almostXYears: { one: "almost 1 year", other: "almost {{count}} years" } };return { localize: function localize(t, r, n) {
        var o;return n = n || {}, o = "string" == typeof e[t] ? e[t] : 1 === r ? e[t].one : e[t].other.replace("{{count}}", r), n.addSuffix ? n.comparison > 0 ? "in " + o : o + " ago" : o;
      } };
  };
}, function (e, t, r) {
  var n = r(118);e.exports = function () {
    var e = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        t = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        r = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
        o = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        a = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        u = ["AM", "PM"],
        i = ["am", "pm"],
        s = ["a.m.", "p.m."],
        f = { MMM: function MMM(t) {
        return e[t.getMonth()];
      }, MMMM: function MMMM(e) {
        return t[e.getMonth()];
      }, dd: function dd(e) {
        return r[e.getDay()];
      }, ddd: function ddd(e) {
        return o[e.getDay()];
      }, dddd: function dddd(e) {
        return a[e.getDay()];
      }, A: function A(e) {
        return e.getHours() / 12 >= 1 ? u[1] : u[0];
      }, a: function a(e) {
        return e.getHours() / 12 >= 1 ? i[1] : i[0];
      }, aa: function aa(e) {
        return e.getHours() / 12 >= 1 ? s[1] : s[0];
      } };return ["M", "D", "DDD", "d", "Q", "W"].forEach(function (e) {
      f[e + "o"] = function (t, r) {
        return function (e) {
          var t = e % 100;if (t > 20 || t < 10) switch (t % 10) {case 1:
              return e + "st";case 2:
              return e + "nd";case 3:
              return e + "rd";}return e + "th";
        }(r[e](t));
      };
    }), { formatters: f, formattingTokensRegExp: n(f) };
  };
}, function (e, t) {
  var r = ["M", "MM", "Q", "D", "DD", "DDD", "DDDD", "d", "E", "W", "WW", "YY", "YYYY", "GG", "GGGG", "H", "HH", "h", "hh", "m", "mm", "s", "ss", "S", "SS", "SSS", "Z", "ZZ", "X", "x"];e.exports = function (e) {
    var t = [];for (var n in e) {
      e.hasOwnProperty(n) && t.push(n);
    }var o = r.concat(t).sort().reverse();return new RegExp("(\\[[^\\[]*\\])|(\\\\)?(" + o.join("|") + "|.)", "g");
  };
}, function (e, t, r) {
  var n = r(21),
      o = r(1),
      a = r(23),
      u = r(24);e.exports = function (e, t, r) {
    var i = r || {},
        s = n(e, t),
        f = i.locale,
        c = u.distanceInWords.localize;f && f.distanceInWords && f.distanceInWords.localize && (c = f.distanceInWords.localize);var l,
        v,
        g,
        d = { addSuffix: Boolean(i.addSuffix), comparison: s };s > 0 ? (l = o(e), v = o(t)) : (l = o(t), v = o(e));var p = Math[i.partialMethod ? String(i.partialMethod) : "floor"],
        m = a(v, l),
        h = v.getTimezoneOffset() - l.getTimezoneOffset(),
        x = p(m / 60) - h;if ("s" === (g = i.unit ? String(i.unit) : x < 1 ? "s" : x < 60 ? "m" : x < 1440 ? "h" : x < 43200 ? "d" : x < 525600 ? "M" : "Y")) return c("xSeconds", m, d);if ("m" === g) return c("xMinutes", x, d);if ("h" === g) return c("xHours", p(x / 60), d);if ("d" === g) return c("xDays", p(x / 1440), d);if ("M" === g) return c("xMonths", p(x / 43200), d);if ("Y" === g) return c("xYears", p(x / 525600), d);throw new Error("Unknown unit: " + g);
  };
}, function (e, t, r) {
  var n = r(43);e.exports = function (e, t) {
    return n(Date.now(), e, t);
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t, r) {
    var o = n(e),
        a = void 0 !== r ? r : 1,
        u = n(t).getTime();if (o.getTime() > u) throw new Error("The first date cannot be after the second date");var i = [],
        s = o;for (s.setHours(0, 0, 0, 0); s.getTime() <= u;) {
      i.push(n(s)), s.setDate(s.getDate() + a);
    }return i;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e);return t.setMinutes(59, 59, 999), t;
  };
}, function (e, t, r) {
  var n = r(44);e.exports = function (e) {
    return n(e, { weekStartsOn: 1 });
  };
}, function (e, t, r) {
  var n = r(4),
      o = r(5);e.exports = function (e) {
    var t = n(e),
        r = new Date(0);r.setFullYear(t + 1, 0, 4), r.setHours(0, 0, 0, 0);var a = o(r);return a.setMilliseconds(a.getMilliseconds() - 1), a;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e);return t.setSeconds(59, 999), t;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e),
        r = t.getMonth(),
        o = r - r % 3 + 3;return t.setMonth(o, 0), t.setHours(23, 59, 59, 999), t;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e);return t.setMilliseconds(999), t;
  };
}, function (e, t, r) {
  var n = r(25);e.exports = function () {
    return n(new Date());
  };
}, function (e, t) {
  e.exports = function () {
    var e = new Date(),
        t = e.getFullYear(),
        r = e.getMonth(),
        n = e.getDate(),
        o = new Date(0);return o.setFullYear(t, r, n + 1), o.setHours(23, 59, 59, 999), o;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e),
        r = t.getFullYear();return t.setFullYear(r + 1, 0, 0), t.setHours(23, 59, 59, 999), t;
  };
}, function (e, t) {
  e.exports = function () {
    var e = new Date(),
        t = e.getFullYear(),
        r = e.getMonth(),
        n = e.getDate(),
        o = new Date(0);return o.setFullYear(t, r, n - 1), o.setHours(23, 59, 59, 999), o;
  };
}, function (e, t, r) {
  var n = r(46),
      o = r(26),
      a = r(4),
      u = r(1),
      i = r(48),
      s = r(24);var f = { M: function M(e) {
      return e.getMonth() + 1;
    }, MM: function MM(e) {
      return v(e.getMonth() + 1, 2);
    }, Q: function Q(e) {
      return Math.ceil((e.getMonth() + 1) / 3);
    }, D: function D(e) {
      return e.getDate();
    }, DD: function DD(e) {
      return v(e.getDate(), 2);
    }, DDD: function DDD(e) {
      return n(e);
    }, DDDD: function DDDD(e) {
      return v(n(e), 3);
    }, d: function d(e) {
      return e.getDay();
    }, E: function E(e) {
      return e.getDay() || 7;
    }, W: function W(e) {
      return o(e);
    }, WW: function WW(e) {
      return v(o(e), 2);
    }, YY: function YY(e) {
      return v(e.getFullYear(), 4).substr(2);
    }, YYYY: function YYYY(e) {
      return v(e.getFullYear(), 4);
    }, GG: function GG(e) {
      return String(a(e)).substr(2);
    }, GGGG: function GGGG(e) {
      return a(e);
    }, H: function H(e) {
      return e.getHours();
    }, HH: function HH(e) {
      return v(e.getHours(), 2);
    }, h: function h(e) {
      var t = e.getHours();return 0 === t ? 12 : t > 12 ? t % 12 : t;
    }, hh: function hh(e) {
      return v(f.h(e), 2);
    }, m: function m(e) {
      return e.getMinutes();
    }, mm: function mm(e) {
      return v(e.getMinutes(), 2);
    }, s: function s(e) {
      return e.getSeconds();
    }, ss: function ss(e) {
      return v(e.getSeconds(), 2);
    }, S: function S(e) {
      return Math.floor(e.getMilliseconds() / 100);
    }, SS: function SS(e) {
      return v(Math.floor(e.getMilliseconds() / 10), 2);
    }, SSS: function SSS(e) {
      return v(e.getMilliseconds(), 3);
    }, Z: function Z(e) {
      return l(e.getTimezoneOffset(), ":");
    }, ZZ: function ZZ(e) {
      return l(e.getTimezoneOffset());
    }, X: function X(e) {
      return Math.floor(e.getTime() / 1e3);
    }, x: function x(e) {
      return e.getTime();
    } };function c(e) {
    return e.match(/\[[\s\S]/) ? e.replace(/^\[|]$/g, "") : e.replace(/\\/g, "");
  }function l(e, t) {
    t = t || "";var r = e > 0 ? "-" : "+",
        n = Math.abs(e),
        o = n % 60;return r + v(Math.floor(n / 60), 2) + t + v(o, 2);
  }function v(e, t) {
    for (var r = Math.abs(e).toString(); r.length < t;) {
      r = "0" + r;
    }return r;
  }e.exports = function (e, t, r) {
    var n = t ? String(t) : "YYYY-MM-DDTHH:mm:ss.SSSZ",
        o = (r || {}).locale,
        a = s.format.formatters,
        l = s.format.formattingTokensRegExp;o && o.format && o.format.formatters && (a = o.format.formatters, o.format.formattingTokensRegExp && (l = o.format.formattingTokensRegExp));var v = u(e);return i(v) ? function (e, t, r) {
      var n,
          o,
          a = e.match(r),
          u = a.length;for (n = 0; n < u; n++) {
        o = t[a[n]] || f[a[n]], a[n] = o || c(a[n]);
      }return function (e) {
        for (var t = "", r = 0; r < u; r++) {
          a[r] instanceof Function ? t += a[r](e, f) : t += a[r];
        }return t;
      };
    }(n, a, l)(v) : "Invalid Date";
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return n(e).getDate();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return n(e).getDay();
  };
}, function (e, t, r) {
  var n = r(49);e.exports = function (e) {
    return n(e) ? 366 : 365;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return n(e).getHours();
  };
}, function (e, t, r) {
  var n = r(9),
      o = r(20);e.exports = function (e) {
    var t = n(e),
        r = n(o(t, 60)).valueOf() - t.valueOf();return Math.round(r / 6048e5);
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return n(e).getMilliseconds();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return n(e).getMinutes();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return n(e).getMonth();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t, r, o) {
    var a = n(e).getTime(),
        u = n(t).getTime(),
        i = n(r).getTime(),
        s = n(o).getTime();if (a > u || i > s) throw new Error("The start of the range cannot be after the end of the range");if (!(a < s && i < u)) return 0;var f = (s > u ? u : s) - (i < a ? a : i);return Math.ceil(f / 864e5);
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return n(e).getSeconds();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return n(e).getTime();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return n(e).getFullYear();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e),
        o = n(t);return r.getTime() > o.getTime();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e),
        o = n(t);return r.getTime() < o.getTime();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e),
        o = n(t);return r.getTime() === o.getTime();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return 1 === n(e).getDate();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return 5 === n(e).getDay();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return n(e).getTime() > new Date().getTime();
  };
}, function (e, t, r) {
  var n = r(1),
      o = r(25),
      a = r(45);e.exports = function (e) {
    var t = n(e);return o(t).getTime() === a(t).getTime();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return 1 === n(e).getDay();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return n(e).getTime() < new Date().getTime();
  };
}, function (e, t, r) {
  var n = r(6);e.exports = function (e, t) {
    var r = n(e),
        o = n(t);return r.getTime() === o.getTime();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return 6 === n(e).getDay();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return 0 === n(e).getDay();
  };
}, function (e, t, r) {
  var n = r(51);e.exports = function (e) {
    return n(new Date(), e);
  };
}, function (e, t, r) {
  var n = r(53);e.exports = function (e) {
    return n(new Date(), e);
  };
}, function (e, t, r) {
  var n = r(54);e.exports = function (e) {
    return n(new Date(), e);
  };
}, function (e, t, r) {
  var n = r(55);e.exports = function (e) {
    return n(new Date(), e);
  };
}, function (e, t, r) {
  var n = r(57);e.exports = function (e) {
    return n(new Date(), e);
  };
}, function (e, t, r) {
  var n = r(58);e.exports = function (e) {
    return n(new Date(), e);
  };
}, function (e, t, r) {
  var n = r(60);e.exports = function (e) {
    return n(new Date(), e);
  };
}, function (e, t, r) {
  var n = r(27);e.exports = function (e, t) {
    return n(new Date(), e, t);
  };
}, function (e, t, r) {
  var n = r(62);e.exports = function (e) {
    return n(new Date(), e);
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return 4 === n(e).getDay();
  };
}, function (e, t, r) {
  var n = r(6);e.exports = function (e) {
    return n(e).getTime() === n(new Date()).getTime();
  };
}, function (e, t, r) {
  var n = r(6);e.exports = function (e) {
    var t = new Date();return t.setDate(t.getDate() + 1), n(e).getTime() === n(t).getTime();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return 2 === n(e).getDay();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    return 3 === n(e).getDay();
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e).getDay();return 0 === t || 6 === t;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t, r) {
    var o = n(e).getTime(),
        a = n(t).getTime(),
        u = n(r).getTime();if (a > u) throw new Error("The start of the range cannot be after the end of the range");return o >= a && o <= u;
  };
}, function (e, t, r) {
  var n = r(6);e.exports = function (e) {
    var t = new Date();return t.setDate(t.getDate() - 1), n(e).getTime() === n(t).getTime();
  };
}, function (e, t, r) {
  var n = r(63);e.exports = function (e) {
    return n(e, { weekStartsOn: 1 });
  };
}, function (e, t, r) {
  var n = r(4),
      o = r(5);e.exports = function (e) {
    var t = n(e),
        r = new Date(0);r.setFullYear(t + 1, 0, 4), r.setHours(0, 0, 0, 0);var a = o(r);return a.setDate(a.getDate() - 1), a;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e),
        r = t.getMonth();return t.setFullYear(t.getFullYear(), r + 1, 0), t.setHours(0, 0, 0, 0), t;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e),
        r = t.getMonth(),
        o = r - r % 3 + 3;return t.setMonth(o, 0), t.setHours(0, 0, 0, 0), t;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e),
        r = t.getFullYear();return t.setFullYear(r + 1, 0, 0), t.setHours(0, 0, 0, 0), t;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function () {
    var e = Array.prototype.slice.call(arguments),
        t = e.map(function (e) {
      return n(e);
    }),
        r = Math.max.apply(null, t);return new Date(r);
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function () {
    var e = Array.prototype.slice.call(arguments),
        t = e.map(function (e) {
      return n(e);
    }),
        r = Math.min.apply(null, t);return new Date(r);
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e),
        o = Number(t);return r.setDate(o), r;
  };
}, function (e, t, r) {
  var n = r(1),
      o = r(7);e.exports = function (e, t, r) {
    var a = r && Number(r.weekStartsOn) || 0,
        u = n(e),
        i = Number(t),
        s = u.getDay();return o(u, ((i % 7 + 7) % 7 < a ? 7 : 0) + i - s);
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e),
        o = Number(t);return r.setMonth(0), r.setDate(o), r;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e),
        o = Number(t);return r.setHours(o), r;
  };
}, function (e, t, r) {
  var n = r(1),
      o = r(7),
      a = r(50);e.exports = function (e, t) {
    var r = n(e),
        u = Number(t),
        i = a(r);return o(r, u - i);
  };
}, function (e, t, r) {
  var n = r(1),
      o = r(26);e.exports = function (e, t) {
    var r = n(e),
        a = Number(t),
        u = o(r) - a;return r.setDate(r.getDate() - 7 * u), r;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e),
        o = Number(t);return r.setMilliseconds(o), r;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e),
        o = Number(t);return r.setMinutes(o), r;
  };
}, function (e, t, r) {
  var n = r(1),
      o = r(64);e.exports = function (e, t) {
    var r = n(e),
        a = Number(t) - (Math.floor(r.getMonth() / 3) + 1);return o(r, r.getMonth() + 3 * a);
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e),
        o = Number(t);return r.setSeconds(o), r;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e, t) {
    var r = n(e),
        o = Number(t);return r.setFullYear(o), r;
  };
}, function (e, t, r) {
  var n = r(1);e.exports = function (e) {
    var t = n(e);return t.setDate(1), t.setHours(0, 0, 0, 0), t;
  };
}, function (e, t, r) {
  var n = r(6);e.exports = function () {
    return n(new Date());
  };
}, function (e, t) {
  e.exports = function () {
    var e = new Date(),
        t = e.getFullYear(),
        r = e.getMonth(),
        n = e.getDate(),
        o = new Date(0);return o.setFullYear(t, r, n + 1), o.setHours(0, 0, 0, 0), o;
  };
}, function (e, t) {
  e.exports = function () {
    var e = new Date(),
        t = e.getFullYear(),
        r = e.getMonth(),
        n = e.getDate(),
        o = new Date(0);return o.setFullYear(t, r, n - 1), o.setHours(0, 0, 0, 0), o;
  };
}, function (e, t, r) {
  var n = r(7);e.exports = function (e, t) {
    var r = Number(t);return n(e, -r);
  };
}, function (e, t, r) {
  var n = r(30);e.exports = function (e, t) {
    var r = Number(t);return n(e, -r);
  };
}, function (e, t, r) {
  var n = r(8);e.exports = function (e, t) {
    var r = Number(t);return n(e, -r);
  };
}, function (e, t, r) {
  var n = r(33);e.exports = function (e, t) {
    var r = Number(t);return n(e, -r);
  };
}, function (e, t, r) {
  var n = r(16);e.exports = function (e, t) {
    var r = Number(t);return n(e, -r);
  };
}, function (e, t, r) {
  var n = r(34);e.exports = function (e, t) {
    var r = Number(t);return n(e, -r);
  };
}, function (e, t, r) {
  var n = r(35);e.exports = function (e, t) {
    var r = Number(t);return n(e, -r);
  };
}, function (e, t, r) {
  var n = r(20);e.exports = function (e, t) {
    var r = Number(t);return n(e, -r);
  };
}, function (e, t, r) {
  var n = r(36);e.exports = function (e, t) {
    var r = Number(t);return n(e, -r);
  };
},,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,, function (e, t, r) {
  e.exports = r(87);
}]));