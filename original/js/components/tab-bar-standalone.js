"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (t, e) {
  for (var i in e) {
    t[i] = e[i];
  }
}(exports, function (t) {
  var e = {};function i(l) {
    if (e[l]) return e[l].exports;var s = e[l] = { i: l, l: !1, exports: {} };return t[l].call(s.exports, s, s.exports, i), s.l = !0, s.exports;
  }return i.m = t, i.c = e, i.d = function (t, e, l) {
    i.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: l });
  }, i.r = function (t) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 });
  }, i.t = function (t, e) {
    if (1 & e && (t = i(t)), 8 & e) return t;if (4 & e && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t && t.__esModule) return t;var l = Object.create(null);if (i.r(l), Object.defineProperty(l, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t) for (var s in t) {
      i.d(l, s, function (e) {
        return t[e];
      }.bind(null, s));
    }return l;
  }, i.n = function (t) {
    var e = t && t.__esModule ? function () {
      return t.default;
    } : function () {
      return t;
    };return i.d(e, "a", e), e;
  }, i.o = function (t, e) {
    return Object.prototype.hasOwnProperty.call(t, e);
  }, i.p = "", i(i.s = 287);
}({ 0: function _(t, e) {
    t.exports = function () {
      function _class() {
        _classCallCheck(this, _class);
      }

      _createClass(_class, null, [{
        key: "triggerEvent",
        value: function triggerEvent(t, e) {
          var i = void 0;"function" == typeof Event ? i = new Event(e) : (i = document.createEvent("Event"), i.initEvent(e, !0, !0)), t.dispatchEvent(i);
        }
      }, {
        key: "getElementByAttribute",
        value: function getElementByAttribute(t, e) {
          return e ? document.querySelector("[" + t + "=\"" + e + "\"]") : document.querySelector("[" + t + "]");
        }
      }, {
        key: "getElementsByAttribute",
        value: function getElementsByAttribute(t, e) {
          return e ? document.querySelectorAll("[" + t + "=\"" + e + "\"]") : document.querySelectorAll("[" + t + "]");
        }
      }, {
        key: "getElementByAttributeWithinElement",
        value: function getElementByAttributeWithinElement(t, e, i) {
          return i ? t.querySelector("[" + e + "=\"" + i + "\"]") : t.querySelector("[" + e + "]");
        }
      }, {
        key: "isMouseOver",
        value: function isMouseOver(t) {
          return Array.prototype.slice.call(t.parentElement.querySelectorAll(":hover")).filter(function () {
            return t[0] == this;
          }).length > 0;
        }
      }, {
        key: "getElementsByAttributeWithinElement",
        value: function getElementsByAttributeWithinElement(t, e, i) {
          return i ? t.querySelectorAll("[" + e + "=\"" + i + "\"]") : t.querySelectorAll("[" + e + "]");
        }
      }, {
        key: "getElementByClassWithinElement",
        value: function getElementByClassWithinElement(t, e) {
          return t.querySelector("." + e);
        }
      }, {
        key: "maxWidthMobileViewports",
        value: function maxWidthMobileViewports() {
          return 940;
        }
      }, {
        key: "createEvent",
        value: function createEvent(t) {
          var e = void 0;return "function" == typeof Event ? e = new Event(t) : (e = document.createEvent("Event"), e.initEvent(t, !0, !0)), e;
        }
      }, {
        key: "isDesktop",
        value: function isDesktop() {
          return Math.max(document.documentElement.clientWidth, window.innerWidth || 0) > this.maxWidthMobileViewports();
        }
      }, {
        key: "isIE11",
        value: function isIE11() {
          return -1 !== navigator.userAgent.indexOf("MSIE") || navigator.appVersion.indexOf("Trident/") > -1;
        }
      }, {
        key: "getKeyCodeOnKeyDownEvent",
        value: function getKeyCodeOnKeyDownEvent(t) {
          var e = void 0;if (null != t) if (void 0 === t.code) switch (t.keyCode) {case 13:
              e = "Enter";break;case 38:
              e = "ArrowUp";break;case 40:
              e = "ArrowDown";break;case 9:
              e = "Tab";break;case 27:
              e = "Escape";break;case 33:
              e = "PageUp";break;case 34:
              e = "PageDown";break;default:
              e = void 0;} else e = t.code;return e;
        }
      }]);

      return _class;
    }();
  }, 287: function _(t, e, i) {
    t.exports = i(288);
  }, 288: function _(t, e, i) {
    "use strict";
    i.r(e);var l = i(91);var s = document.querySelectorAll(l.TabBar.getSelector());for (var _t = 0; _t < s.length; _t++) {
      new l.TabBar(s[_t]);
    }
  }, 91: function _(t, e, i) {
    "use strict";
    i.r(e), i.d(e, "TabBar", function () {
      return r;
    });var l = i(0),
        s = i.n(l),
        o = i(92),
        n = i.n(o);
    var r = function () {
      function r(t) {
        _classCallCheck(this, r);

        n.a.polyfill(), this.element = t, this.items = s.a.getElementsByAttributeWithinElement(this.element, this.attributes.item), this.setVariables(), this.setFadeElements(), this.addEventListeners(), this.toggleFadeElements(), this.scrollToActive();
      }

      _createClass(r, [{
        key: "setVariables",
        value: function setVariables() {
          this.chevronRight = '<svg id="chevron-right" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">\n  \t\t\t\t<path d="M7,2 C7.256,2 7.512,2.098 7.707,2.293 L14.707,9.293 C15.098,9.684 15.098,10.316 14.707,10.707 L7.707,17.707 C7.316,18.098 6.684,18.098 6.293,17.707 C5.902,17.316 5.902,16.684 6.293,16.293 L12.586,10 L6.293,3.707 C5.902,3.316 5.902,2.684 6.293,2.293 C6.488,2.098 6.744,2 7,2"/>\n\t\t\t\t</svg>', this.chevronLeft = '<svg id="chevron-left" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">\n  \t\t\t\t<path d="M13,18 C12.744,18 12.488,17.902 12.293,17.707 L5.293,10.707 C4.902,10.316 4.902,9.684 5.293,9.293 L12.293,2.293 C12.684,1.902 13.316,1.902 13.707,2.293 C14.098,2.684 14.098,3.316 13.707,3.707 L7.414,10 L13.707,16.293 C14.098,16.684 14.098,17.316 13.707,17.707 C13.512,17.902 13.256,18 13,18"/>\n\t\t\t\t</svg>', this.fadeLeft = "<div class=\"" + this.classes.fadeElement + " " + this.classes.fadeElementLeft + " " + this.classes.hide + "\" " + this.attributes.fadeElementLeft + "><div " + this.attributes.iconLeft + " class=\"" + this.classes.icon + " " + this.classes.iconLeft + "\">" + this.chevronLeft + "</div></div>", this.fadeRight = "<div class=\"" + this.classes.fadeElement + " " + this.classes.fadeElementRight + " " + this.classes.hide + "\" " + this.attributes.fadeElementRight + "><div " + this.attributes.iconRight + " class=\"" + this.classes.icon + " " + this.classes.iconRight + "\">" + this.chevronRight + "</div></div>";
        }
      }, {
        key: "setFadeElements",
        value: function setFadeElements() {
          var _this = this;

          this.items.forEach(function (t, e, i) {
            1 === e && t.insertAdjacentHTML("beforebegin", _this.fadeLeft), e === i.length - 1 && t.insertAdjacentHTML("afterend", _this.fadeRight);
          });
        }
      }, {
        key: "addEventListeners",
        value: function addEventListeners() {
          var _this2 = this;

          this.iconLeft = s.a.getElementByAttributeWithinElement(this.element, this.attributes.iconLeft), this.iconRight = s.a.getElementByAttributeWithinElement(this.element, this.attributes.iconRight), this.iconLeft.addEventListener("click", function () {
            _this2.element.scrollBy({ left: -_this2.element.clientWidth / 2, behavior: "smooth" });
          }), this.iconRight.addEventListener("click", function () {
            _this2.element.scrollBy({ left: _this2.element.clientWidth / 2, behavior: "smooth" });
          }), this.element.addEventListener("scroll", function () {
            _this2.toggleFadeElements();
          }), window.addEventListener("resize", function () {
            _this2.toggleFadeElements(), _this2.scrollToActive();
          }), this.items.forEach(function (t) {
            t.addEventListener("click", function (t) {
              t.preventDefault, _this2.setActiveClass(t);
            });
          });
        }
      }, {
        key: "setActiveClass",
        value: function setActiveClass(t) {
          var _this3 = this;

          this.items.forEach(function (t) {
            t.classList.remove(_this3.classes.active);
          }), t.currentTarget.classList.add(this.classes.active), this.scrollToActive();
        }
      }, {
        key: "scrollToActive",
        value: function scrollToActive() {
          this.activeElement = s.a.getElementByClassWithinElement(this.element, this.classes.active);var t = this.element.getBoundingClientRect(),
              e = this.activeElement.getBoundingClientRect(),
              i = window.innerWidth - 2 * t.left;e.left + this.activeElement.clientWidth > i ? this.element.scrollBy({ behavior: "smooth", left: e.left - 2 * t.left }) : e.left < this.activeElement.clientWidth + 50 && this.element.scrollBy({ behavior: "smooth", left: e.left - i / 2 });
        }
      }, {
        key: "toggleFadeElements",
        value: function toggleFadeElements() {
          this.fadeElementRight = s.a.getElementByAttributeWithinElement(this.element, this.attributes.fadeElementRight), this.fadeElementLeft = s.a.getElementByAttributeWithinElement(this.element, this.attributes.fadeElementLeft);var t = this.element.scrollWidth - this.element.clientWidth;this.screenCenter = window.innerWidth / 2, this.tabBarWidth = this.element.clientWidth, this.positionLeft = this.screenCenter - this.tabBarWidth / 2, this.positionRight = this.screenCenter + this.tabBarWidth / 2, this.element.scrollLeft > t - 20 ? this.fadeElementRight.classList.add(this.classes.hide) : (this.fadeElementRight.classList.remove(this.classes.hide), this.fadeElementRight.style.left = this.positionRight - 25 + "px"), this.element.scrollLeft > 10 ? (this.fadeElementLeft.classList.remove(this.classes.hide), this.fadeElementLeft.style.left = this.positionLeft - 25 + "px") : this.fadeElementLeft.classList.add(this.classes.hide);
        }
      }, {
        key: "classes",
        get: function get() {
          return { fadeElement: "fade-element", fadeElementRight: "fade-element--right", fadeElementLeft: "fade-element--left", icon: "icon", iconLeft: "icon__left", iconRight: "icon__right", hide: "hide", active: "active" };
        }
      }, {
        key: "attributes",
        get: function get() {
          return { item: "data-rs-tab-bar-item", iconLeft: "data-rs-tab-bar-icon-left", iconRight: "data-rs-tab-bar-icon-right", fadeElementLeft: "data-rs-tab-bar-fade-element-left", fadeElementRight: "data-rs-tab-bar-fade-element-right" };
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[data-rs-tab-bar]";
        }
      }]);

      return r;
    }();
  }, 92: function _(t, e, i) {
    !function () {
      "use strict";
      t.exports = { polyfill: function polyfill() {
          var t = window,
              e = document;if (!("scrollBehavior" in e.documentElement.style) || !0 === t.__forceSmoothScrollPolyfill__) {
            var i,
                l = t.HTMLElement || t.Element,
                s = { scroll: t.scroll || t.scrollTo, scrollBy: t.scrollBy, elementScroll: l.prototype.scroll || r, scrollIntoView: l.prototype.scrollIntoView },
                o = t.performance && t.performance.now ? t.performance.now.bind(t.performance) : Date.now,
                n = (i = t.navigator.userAgent, new RegExp(["MSIE ", "Trident/", "Edge/"].join("|")).test(i) ? 1 : 0);t.scroll = t.scrollTo = function () {
              void 0 !== arguments[0] && (!0 !== c(arguments[0]) ? m.call(t, e.body, void 0 !== arguments[0].left ? ~~arguments[0].left : t.scrollX || t.pageXOffset, void 0 !== arguments[0].top ? ~~arguments[0].top : t.scrollY || t.pageYOffset) : s.scroll.call(t, void 0 !== arguments[0].left ? arguments[0].left : "object" != _typeof(arguments[0]) ? arguments[0] : t.scrollX || t.pageXOffset, void 0 !== arguments[0].top ? arguments[0].top : void 0 !== arguments[1] ? arguments[1] : t.scrollY || t.pageYOffset));
            }, t.scrollBy = function () {
              void 0 !== arguments[0] && (c(arguments[0]) ? s.scrollBy.call(t, void 0 !== arguments[0].left ? arguments[0].left : "object" != _typeof(arguments[0]) ? arguments[0] : 0, void 0 !== arguments[0].top ? arguments[0].top : void 0 !== arguments[1] ? arguments[1] : 0) : m.call(t, e.body, ~~arguments[0].left + (t.scrollX || t.pageXOffset), ~~arguments[0].top + (t.scrollY || t.pageYOffset)));
            }, l.prototype.scroll = l.prototype.scrollTo = function () {
              if (void 0 !== arguments[0]) if (!0 !== c(arguments[0])) {
                var t = arguments[0].left,
                    e = arguments[0].top;m.call(this, this, void 0 === t ? this.scrollLeft : ~~t, void 0 === e ? this.scrollTop : ~~e);
              } else {
                if ("number" == typeof arguments[0] && void 0 === arguments[1]) throw new SyntaxError("Value could not be converted");s.elementScroll.call(this, void 0 !== arguments[0].left ? ~~arguments[0].left : "object" != _typeof(arguments[0]) ? ~~arguments[0] : this.scrollLeft, void 0 !== arguments[0].top ? ~~arguments[0].top : void 0 !== arguments[1] ? ~~arguments[1] : this.scrollTop);
              }
            }, l.prototype.scrollBy = function () {
              void 0 !== arguments[0] && (!0 !== c(arguments[0]) ? this.scroll({ left: ~~arguments[0].left + this.scrollLeft, top: ~~arguments[0].top + this.scrollTop, behavior: arguments[0].behavior }) : s.elementScroll.call(this, void 0 !== arguments[0].left ? ~~arguments[0].left + this.scrollLeft : ~~arguments[0] + this.scrollLeft, void 0 !== arguments[0].top ? ~~arguments[0].top + this.scrollTop : ~~arguments[1] + this.scrollTop));
            }, l.prototype.scrollIntoView = function () {
              if (!0 !== c(arguments[0])) {
                var i = d(this),
                    l = i.getBoundingClientRect(),
                    o = this.getBoundingClientRect();i !== e.body ? (m.call(this, i, i.scrollLeft + o.left - l.left, i.scrollTop + o.top - l.top), "fixed" !== t.getComputedStyle(i).position && t.scrollBy({ left: l.left, top: l.top, behavior: "smooth" })) : t.scrollBy({ left: o.left, top: o.top, behavior: "smooth" });
              } else s.scrollIntoView.call(this, void 0 === arguments[0] || arguments[0]);
            };
          }function r(t, e) {
            this.scrollLeft = t, this.scrollTop = e;
          }function c(t) {
            if (null === t || "object" != (typeof t === "undefined" ? "undefined" : _typeof(t)) || void 0 === t.behavior || "auto" === t.behavior || "instant" === t.behavior) return !0;if ("object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && "smooth" === t.behavior) return !1;throw new TypeError("behavior member of ScrollOptions " + t.behavior + " is not a valid value for enumeration ScrollBehavior.");
          }function a(t, e) {
            return "Y" === e ? t.clientHeight + n < t.scrollHeight : "X" === e ? t.clientWidth + n < t.scrollWidth : void 0;
          }function h(e, i) {
            var l = t.getComputedStyle(e, null)["overflow" + i];return "auto" === l || "scroll" === l;
          }function f(t) {
            var e = a(t, "Y") && h(t, "Y"),
                i = a(t, "X") && h(t, "X");return e || i;
          }function d(t) {
            for (; t !== e.body && !1 === f(t);) {
              t = t.parentNode || t.host;
            }return t;
          }function u(e) {
            var i,
                l,
                s,
                n,
                r = (o() - e.startTime) / 468;n = r = r > 1 ? 1 : r, i = .5 * (1 - Math.cos(Math.PI * n)), l = e.startX + (e.x - e.startX) * i, s = e.startY + (e.y - e.startY) * i, e.method.call(e.scrollable, l, s), l === e.x && s === e.y || t.requestAnimationFrame(u.bind(t, e));
          }function m(i, l, n) {
            var c,
                a,
                h,
                f,
                d = o();i === e.body ? (c = t, a = t.scrollX || t.pageXOffset, h = t.scrollY || t.pageYOffset, f = s.scroll) : (c = i, a = i.scrollLeft, h = i.scrollTop, f = r), u({ scrollable: c, method: f, startTime: d, startX: a, startY: h, x: l, y: n });
          }
        } };
    }();
  } }));