"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (t, e) {
  for (var n in e) {
    t[n] = e[n];
  }
}(exports, function (t) {
  var e = {};function n(i) {
    if (e[i]) return e[i].exports;var r = e[i] = { i: i, l: !1, exports: {} };return t[i].call(r.exports, r, r.exports, n), r.l = !0, r.exports;
  }return n.m = t, n.c = e, n.d = function (t, e, i) {
    n.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: i });
  }, n.r = function (t) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 });
  }, n.t = function (t, e) {
    if (1 & e && (t = n(t)), 8 & e) return t;if (4 & e && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t && t.__esModule) return t;var i = Object.create(null);if (n.r(i), Object.defineProperty(i, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t) for (var r in t) {
      n.d(i, r, function (e) {
        return t[e];
      }.bind(null, r));
    }return i;
  }, n.n = function (t) {
    var e = t && t.__esModule ? function () {
      return t.default;
    } : function () {
      return t;
    };return n.d(e, "a", e), e;
  }, n.o = function (t, e) {
    return Object.prototype.hasOwnProperty.call(t, e);
  }, n.p = "", n(n.s = 237);
}({ 0: function _(t, e) {
    t.exports = function () {
      function _class() {
        _classCallCheck(this, _class);
      }

      _createClass(_class, null, [{
        key: "triggerEvent",
        value: function triggerEvent(t, e) {
          var n = void 0;"function" == typeof Event ? n = new Event(e) : (n = document.createEvent("Event"), n.initEvent(e, !0, !0)), t.dispatchEvent(n);
        }
      }, {
        key: "getElementByAttribute",
        value: function getElementByAttribute(t, e) {
          return e ? document.querySelector("[" + t + "=\"" + e + "\"]") : document.querySelector("[" + t + "]");
        }
      }, {
        key: "getElementsByAttribute",
        value: function getElementsByAttribute(t, e) {
          return e ? document.querySelectorAll("[" + t + "=\"" + e + "\"]") : document.querySelectorAll("[" + t + "]");
        }
      }, {
        key: "getElementByAttributeWithinElement",
        value: function getElementByAttributeWithinElement(t, e, n) {
          return n ? t.querySelector("[" + e + "=\"" + n + "\"]") : t.querySelector("[" + e + "]");
        }
      }, {
        key: "isMouseOver",
        value: function isMouseOver(t) {
          return Array.prototype.slice.call(t.parentElement.querySelectorAll(":hover")).filter(function () {
            return t[0] == this;
          }).length > 0;
        }
      }, {
        key: "getElementsByAttributeWithinElement",
        value: function getElementsByAttributeWithinElement(t, e, n) {
          return n ? t.querySelectorAll("[" + e + "=\"" + n + "\"]") : t.querySelectorAll("[" + e + "]");
        }
      }, {
        key: "getElementByClassWithinElement",
        value: function getElementByClassWithinElement(t, e) {
          return t.querySelector("." + e);
        }
      }, {
        key: "maxWidthMobileViewports",
        value: function maxWidthMobileViewports() {
          return 940;
        }
      }, {
        key: "createEvent",
        value: function createEvent(t) {
          var e = void 0;return "function" == typeof Event ? e = new Event(t) : (e = document.createEvent("Event"), e.initEvent(t, !0, !0)), e;
        }
      }, {
        key: "isDesktop",
        value: function isDesktop() {
          return Math.max(document.documentElement.clientWidth, window.innerWidth || 0) > this.maxWidthMobileViewports();
        }
      }, {
        key: "isIE11",
        value: function isIE11() {
          return -1 !== navigator.userAgent.indexOf("MSIE") || navigator.appVersion.indexOf("Trident/") > -1;
        }
      }, {
        key: "getKeyCodeOnKeyDownEvent",
        value: function getKeyCodeOnKeyDownEvent(t) {
          var e = void 0;if (null != t) if (void 0 === t.code) switch (t.keyCode) {case 13:
              e = "Enter";break;case 38:
              e = "ArrowUp";break;case 40:
              e = "ArrowDown";break;case 9:
              e = "Tab";break;case 27:
              e = "Escape";break;case 33:
              e = "PageUp";break;case 34:
              e = "PageDown";break;default:
              e = void 0;} else e = t.code;return e;
        }
      }]);

      return _class;
    }();
  }, 237: function _(t, e, n) {
    t.exports = n(238);
  }, 238: function _(t, e, n) {
    "use strict";
    n.r(e);var i = n(75);var r = document.querySelectorAll(i.Collapsible.getSelector());for (var _t = 0; _t < r.length; _t++) {
      new i.Collapsible(r[_t]);
    }
  }, 75: function _(t, e, n) {
    "use strict";
    n.r(e), n.d(e, "Collapsible", function () {
      return a;
    });var i = n(0),
        r = n.n(i);var s = "data-rs-collapsible";
    var a = function () {
      function a(t) {
        _classCallCheck(this, a);

        this.element = t, this.content = this.element.parentElement.querySelector("[" + this.attributes.content + "]"), this.triggerElement = this.element, this.initialExpanded = this.content.getAttribute(this.attributes.content) === this.states.expanded, this.addEventHandlers(), this.initialExpanded && this.handleExpandedElement();
      }

      _createClass(a, [{
        key: "addEventHandlers",
        value: function addEventHandlers() {
          var _this = this;

          this.element.getAttribute(s) === this.attributes.navigation && (this.triggerElement = this.element.querySelector("[" + this.attributes.button + "]")), null != this.triggerElement && this.triggerElement.addEventListener("click", function (t) {
            t.preventDefault(), _this.toggle();
          }), this.initialExpanded && window.addEventListener("load", function () {
            _this.handleExpandedElement();
          }), window.addEventListener("orientationchange", function () {
            _this.recalculateContentHeight();
          }), window.addEventListener("resize", function () {
            _this.recalculateContentHeight();
          });
        }
      }, {
        key: "toggle",
        value: function toggle() {
          if (this.isTriggerExpanded) this.collapseTrigger(), this.collapseContent();else {
            var _t2 = this.content.scrollHeight;this.expandTrigger(), this.expandContent(_t2);
          }
        }
      }, {
        key: "handleExpandedElement",
        value: function handleExpandedElement() {
          if (r.a.isDesktop()) {
            var _t3 = this.content.scrollHeight;this.content.style.maxHeight !== _t3 + "px" && (this.expandContent(_t3), this.expandTrigger()), !1 === this.initialExpanded && (this.content.setAttribute(this.attributes.content, this.attributes.filter), this.element.setAttribute(this.attributes.expanded, this.states.expanded));
          } else this.collapseTrigger(), this.collapseContent();
        }
      }, {
        key: "collapseTrigger",
        value: function collapseTrigger() {
          this.element.classList.remove(this.classes.collapsibleTriggerExpanded), this.element.setAttribute(this.attributes.ariaExpanded, "false"), this.element.removeAttribute(this.attributes.expanded);
        }
      }, {
        key: "collapseContent",
        value: function collapseContent() {
          this.content.style.maxHeight = "0px", this.content.setAttribute(this.attributes.content, ""), this.content.setAttribute(this.attributes.ariaHidden, "true");
        }
      }, {
        key: "expandTrigger",
        value: function expandTrigger() {
          this.element.classList.add(this.classes.collapsibleTriggerExpanded), this.element.setAttribute(this.attributes.ariaExpanded, "true");
        }
      }, {
        key: "expandContent",
        value: function expandContent(t) {
          this.content.style.maxHeight = t + 30 + "px", this.content.setAttribute(this.attributes.content, this.states.expanded), this.content.setAttribute(this.attributes.ariaHidden, "false");
        }
      }, {
        key: "recalculateContentHeight",
        value: function recalculateContentHeight() {
          this.content = this.element.parentElement.querySelector("[" + this.attributes.content + "]"), this.contentHeight = this.content.scrollHeight, this.content.getAttribute(this.attributes.content) === this.states.expanded && (this.content.style.maxHeight = this.contentHeight + "px");
        }
      }, {
        key: "attributes",
        get: function get() {
          return { content: s + "-content", button: s + "-button", expanded: s + "-expanded", navigation: "navigation", ariaExpanded: "aria-expanded", ariaHidden: "aria-hidden", filter: "filter" };
        }
      }, {
        key: "classes",
        get: function get() {
          return { collapsibleTriggerExpanded: "collapsible__trigger--expanded" };
        }
      }, {
        key: "states",
        get: function get() {
          return { expanded: "expanded" };
        }
      }, {
        key: "isTriggerExpanded",
        get: function get() {
          return this.element.classList.contains(this.classes.collapsibleTriggerExpanded);
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[" + s + "]";
        }
      }]);

      return a;
    }();
  } }));