"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (e, t) {
  for (var s in t) {
    e[s] = t[s];
  }
}(exports, function (e) {
  var t = {};function s(i) {
    if (t[i]) return t[i].exports;var n = t[i] = { i: i, l: !1, exports: {} };return e[i].call(n.exports, n, n.exports, s), n.l = !0, n.exports;
  }return s.m = e, s.c = t, s.d = function (e, t, i) {
    s.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: i });
  }, s.r = function (e) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(e, "__esModule", { value: !0 });
  }, s.t = function (e, t) {
    if (1 & t && (e = s(e)), 8 & t) return e;if (4 & t && "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e && e.__esModule) return e;var i = Object.create(null);if (s.r(i), Object.defineProperty(i, "default", { enumerable: !0, value: e }), 2 & t && "string" != typeof e) for (var n in e) {
      s.d(i, n, function (t) {
        return e[t];
      }.bind(null, n));
    }return i;
  }, s.n = function (e) {
    var t = e && e.__esModule ? function () {
      return e.default;
    } : function () {
      return e;
    };return s.d(t, "a", t), t;
  }, s.o = function (e, t) {
    return Object.prototype.hasOwnProperty.call(e, t);
  }, s.p = "", s(s.s = 215);
}({ 0: function _(e, t) {
    e.exports = function () {
      function _class() {
        _classCallCheck(this, _class);
      }

      _createClass(_class, null, [{
        key: "triggerEvent",
        value: function triggerEvent(e, t) {
          var s = void 0;"function" == typeof Event ? s = new Event(t) : (s = document.createEvent("Event"), s.initEvent(t, !0, !0)), e.dispatchEvent(s);
        }
      }, {
        key: "getElementByAttribute",
        value: function getElementByAttribute(e, t) {
          return t ? document.querySelector("[" + e + "=\"" + t + "\"]") : document.querySelector("[" + e + "]");
        }
      }, {
        key: "getElementsByAttribute",
        value: function getElementsByAttribute(e, t) {
          return t ? document.querySelectorAll("[" + e + "=\"" + t + "\"]") : document.querySelectorAll("[" + e + "]");
        }
      }, {
        key: "getElementByAttributeWithinElement",
        value: function getElementByAttributeWithinElement(e, t, s) {
          return s ? e.querySelector("[" + t + "=\"" + s + "\"]") : e.querySelector("[" + t + "]");
        }
      }, {
        key: "isMouseOver",
        value: function isMouseOver(e) {
          return Array.prototype.slice.call(e.parentElement.querySelectorAll(":hover")).filter(function () {
            return e[0] == this;
          }).length > 0;
        }
      }, {
        key: "getElementsByAttributeWithinElement",
        value: function getElementsByAttributeWithinElement(e, t, s) {
          return s ? e.querySelectorAll("[" + t + "=\"" + s + "\"]") : e.querySelectorAll("[" + t + "]");
        }
      }, {
        key: "getElementByClassWithinElement",
        value: function getElementByClassWithinElement(e, t) {
          return e.querySelector("." + t);
        }
      }, {
        key: "maxWidthMobileViewports",
        value: function maxWidthMobileViewports() {
          return 940;
        }
      }, {
        key: "createEvent",
        value: function createEvent(e) {
          var t = void 0;return "function" == typeof Event ? t = new Event(e) : (t = document.createEvent("Event"), t.initEvent(e, !0, !0)), t;
        }
      }, {
        key: "isDesktop",
        value: function isDesktop() {
          return Math.max(document.documentElement.clientWidth, window.innerWidth || 0) > this.maxWidthMobileViewports();
        }
      }, {
        key: "isIE11",
        value: function isIE11() {
          return -1 !== navigator.userAgent.indexOf("MSIE") || navigator.appVersion.indexOf("Trident/") > -1;
        }
      }, {
        key: "getKeyCodeOnKeyDownEvent",
        value: function getKeyCodeOnKeyDownEvent(e) {
          var t = void 0;if (null != e) if (void 0 === e.code) switch (e.keyCode) {case 13:
              t = "Enter";break;case 38:
              t = "ArrowUp";break;case 40:
              t = "ArrowDown";break;case 9:
              t = "Tab";break;case 27:
              t = "Escape";break;case 33:
              t = "PageUp";break;case 34:
              t = "PageDown";break;default:
              t = void 0;} else t = e.code;return t;
        }
      }]);

      return _class;
    }();
  }, 215: function _(e, t, s) {
    e.exports = s(67);
  }, 67: function _(e, t, s) {
    "use strict";
    s.r(t), s.d(t, "AutoSuggest", function () {
      return l;
    });var i = s(0),
        n = s.n(i);var r = "data-rs-auto-suggest";
    var l = function () {
      function l(e) {
        _classCallCheck(this, l);

        this.element = e, this.minimumLength = 1, this.container = this.element, this.input = n.a.getElementByAttributeWithinElement(this.element, this.attributes.input), this.noResultMessage = this.input.getAttribute(this.attributes.noResultMessage), this.list = n.a.getElementByAttributeWithinElement(this.element, this.attributes.list), this.separator = this.list.getAttribute(this.attributes.separator), this.dataSource = this.getDataSource(), this.threshold = Number(this.input.getAttribute(this.attributes.threshold)) || 300, this.disableNativeAutoComplete(), this.addEventHandlers(), this.timeout, this.numberOfListItems = 0, this.selectedIndex, this.selectedValue, this.previousInput = "", this.resultList;
      }

      _createClass(l, [{
        key: "disableNativeAutoComplete",
        value: function disableNativeAutoComplete() {
          this.input.setAttribute(this.attributes.autocomplete, "off");
        }
      }, {
        key: "addEventHandlers",
        value: function addEventHandlers() {
          var _this = this;

          ["input", "focus", "keydown"].forEach(function (e) {
            _this.element.addEventListener(e, _this.onInput.bind(_this), { capture: !0 });
          }), document.addEventListener("click", this.clickOutside.bind(this));
        }
      }, {
        key: "onInput",
        value: function onInput(e) {
          e.target.hasAttribute(this.attributes.input) && ("input" === e.type || "focus" === e.type ? (this.numberOfListItems = 0, this.input.value.length >= this.minimumLength ? ((this.previousInput.length < this.minimumLength || -1 === this.input.value.toLowerCase().indexOf(this.previousInput.toLowerCase())) && (this.dataSource = this.getDataSource()), this.createList()) : this.closeList(), this.previousInput = this.input.value) : "keydown" === e.type && (n.a.getKeyCodeOnKeyDownEvent(e) == this.keyCodes.Tab && (this.selectedIndex > -1 && this.setSelectedValue(this.selectedIndex), this.closeList()), n.a.getKeyCodeOnKeyDownEvent(e) == this.keyCodes.Escape && this.closeList(), this.handleButtonKeys(e)));
        }
      }, {
        key: "clickOutside",
        value: function clickOutside(e) {
          this.list.contains(e.target) || this.input.contains(e.target) || this.closeList();
        }
      }, {
        key: "createList",
        value: function createList() {
          var _this2 = this;

          var e = "",
              t = 0;if (window.clearTimeout(this.timeout), this.resultList = JSON.parse(JSON.stringify(this.dataSource)), this.dataSource.forEach(function (s) {
            if (s.toLowerCase().indexOf(_this2.input.value.toLowerCase()) > -1) {
              var _i = new RegExp(_this2.input.value, "gi"),
                  _n = s.replace(_i, function (e) {
                return "<mark>" + e + "</mark>";
              }),
                  _r = "<li class=\"" + _this2.classes.autosuggestItem + "\" " + _this2.attributes.listValue + " " + _this2.attributes.index + "=\"" + t + "\" >" + _n + "</li>";e += _r, _this2.numberOfListItems++, t++;
            } else {
              var _e = _this2.resultList.indexOf(s);_e > -1 && _this2.resultList.splice(_e, 1);
            }
          }), this.dataSource = JSON.parse(JSON.stringify(this.resultList)), this.numberOfListItems > 0) this.timeout = setTimeout(function () {
            return _this2.bindResultList(e);
          }, this.threshold);else {
            var _e2 = "<li class=\"" + this.classes.autosuggestNoResult + "\">" + (this.noResultMessage || "no results") + " </li>";this.bindResultList(_e2);
          }
        }
      }, {
        key: "bindResultList",
        value: function bindResultList(e) {
          var _this3 = this;

          this.list.innerHTML = e, this.container.classList.add(this.classes.autosuggestOpen), this.list.querySelectorAll("[" + this.attributes.listValue + "]").forEach(function (e, t) {
            0 === t && (e.classList.add(_this3.classes.autosuggestPreselect), e.setAttribute(_this3.attributes.preselect, ""), _this3.selectedIndex = 0, _this3.updatePreselectedItem(_this3.selectedIndex, null, !0)), e.addEventListener("click", function () {
              _this3.setSelectedValue(_this3.selectedIndex);
            }), e.addEventListener("mouseenter", function (t) {
              _this3.updatePreselectedItem(e.getAttribute(_this3.attributes.index), t, !1);
            });
          });
        }
      }, {
        key: "updatePreselectedItem",
        value: function updatePreselectedItem(e, t, s) {
          var i = n.a.getElementByAttribute(this.attributes.preselect);i && (i.classList.remove(this.classes.autosuggestPreselect), i.removeAttribute(this.attributes.preselect));var r = n.a.getElementByAttribute(this.attributes.index, e);r && (r.classList.add(this.classes.autosuggestPreselect), r.setAttribute(this.attributes.preselect, "")), this.selectedIndex = e, this.selectedValue = r.innerText, (null != t && "keydown" === t.type || !0 === s) && this.setScrollbar(r, t);
        }
      }, {
        key: "setSelectedValue",
        value: function setSelectedValue(e) {
          var t = n.a.getElementByAttribute(this.attributes.index, e);null != t && (this.input.value = t.innerText, this.closeList());
        }
      }, {
        key: "handleButtonKeys",
        value: function handleButtonKeys(e) {
          if (null != e && this.selectedIndex > -1) switch (n.a.getKeyCodeOnKeyDownEvent(e)) {case this.keyCodes.Down:
              e.preventDefault(), this.selectedIndex < this.numberOfListItems - 1 && (this.selectedIndex++, this.updatePreselectedItem(this.selectedIndex, e, !1));break;case this.keyCodes.Up:
              e.preventDefault(), this.selectedIndex > 0 && (this.selectedIndex--, this.updatePreselectedItem(this.selectedIndex, e, !1));break;case this.keyCodes.PageDown:
              e.preventDefault(), this.selectedIndex < this.numberOfListItems - 1 && this.updatePreselectedItem(Number(this.numberOfListItems) - 1, e, !1);break;case this.keyCodes.PageUp:
              e.preventDefault(), this.selectedIndex > 0 && this.updatePreselectedItem(0, e, !1);break;case this.keyCodes.Enter:
              e.preventDefault(), this.setSelectedValue(this.selectedIndex);}
        }
      }, {
        key: "setScrollbar",
        value: function setScrollbar(e, t) {
          var s = e.offsetHeight,
              i = e.offsetTop,
              r = this.list.children[0].offsetTop;null != t && n.a.getKeyCodeOnKeyDownEvent(t) == this.keyCodes.Down ? s + i > this.list.offsetHeight + this.list.scrollTop && (this.list.scrollTop = s + i - this.list.offsetHeight) : (i - r < this.list.scrollTop || i + s > this.list.scrollTop + this.list.offsetHeight) && (this.list.scrollTop = i - r);
        }
      }, {
        key: "closeList",
        value: function closeList() {
          this.container.classList.remove(this.classes.autosuggestOpen), this.list.innerHTML = "", this.selectedValue = "", this.selectedIndex = -1;
        }
      }, {
        key: "getDataSource",
        value: function getDataSource() {
          var e = this.list.getAttribute(this.attributes.values);return null != e ? e.split(this.separator) : [];
        }
      }, {
        key: "attributes",
        get: function get() {
          return { autocomplete: "autocomplete", noResultMessage: r + "-no-result-message", container: r + "-container", index: r + "-index", input: r + "-input", list: r + "-list", listValue: r + "-list-value", preselect: r + "-preselect", selected: r + "-selected", separator: r + "-separator", threshold: r + "-threshold", values: r + "-values" };
        }
      }, {
        key: "classes",
        get: function get() {
          return { autosuggestOpen: "select-menu--open", autosuggestItem: "select-menu__item", autosuggestNoResult: "select-menu__item select-menu__item--no-result", autosuggestPreselect: "select-menu__item--preselect" };
        }
      }, {
        key: "keyCodes",
        get: function get() {
          return { Enter: "Enter", Up: "ArrowUp", Down: "ArrowDown", Tab: "Tab", Escape: "Escape", PageUp: "PageUp", PageDown: "PageDown" };
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[" + r + "]";
        }
      }]);

      return l;
    }();
  } }));