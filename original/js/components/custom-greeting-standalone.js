"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (e, t) {
  for (var n in t) {
    e[n] = t[n];
  }
}(exports, function (e) {
  var t = {};function n(r) {
    if (t[r]) return t[r].exports;var o = t[r] = { i: r, l: !1, exports: {} };return e[r].call(o.exports, o, o.exports, n), o.l = !0, o.exports;
  }return n.m = e, n.c = t, n.d = function (e, t, r) {
    n.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: r });
  }, n.r = function (e) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(e, "__esModule", { value: !0 });
  }, n.t = function (e, t) {
    if (1 & t && (e = n(e)), 8 & t) return e;if (4 & t && "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e && e.__esModule) return e;var r = Object.create(null);if (n.r(r), Object.defineProperty(r, "default", { enumerable: !0, value: e }), 2 & t && "string" != typeof e) for (var o in e) {
      n.d(r, o, function (t) {
        return e[t];
      }.bind(null, o));
    }return r;
  }, n.n = function (e) {
    var t = e && e.__esModule ? function () {
      return e.default;
    } : function () {
      return e;
    };return n.d(t, "a", t), t;
  }, n.o = function (e, t) {
    return Object.prototype.hasOwnProperty.call(e, t);
  }, n.p = "", n(n.s = 243);
}({ 243: function _(e, t, n) {
    e.exports = n(244);
  }, 244: function _(e, t, n) {
    "use strict";
    n.r(t);var r = n(77);var o = document.querySelectorAll(r.CustomGreeting.getSelector());for (var _e = 0; _e < o.length; _e++) {
      new r.CustomGreeting(o[_e]);
    }
  }, 77: function _(e, t, n) {
    "use strict";
    n.r(t), n.d(t, "CustomGreeting", function () {
      return r;
    });
    var r = function () {
      function r(e) {
        _classCallCheck(this, r);

        this.element = e, this.element.innerText = this.getCustomGreeting();
      }

      _createClass(r, [{
        key: "getCustomGreeting",
        value: function getCustomGreeting() {
          var e = new Date().getHours();return e < 6 ? "good night" : e >= 6 && e < 12 ? "good morning" : e >= 12 && e < 18 ? "good afternoon" : e >= 18 ? "good evening" : void 0;
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[data-rs-custom-greeting]";
        }
      }]);

      return r;
    }();
  } }));