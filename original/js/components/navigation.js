"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (e, t) {
  for (var i in t) {
    e[i] = t[i];
  }
}(exports, function (e) {
  var t = {};function i(a) {
    if (t[a]) return t[a].exports;var r = t[a] = { i: a, l: !1, exports: {} };return e[a].call(r.exports, r, r.exports, i), r.l = !0, r.exports;
  }return i.m = e, i.c = t, i.d = function (e, t, a) {
    i.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: a });
  }, i.r = function (e) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(e, "__esModule", { value: !0 });
  }, i.t = function (e, t) {
    if (1 & t && (e = i(e)), 8 & t) return e;if (4 & t && "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e && e.__esModule) return e;var a = Object.create(null);if (i.r(a), Object.defineProperty(a, "default", { enumerable: !0, value: e }), 2 & t && "string" != typeof e) for (var r in e) {
      i.d(a, r, function (t) {
        return e[t];
      }.bind(null, r));
    }return a;
  }, i.n = function (e) {
    var t = e && e.__esModule ? function () {
      return e.default;
    } : function () {
      return e;
    };return i.d(t, "a", t), t;
  }, i.o = function (e, t) {
    return Object.prototype.hasOwnProperty.call(e, t);
  }, i.p = "", i(i.s = 262);
}({ 0: function _(e, t) {
    e.exports = function () {
      function _class() {
        _classCallCheck(this, _class);
      }

      _createClass(_class, null, [{
        key: "triggerEvent",
        value: function triggerEvent(e, t) {
          var i = void 0;"function" == typeof Event ? i = new Event(t) : (i = document.createEvent("Event"), i.initEvent(t, !0, !0)), e.dispatchEvent(i);
        }
      }, {
        key: "getElementByAttribute",
        value: function getElementByAttribute(e, t) {
          return t ? document.querySelector("[" + e + "=\"" + t + "\"]") : document.querySelector("[" + e + "]");
        }
      }, {
        key: "getElementsByAttribute",
        value: function getElementsByAttribute(e, t) {
          return t ? document.querySelectorAll("[" + e + "=\"" + t + "\"]") : document.querySelectorAll("[" + e + "]");
        }
      }, {
        key: "getElementByAttributeWithinElement",
        value: function getElementByAttributeWithinElement(e, t, i) {
          return i ? e.querySelector("[" + t + "=\"" + i + "\"]") : e.querySelector("[" + t + "]");
        }
      }, {
        key: "isMouseOver",
        value: function isMouseOver(e) {
          return Array.prototype.slice.call(e.parentElement.querySelectorAll(":hover")).filter(function () {
            return e[0] == this;
          }).length > 0;
        }
      }, {
        key: "getElementsByAttributeWithinElement",
        value: function getElementsByAttributeWithinElement(e, t, i) {
          return i ? e.querySelectorAll("[" + t + "=\"" + i + "\"]") : e.querySelectorAll("[" + t + "]");
        }
      }, {
        key: "getElementByClassWithinElement",
        value: function getElementByClassWithinElement(e, t) {
          return e.querySelector("." + t);
        }
      }, {
        key: "maxWidthMobileViewports",
        value: function maxWidthMobileViewports() {
          return 940;
        }
      }, {
        key: "createEvent",
        value: function createEvent(e) {
          var t = void 0;return "function" == typeof Event ? t = new Event(e) : (t = document.createEvent("Event"), t.initEvent(e, !0, !0)), t;
        }
      }, {
        key: "isDesktop",
        value: function isDesktop() {
          return Math.max(document.documentElement.clientWidth, window.innerWidth || 0) > this.maxWidthMobileViewports();
        }
      }, {
        key: "isIE11",
        value: function isIE11() {
          return -1 !== navigator.userAgent.indexOf("MSIE") || navigator.appVersion.indexOf("Trident/") > -1;
        }
      }, {
        key: "getKeyCodeOnKeyDownEvent",
        value: function getKeyCodeOnKeyDownEvent(e) {
          var t = void 0;if (null != e) if (void 0 === e.code) switch (e.keyCode) {case 13:
              t = "Enter";break;case 38:
              t = "ArrowUp";break;case 40:
              t = "ArrowDown";break;case 9:
              t = "Tab";break;case 27:
              t = "Escape";break;case 33:
              t = "PageUp";break;case 34:
              t = "PageDown";break;default:
              t = void 0;} else t = e.code;return t;
        }
      }]);

      return _class;
    }();
  }, 262: function _(e, t, i) {
    e.exports = i(81);
  }, 81: function _(e, t, i) {
    "use strict";
    i.r(t), i.d(t, "Navigation", function () {
      return s;
    });var a = i(0),
        r = i.n(a);
    var s = function () {
      function s(e) {
        _classCallCheck(this, s);

        this.element = e, this.menuIcon = r.a.getElementByAttribute(this.attributes.menuIcon), this.modalMain = r.a.getElementByAttributeWithinElement(this.element, this.attributes.modalMain), this.collapsibles = r.a.getElementsByAttributeWithinElement(this.element, this.attributes.collapsible), this.modalHeader = r.a.getElementByAttributeWithinElement(this.element, this.attributes.modalHeader), this.scrollPos = 0, this.addEventHandlers();
      }

      _createClass(s, [{
        key: "addEventHandlers",
        value: function addEventHandlers() {
          var _this = this;

          this.menuIcon.addEventListener("click", function () {
            document.querySelector("html").classList.contains(_this.classes.modalOpen) && setTimeout(function () {
              return _this.closeCollapsible();
            }, 500), document.querySelector("html").classList.toggle("" + _this.classes.modalOpen), document.querySelector("html").classList.toggle("" + _this.classes.modalNavOpen), document.querySelectorAll(_this.classes.noticeTextOnly).forEach(function (e) {
              e.classList.toggle("" + _this.classes.hidden);
            }), _this.element.classList.toggle("" + _this.classes.modalActive);var e = r.a.getElementByAttributeWithinElement(_this.element, _this.attributes.headerDivider);null !== e && (e.classList.remove("" + _this.classes.modalHeaderDividerIn), e.classList.remove("" + _this.classes.modalHeaderDividerOut));
          }), this.modalMain.addEventListener("scroll", function () {
            _this.checkPosition();
          }, { passive: !0 });
        }
      }, {
        key: "closeCollapsible",
        value: function closeCollapsible() {
          var _this2 = this;

          this.collapsibles.forEach(function (e) {
            _this2.content = e.parentElement.querySelector("[" + _this2.attributes.collapsibleContent + "]"), e.classList.remove(_this2.classes.collapsibleTriggerExpanded), e.setAttribute(_this2.attributes.ariaExpanded, "false"), e.removeAttribute(_this2.attributes.expanded), _this2.content.style.maxHeight = "0px", _this2.content.setAttribute(_this2.attributes.content, ""), _this2.content.setAttribute(_this2.attributes.ariaHidden, "true");
          });
        }
      }, {
        key: "checkPosition",
        value: function checkPosition() {
          var e = this.modalMain.scrollTop;e > this.scrollPos && (this.modalHeader.classList.add(this.classes.modalHeaderDivider), this.modalHeader.setAttribute(this.attributes.headerDivider, ""), this.modalHeader.classList.add(this.classes.modalHeaderDividerIn), this.modalHeader.classList.remove(this.classes.modalHeaderDividerOut)), 0 === e && (this.modalHeader.classList.add(this.classes.modalHeaderDividerOut), this.modalHeader.classList.remove(this.classes.modalHeaderDivider), this.modalHeader.removeAttribute(this.attributes.headerDivider), this.modalHeader.classList.remove(this.classes.modalHeaderDividerIn));
        }
      }, {
        key: "classes",
        get: function get() {
          return { hidden: "hidden", noticeTextOnly: ".notice-text-only", modalOpen: "modal-open", modalNavOpen: "modal-nav-open", modalActive: "modal--active", modalHeaderDivider: "modal__header--divider", modalHeaderDividerIn: "modal__header--divider-in", modalHeaderDividerOut: "modal__header--divider-out", collapsibleTriggerExpanded: "collapsible__trigger--expanded" };
        }
      }, {
        key: "attributes",
        get: function get() {
          return { ariaExpanded: "aria-expanded", ariaHidden: "aria-hidden", collapsibleContent: "data-rs-collapsible-content", collapsible: "data-rs-collapsible", content: "data-rs-collapsible-content", headerDivider: "data-rs-navigation-header-divider", menuIcon: "data-rs-navigation-menu-icon", modalMain: "data-rs-navigation-modal-main", modalHeader: "data-rs-navigation-modal-header" };
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[data-rs-navigation]";
        }
      }]);

      return s;
    }();
  } }));