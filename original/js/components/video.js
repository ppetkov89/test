"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (t, e) {
  for (var r in e) {
    t[r] = e[r];
  }
}(exports, function (t) {
  var e = {};function r(i) {
    if (e[i]) return e[i].exports;var n = e[i] = { i: i, l: !1, exports: {} };return t[i].call(n.exports, n, n.exports, r), n.l = !0, n.exports;
  }return r.m = t, r.c = e, r.d = function (t, e, i) {
    r.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: i });
  }, r.r = function (t) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 });
  }, r.t = function (t, e) {
    if (1 & e && (t = r(t)), 8 & e) return t;if (4 & e && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t && t.__esModule) return t;var i = Object.create(null);if (r.r(i), Object.defineProperty(i, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t) for (var n in t) {
      r.d(i, n, function (e) {
        return t[e];
      }.bind(null, n));
    }return i;
  }, r.n = function (t) {
    var e = t && t.__esModule ? function () {
      return t.default;
    } : function () {
      return t;
    };return r.d(e, "a", e), e;
  }, r.o = function (t, e) {
    return Object.prototype.hasOwnProperty.call(t, e);
  }, r.p = "", r(r.s = 312);
}({ 0: function _(t, e) {
    t.exports = function () {
      function _class() {
        _classCallCheck(this, _class);
      }

      _createClass(_class, null, [{
        key: "triggerEvent",
        value: function triggerEvent(t, e) {
          var r = void 0;"function" == typeof Event ? r = new Event(e) : (r = document.createEvent("Event"), r.initEvent(e, !0, !0)), t.dispatchEvent(r);
        }
      }, {
        key: "getElementByAttribute",
        value: function getElementByAttribute(t, e) {
          return e ? document.querySelector("[" + t + "=\"" + e + "\"]") : document.querySelector("[" + t + "]");
        }
      }, {
        key: "getElementsByAttribute",
        value: function getElementsByAttribute(t, e) {
          return e ? document.querySelectorAll("[" + t + "=\"" + e + "\"]") : document.querySelectorAll("[" + t + "]");
        }
      }, {
        key: "getElementByAttributeWithinElement",
        value: function getElementByAttributeWithinElement(t, e, r) {
          return r ? t.querySelector("[" + e + "=\"" + r + "\"]") : t.querySelector("[" + e + "]");
        }
      }, {
        key: "isMouseOver",
        value: function isMouseOver(t) {
          return Array.prototype.slice.call(t.parentElement.querySelectorAll(":hover")).filter(function () {
            return t[0] == this;
          }).length > 0;
        }
      }, {
        key: "getElementsByAttributeWithinElement",
        value: function getElementsByAttributeWithinElement(t, e, r) {
          return r ? t.querySelectorAll("[" + e + "=\"" + r + "\"]") : t.querySelectorAll("[" + e + "]");
        }
      }, {
        key: "getElementByClassWithinElement",
        value: function getElementByClassWithinElement(t, e) {
          return t.querySelector("." + e);
        }
      }, {
        key: "maxWidthMobileViewports",
        value: function maxWidthMobileViewports() {
          return 940;
        }
      }, {
        key: "createEvent",
        value: function createEvent(t) {
          var e = void 0;return "function" == typeof Event ? e = new Event(t) : (e = document.createEvent("Event"), e.initEvent(t, !0, !0)), e;
        }
      }, {
        key: "isDesktop",
        value: function isDesktop() {
          return Math.max(document.documentElement.clientWidth, window.innerWidth || 0) > this.maxWidthMobileViewports();
        }
      }, {
        key: "isIE11",
        value: function isIE11() {
          return -1 !== navigator.userAgent.indexOf("MSIE") || navigator.appVersion.indexOf("Trident/") > -1;
        }
      }, {
        key: "getKeyCodeOnKeyDownEvent",
        value: function getKeyCodeOnKeyDownEvent(t) {
          var e = void 0;if (null != t) if (void 0 === t.code) switch (t.keyCode) {case 13:
              e = "Enter";break;case 38:
              e = "ArrowUp";break;case 40:
              e = "ArrowDown";break;case 9:
              e = "Tab";break;case 27:
              e = "Escape";break;case 33:
              e = "PageUp";break;case 34:
              e = "PageDown";break;default:
              e = void 0;} else e = t.code;return e;
        }
      }]);

      return _class;
    }();
  }, 312: function _(t, e, r) {
    t.exports = r(99);
  }, 99: function _(t, e, r) {
    "use strict";
    r.r(e), r.d(e, "Video", function () {
      return o;
    });var i = r(0),
        n = r.n(i);
    var o = function () {
      function o(t) {
        _classCallCheck(this, o);

        this.element = t, this.modal = n.a.getElementByAttribute(this.attributes.modal, this.element.getAttribute("data-rs-video")), this.iframe = n.a.getElementByAttribute(this.attributes.iframe, this.element.getAttribute("data-rs-video")), this.scrollbarWidth = window.innerWidth - document.documentElement.clientWidth, this.scrollPosition = 0, this.bodyMarginTop = this.initializeBodyMarginTop(), this.addEventHandlers();
      }

      _createClass(o, [{
        key: "initializeBodyMarginTop",
        value: function initializeBodyMarginTop() {
          return "" === document.body.style.marginTop || null === document.body.style.marginTop ? 0 : document.body.style.marginTop.slice(0, document.body.style.marginTop.length - 2);
        }
      }, {
        key: "addEventHandlers",
        value: function addEventHandlers() {
          var _this = this;

          this.element.addEventListener("click", function (t) {
            t.preventDefault(), _this.scrollPosition = window.pageYOffset, _this.toggleModal();
          }), this.modal.addEventListener("click", function (t) {
            t.preventDefault(), _this.toggleModal(), document.body.style.marginTop = _this.bodyMarginTop + "px", window.scrollTo(0, _this.scrollPosition);
          });
        }
      }, {
        key: "toggleModal",
        value: function toggleModal() {
          this.modal.classList.contains(this.classes.modalActive) ? (this.iframe.setAttribute("src", this.iframe.getAttribute("src")), document.body.style.paddingRight = "0px", this.modal.style.paddingRight = "") : (document.body.style.marginTop = this.bodyMarginTop - this.scrollPosition + "px", document.body.style.paddingRight = this.scrollbarWidth + "px", this.modal.style.paddingRight = this.scrollbarWidth + "px"), document.querySelector("html").classList.toggle(this.classes.modalOpen), this.modal.classList.toggle(this.classes.modalActive);
        }
      }, {
        key: "classes",
        get: function get() {
          return { modal: "modal", modalActive: "modal--active", modalOpen: "modal-open", modalOverlayActive: "modal__overlay--active" };
        }
      }, {
        key: "attributes",
        get: function get() {
          return { overlay: "data-rs-video-overlay", modal: "data-rs-video-modal", iframe: "data-rs-video-iframe" };
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[data-rs-video]";
        }
      }]);

      return o;
    }();
  } }));