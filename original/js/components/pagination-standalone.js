"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (t, e) {
  for (var n in e) {
    t[n] = e[n];
  }
}(exports, function (t) {
  var e = {};function n(i) {
    if (e[i]) return e[i].exports;var s = e[i] = { i: i, l: !1, exports: {} };return t[i].call(s.exports, s, s.exports, n), s.l = !0, s.exports;
  }return n.m = t, n.c = e, n.d = function (t, e, i) {
    n.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: i });
  }, n.r = function (t) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 });
  }, n.t = function (t, e) {
    if (1 & e && (t = n(t)), 8 & e) return t;if (4 & e && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t && t.__esModule) return t;var i = Object.create(null);if (n.r(i), Object.defineProperty(i, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t) for (var s in t) {
      n.d(i, s, function (e) {
        return t[e];
      }.bind(null, s));
    }return i;
  }, n.n = function (t) {
    var e = t && t.__esModule ? function () {
      return t.default;
    } : function () {
      return t;
    };return n.d(e, "a", e), e;
  }, n.o = function (t, e) {
    return Object.prototype.hasOwnProperty.call(t, e);
  }, n.p = "", n(n.s = 263);
}({ 263: function _(t, e, n) {
    t.exports = n(264);
  }, 264: function _(t, e, n) {
    "use strict";
    n.r(e);var i = n(82);var s = document.querySelectorAll(i.Pagination.getSelector());for (var _t = 0; _t < s.length; _t++) {
      new i.Pagination(s[_t]);
    }
  }, 82: function _(t, e, n) {
    "use strict";
    n.r(e), n.d(e, "Pagination", function () {
      return i;
    });
    var i = function () {
      function i(t) {
        _classCallCheck(this, i);

        this.element = t, this.paginationList = this.element.querySelector("[" + this.attributes.paginationList + "]"), this.listItems = [].concat(_toConsumableArray(this.paginationList.getElementsByTagName("li"))), this.handleNextPreviousElements(), this.handleListItems();
      }

      _createClass(i, [{
        key: "handleListItems",
        value: function handleListItems() {
          var t = !1;this.listItems.length > 0 && (this.listItems[0].innerText.trim().length > 2 || this.listItems[this.listItems.length - 1].innerText.trim().length > 2) && (t = !0), t && this.paginationList.classList.add(this.classes.paginationListSmall);
        }
      }, {
        key: "handleNextPreviousElements",
        value: function handleNextPreviousElements() {
          var t = this.listItems[0];"undefined" !== t.getAttribute(this.attributes.paginationControl) && null !== t.getAttribute(this.attributes.paginationControl) && this.listItems.shift();var e = this.listItems[this.listItems.length - 1];"undefined" !== e.getAttribute(this.attributes.paginationControl) && null !== e.getAttribute(this.attributes.paginationControl) && this.listItems.pop();
        }
      }, {
        key: "attributes",
        get: function get() {
          return { paginationControl: "data-rs-pagination-control", paginationList: "data-rs-pagination-list" };
        }
      }, {
        key: "classes",
        get: function get() {
          return { paginationListSmall: "pagination__list--s" };
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[data-rs-pagination]";
        }
      }]);

      return i;
    }();
  } }));