"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (e, t) {
  for (var i in t) {
    e[i] = t[i];
  }
}(exports, function (e) {
  var t = {};function i(s) {
    if (t[s]) return t[s].exports;var r = t[s] = { i: s, l: !1, exports: {} };return e[s].call(r.exports, r, r.exports, i), r.l = !0, r.exports;
  }return i.m = e, i.c = t, i.d = function (e, t, s) {
    i.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: s });
  }, i.r = function (e) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(e, "__esModule", { value: !0 });
  }, i.t = function (e, t) {
    if (1 & t && (e = i(e)), 8 & t) return e;if (4 & t && "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e && e.__esModule) return e;var s = Object.create(null);if (i.r(s), Object.defineProperty(s, "default", { enumerable: !0, value: e }), 2 & t && "string" != typeof e) for (var r in e) {
      i.d(s, r, function (t) {
        return e[t];
      }.bind(null, r));
    }return s;
  }, i.n = function (e) {
    var t = e && e.__esModule ? function () {
      return e.default;
    } : function () {
      return e;
    };return i.d(t, "a", t), t;
  }, i.o = function (e, t) {
    return Object.prototype.hasOwnProperty.call(e, t);
  }, i.p = "", i(i.s = 218);
}({ 0: function _(e, t) {
    e.exports = function () {
      function _class() {
        _classCallCheck(this, _class);
      }

      _createClass(_class, null, [{
        key: "triggerEvent",
        value: function triggerEvent(e, t) {
          var i = void 0;"function" == typeof Event ? i = new Event(t) : (i = document.createEvent("Event"), i.initEvent(t, !0, !0)), e.dispatchEvent(i);
        }
      }, {
        key: "getElementByAttribute",
        value: function getElementByAttribute(e, t) {
          return t ? document.querySelector("[" + e + "=\"" + t + "\"]") : document.querySelector("[" + e + "]");
        }
      }, {
        key: "getElementsByAttribute",
        value: function getElementsByAttribute(e, t) {
          return t ? document.querySelectorAll("[" + e + "=\"" + t + "\"]") : document.querySelectorAll("[" + e + "]");
        }
      }, {
        key: "getElementByAttributeWithinElement",
        value: function getElementByAttributeWithinElement(e, t, i) {
          return i ? e.querySelector("[" + t + "=\"" + i + "\"]") : e.querySelector("[" + t + "]");
        }
      }, {
        key: "isMouseOver",
        value: function isMouseOver(e) {
          return Array.prototype.slice.call(e.parentElement.querySelectorAll(":hover")).filter(function () {
            return e[0] == this;
          }).length > 0;
        }
      }, {
        key: "getElementsByAttributeWithinElement",
        value: function getElementsByAttributeWithinElement(e, t, i) {
          return i ? e.querySelectorAll("[" + t + "=\"" + i + "\"]") : e.querySelectorAll("[" + t + "]");
        }
      }, {
        key: "getElementByClassWithinElement",
        value: function getElementByClassWithinElement(e, t) {
          return e.querySelector("." + t);
        }
      }, {
        key: "maxWidthMobileViewports",
        value: function maxWidthMobileViewports() {
          return 940;
        }
      }, {
        key: "createEvent",
        value: function createEvent(e) {
          var t = void 0;return "function" == typeof Event ? t = new Event(e) : (t = document.createEvent("Event"), t.initEvent(e, !0, !0)), t;
        }
      }, {
        key: "isDesktop",
        value: function isDesktop() {
          return Math.max(document.documentElement.clientWidth, window.innerWidth || 0) > this.maxWidthMobileViewports();
        }
      }, {
        key: "isIE11",
        value: function isIE11() {
          return -1 !== navigator.userAgent.indexOf("MSIE") || navigator.appVersion.indexOf("Trident/") > -1;
        }
      }, {
        key: "getKeyCodeOnKeyDownEvent",
        value: function getKeyCodeOnKeyDownEvent(e) {
          var t = void 0;if (null != e) if (void 0 === e.code) switch (e.keyCode) {case 13:
              t = "Enter";break;case 38:
              t = "ArrowUp";break;case 40:
              t = "ArrowDown";break;case 9:
              t = "Tab";break;case 27:
              t = "Escape";break;case 33:
              t = "PageUp";break;case 34:
              t = "PageDown";break;default:
              t = void 0;} else t = e.code;return t;
        }
      }]);

      return _class;
    }();
  }, 218: function _(e, t, i) {
    e.exports = i(68);
  }, 68: function _(e, t, i) {
    "use strict";
    i.r(t), i.d(t, "Card", function () {
      return n;
    });var s = i(0),
        r = i.n(s);
    var n = function () {
      function n(e) {
        _classCallCheck(this, n);

        this.element = e, this.initVariables(), this.addEventHandlers();
      }

      _createClass(n, [{
        key: "initVariables",
        value: function initVariables() {
          this.showBackSideCard = r.a.getElementByAttributeWithinElement(this.element, this.attributes.showBackSideCard), this.hideBackSideCard = r.a.getElementByAttributeWithinElement(this.element, this.attributes.hideBackSideCard), this.listParent = this.element.closest("ul"), this.selectAllCards = this.listParent.querySelectorAll(this.attributes.selectAllCards);
        }
      }, {
        key: "addEventHandlers",
        value: function addEventHandlers() {
          var _this = this;

          this.showBackSideCard && this.showBackSideCard.addEventListener("click", function () {
            _this.setCardHeights(!0);
          }), this.hideBackSideCard && this.hideBackSideCard.addEventListener("click", function () {
            _this.setCardHeights();
          }), window.addEventListener("resize", function () {
            _this.resizeCards();
          });
        }
      }, {
        key: "slickSliderActive",
        value: function slickSliderActive(e) {
          return e.hasAttribute("data-rs-carousel-card") && "DIV" === e.parentElement.tagName;
        }
      }, {
        key: "removeAnimationAndHeight",
        value: function removeAnimationAndHeight() {
          this.element.classList.remove(this.classes.animate), this.element.style.removeProperty("height");
        }
      }, {
        key: "setCardHeights",
        value: function setCardHeights(e) {
          var _this2 = this;

          if (e) {
            var _e = this.element.getBoundingClientRect();this.selectAllCards.forEach(function (t) {
              _this2.slickSliderActive(t) && (t.parentElement.style.height = _e.height + "px"), t !== _this2.element && t.classList.contains(_this2.classes.backsideActive) && (t.classList.remove(_this2.classes.backsideActive), t.classList.add(_this2.classes.animate), setTimeout(function () {
                return t.classList.remove(_this2.classes.animate);
              }, 100));
            }), this.element.style.height = _e.height + "px", this.element.classList.add(this.classes.backsideActive);
          } else this.selectAllCards.forEach(function (e) {
            _this2.slickSliderActive(e) && e.parentElement.style.removeProperty("height");
          }), this.element.classList.remove(this.classes.backsideActive), this.element.classList.add(this.classes.animate), setTimeout(this.removeAnimationAndHeight.bind(this), 100);
        }
      }, {
        key: "resizeCards",
        value: function resizeCards() {
          var _this3 = this;

          this.selectAllCards.forEach(function (e) {
            _this3.slickSliderActive(e) && e.parentElement.style.removeProperty("height");
          }), this.element.classList.remove(this.classes.backsideActive);
        }
      }, {
        key: "attributes",
        get: function get() {
          return { showBackSideCard: "data-rs-card-show-backside", hideBackSideCard: "data-rs-card-hide-backside", selectAllCards: "li[data-rs-card]" };
        }
      }, {
        key: "classes",
        get: function get() {
          return { backsideActive: "cards__item--backside-active", animate: "cards__item--animate" };
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[data-rs-card]";
        }
      }]);

      return n;
    }();
  } }));