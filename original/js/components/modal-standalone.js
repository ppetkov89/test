"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (e, t) {
  for (var i in t) {
    e[i] = t[i];
  }
}(exports, function (e) {
  var t = {};function i(s) {
    if (t[s]) return t[s].exports;var r = t[s] = { i: s, l: !1, exports: {} };return e[s].call(r.exports, r, r.exports, i), r.l = !0, r.exports;
  }return i.m = e, i.c = t, i.d = function (e, t, s) {
    i.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: s });
  }, i.r = function (e) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(e, "__esModule", { value: !0 });
  }, i.t = function (e, t) {
    if (1 & t && (e = i(e)), 8 & t) return e;if (4 & t && "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e && e.__esModule) return e;var s = Object.create(null);if (i.r(s), Object.defineProperty(s, "default", { enumerable: !0, value: e }), 2 & t && "string" != typeof e) for (var r in e) {
      i.d(s, r, function (t) {
        return e[t];
      }.bind(null, r));
    }return s;
  }, i.n = function (e) {
    var t = e && e.__esModule ? function () {
      return e.default;
    } : function () {
      return e;
    };return i.d(t, "a", t), t;
  }, i.o = function (e, t) {
    return Object.prototype.hasOwnProperty.call(e, t);
  }, i.p = "", i(i.s = 257);
}({ 0: function _(e, t) {
    e.exports = function () {
      function _class() {
        _classCallCheck(this, _class);
      }

      _createClass(_class, null, [{
        key: "triggerEvent",
        value: function triggerEvent(e, t) {
          var i = void 0;"function" == typeof Event ? i = new Event(t) : (i = document.createEvent("Event"), i.initEvent(t, !0, !0)), e.dispatchEvent(i);
        }
      }, {
        key: "getElementByAttribute",
        value: function getElementByAttribute(e, t) {
          return t ? document.querySelector("[" + e + "=\"" + t + "\"]") : document.querySelector("[" + e + "]");
        }
      }, {
        key: "getElementsByAttribute",
        value: function getElementsByAttribute(e, t) {
          return t ? document.querySelectorAll("[" + e + "=\"" + t + "\"]") : document.querySelectorAll("[" + e + "]");
        }
      }, {
        key: "getElementByAttributeWithinElement",
        value: function getElementByAttributeWithinElement(e, t, i) {
          return i ? e.querySelector("[" + t + "=\"" + i + "\"]") : e.querySelector("[" + t + "]");
        }
      }, {
        key: "isMouseOver",
        value: function isMouseOver(e) {
          return Array.prototype.slice.call(e.parentElement.querySelectorAll(":hover")).filter(function () {
            return e[0] == this;
          }).length > 0;
        }
      }, {
        key: "getElementsByAttributeWithinElement",
        value: function getElementsByAttributeWithinElement(e, t, i) {
          return i ? e.querySelectorAll("[" + t + "=\"" + i + "\"]") : e.querySelectorAll("[" + t + "]");
        }
      }, {
        key: "getElementByClassWithinElement",
        value: function getElementByClassWithinElement(e, t) {
          return e.querySelector("." + t);
        }
      }, {
        key: "maxWidthMobileViewports",
        value: function maxWidthMobileViewports() {
          return 940;
        }
      }, {
        key: "createEvent",
        value: function createEvent(e) {
          var t = void 0;return "function" == typeof Event ? t = new Event(e) : (t = document.createEvent("Event"), t.initEvent(e, !0, !0)), t;
        }
      }, {
        key: "isDesktop",
        value: function isDesktop() {
          return Math.max(document.documentElement.clientWidth, window.innerWidth || 0) > this.maxWidthMobileViewports();
        }
      }, {
        key: "isIE11",
        value: function isIE11() {
          return -1 !== navigator.userAgent.indexOf("MSIE") || navigator.appVersion.indexOf("Trident/") > -1;
        }
      }, {
        key: "getKeyCodeOnKeyDownEvent",
        value: function getKeyCodeOnKeyDownEvent(e) {
          var t = void 0;if (null != e) if (void 0 === e.code) switch (e.keyCode) {case 13:
              t = "Enter";break;case 38:
              t = "ArrowUp";break;case 40:
              t = "ArrowDown";break;case 9:
              t = "Tab";break;case 27:
              t = "Escape";break;case 33:
              t = "PageUp";break;case 34:
              t = "PageDown";break;default:
              t = void 0;} else t = e.code;return t;
        }
      }]);

      return _class;
    }();
  }, 257: function _(e, t, i) {
    e.exports = i(258);
  }, 258: function _(e, t, i) {
    "use strict";
    i.r(t);var s = i(80);var r = document.querySelectorAll(s.Modal.getSelector());for (var _e = 0; _e < r.length; _e++) {
      new s.Modal(r[_e]);
    }
  }, 80: function _(e, t, i) {
    "use strict";
    i.r(t), i.d(t, "Modal", function () {
      return o;
    });var s = i(0),
        r = i.n(s);
    var o = function () {
      function o(e) {
        _classCallCheck(this, o);

        this.element = e, this.openTrigger = r.a.getElementByAttribute(this.attributes.trigger, this.element.getAttribute("data-rs-modal")), this.closeTrigger = r.a.getElementByAttributeWithinElement(this.element, this.attributes.closeTrigger), this.doNotCloseOnClickOverlay = r.a.getElementByAttributeWithinElement(this.element, this.attributes.doNotCloseOnClickOverlay), this.dialog = r.a.getElementByAttributeWithinElement(this.element, this.attributes.dialog), this.main = r.a.getElementByAttributeWithinElement(this.element, this.attributes.main), this.header = r.a.getElementByAttributeWithinElement(this.element, this.attributes.header), this.footer = r.a.getElementByAttributeWithinElement(this.element, this.attributes.footer), this.disableHistory = null !== this.element.getAttribute(this.attributes.disableHistory), this.popupDelay = this.element.getAttribute(this.attributes.popupDelay), this.defaultPopupDelay = 5, this.scrollbarWidth = window.innerWidth - document.documentElement.clientWidth, this.scrollPosition = 3, this.bodyMarginTop = o.initializeBodyMarginTop(), this.addEventHandlers(), this.handleDividers(), this.handleQueryStringParameters(), this.initializePopup();
      }

      _createClass(o, [{
        key: "addEventHandlers",
        value: function addEventHandlers() {
          var _this = this;

          this.openTrigger.addEventListener("click", function (e) {
            e.preventDefault(), _this.scrollPosition = window.pageYOffset, _this.toggleModal(!_this.disableHistory);
          }), this.closeTrigger.addEventListener("click", function (e) {
            e.preventDefault(), _this.toggleModal(!_this.disableHistory), document.body.style.marginTop = _this.bodyMarginTop + "px", window.scrollTo(0, _this.scrollPosition);
          }), this.element.addEventListener("mousedown", function (e) {
            _this.doNotCloseOnClickOverlay || e.target === _this.dialog || _this.dialog.contains(e.target) || (_this.toggleModal(!_this.disableHistory), document.body.style.marginTop = _this.bodyMarginTop + "px", window.scrollTo(0, _this.scrollPosition));
          }), window.addEventListener("resize", function () {
            _this.handleDividers();
          }), this.main.addEventListener("scroll", function () {
            _this.checkPosition();
          }, { passive: !0 }), window.addEventListener("popstate", function () {
            _this.handleQueryStringParameters(!1);
          }), document.addEventListener("keydown", function (e) {
            _this.handleButtonKeys(e);
          });
        }
      }, {
        key: "toggleModal",
        value: function toggleModal() {
          var e = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : !0;
          this.element.classList.contains(this.classes.modalActive) ? this.closeModal(e) : this.openModal(e);
        }
      }, {
        key: "closeModal",
        value: function closeModal(e) {
          document.body.style.paddingRight = "0px", this.element.style.paddingRight = "", e && window.history.pushState({}, "", window.location.pathname), document.querySelector("html").classList.remove(this.classes.modalOpen), this.element.setAttribute(this.attributes.hidden, ""), this.element.classList.remove(this.classes.modalActive);
        }
      }, {
        key: "openModal",
        value: function openModal(e) {
          document.body.style.marginTop = this.bodyMarginTop - this.scrollPosition + "px", document.body.style.paddingRight = this.scrollbarWidth + "px", this.element.style.paddingRight = this.scrollbarWidth + "px", e && window.history.pushState({}, "", window.location.pathname + "?" + this.attributes.modalOpen + "=" + this.element.getAttribute("data-rs-modal")), document.querySelector("html").classList.add(this.classes.modalOpen), this.element.removeAttribute(this.attributes.hidden), this.element.classList.add(this.classes.modalActive);
        }
      }, {
        key: "handleDividers",
        value: function handleDividers() {
          this.main.scrollHeight > this.main.offsetHeight ? this.addDivider() : this.header && this.header.classList.contains(this.classes.divider) && this.removeDivider();
        }
      }, {
        key: "addDivider",
        value: function addDivider() {
          this.main.classList.add(this.classes.modalMainOverflow), null !== this.footer && this.footer.classList.add(this.classes.dividerTop);
        }
      }, {
        key: "removeDivider",
        value: function removeDivider() {
          this.header.classList.remove(this.classes.divider), this.main.classList.remove(this.classes.modalMainOverflow), null !== this.footer && this.footer.classList.remove(this.classes.dividerTop);
        }
      }, {
        key: "checkPosition",
        value: function checkPosition() {
          var e = this.main.scrollTop;this.header && e > this.scrollPosition && (this.header.classList.add(this.classes.divider), this.header.classList.add(this.classes.modalHeaderDividerIn), this.header.classList.remove(this.classes.modalHeaderDividerOut)), (this.header && e < this.scrollPosition || this.header && 0 === e) && (this.header.classList.add(this.classes.modalHeaderDividerOut), this.header.classList.remove(this.classes.divider), this.header.classList.remove(this.classes.modalHeaderDividerIn));
        }
      }, {
        key: "handleQueryStringParameters",
        value: function handleQueryStringParameters() {
          if (window.location.href.includes("?" + this.attributes.modalOpen)) {
            var _e2 = new URLSearchParams(window.location.search);this.openModalElement = _e2.get(this.attributes.modalOpen), this.openModalElement === this.element.getAttribute("data-rs-modal") && this.openModal(!1);
          } else this.closeModal(!1);
        }
      }, {
        key: "initializePopup",
        value: function initializePopup() {
          var _this2 = this;

          if (null !== this.popupDelay) {
            var _e3 = this.element.getAttribute(this.attributes.popupDelay);setTimeout(function () {
              _this2.openModal(!1);
            }, 1e3 * ("" === _e3 ? this.defaultPopupDelay : _e3));
          }
        }
      }, {
        key: "handleButtonKeys",
        value: function handleButtonKeys(e) {
          if (null != e) switch (r.a.getKeyCodeOnKeyDownEvent(e)) {case this.keyCodes.Escape:
              e.preventDefault(), this.closeModal(!0), document.body.style.marginTop = this.bodyMarginTop + "px", window.scrollTo(0, this.scrollPosition);}
        }
      }, {
        key: "classes",
        get: function get() {
          return { modalActive: "modal--active", modalOpen: "modal-open", modalHeaderDivider: "divider", modalHeaderDividerIn: "modal__header--divider-in", modalHeaderDividerOut: "modal__header--divider-out", modalMainOverflow: "modal__main--overflow", divider: "divider", dividerTop: "divider--top" };
        }
      }, {
        key: "attributes",
        get: function get() {
          return { trigger: "data-rs-modal-trigger", popupDelay: "data-rs-modal-popup-delay", closeTrigger: "data-rs-modal-close-trigger", dialog: "data-rs-modal-dialog", main: "data-rs-modal-main", doNotCloseOnClickOverlay: "data-rs-modal-do-not-close-on-click-overlay", header: "data-rs-modal-header", footer: "data-rs-modal-footer", disableHistory: "data-rs-modal-disable-history", modalOpen: "modal_open", hidden: "hidden" };
        }
      }, {
        key: "keyCodes",
        get: function get() {
          return { Escape: "Escape" };
        }
      }], [{
        key: "initializeBodyMarginTop",
        value: function initializeBodyMarginTop() {
          var e = document.body.style.marginTop;return e ? e.slice(0, e.length - 2) : 0;
        }
      }, {
        key: "getSelector",
        value: function getSelector() {
          return "[data-rs-modal]";
        }
      }]);

      return o;
    }();
  } }));