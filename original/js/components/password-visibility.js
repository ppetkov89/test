"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (t, e) {
  for (var r in e) {
    t[r] = e[r];
  }
}(exports, function (t) {
  var e = {};function r(i) {
    if (e[i]) return e[i].exports;var s = e[i] = { i: i, l: !1, exports: {} };return t[i].call(s.exports, s, s.exports, r), s.l = !0, s.exports;
  }return r.m = t, r.c = e, r.d = function (t, e, i) {
    r.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: i });
  }, r.r = function (t) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 });
  }, r.t = function (t, e) {
    if (1 & e && (t = r(t)), 8 & e) return t;if (4 & e && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t && t.__esModule) return t;var i = Object.create(null);if (r.r(i), Object.defineProperty(i, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t) for (var s in t) {
      r.d(i, s, function (e) {
        return t[e];
      }.bind(null, s));
    }return i;
  }, r.n = function (t) {
    var e = t && t.__esModule ? function () {
      return t.default;
    } : function () {
      return t;
    };return r.d(e, "a", e), e;
  }, r.o = function (t, e) {
    return Object.prototype.hasOwnProperty.call(t, e);
  }, r.p = "", r(r.s = 271);
}({ 271: function _(t, e, r) {
    t.exports = r(84);
  }, 84: function _(t, e, r) {
    "use strict";
    r.r(e), r.d(e, "PasswordVisibility", function () {
      return i;
    });
    var i = function () {
      function i(t) {
        _classCallCheck(this, i);

        this.element = t, this.triggerElement = this.element.querySelector("[" + this.attributes.trigger + "]"), this.input = this.element.querySelector("input[type=password]"), this.addEventHandlers();
      }

      _createClass(i, [{
        key: "addEventHandlers",
        value: function addEventHandlers() {
          var _this = this;

          this.triggerElement.addEventListener("keyup", function () {
            _this.toggleVisibility();
          }), this.triggerElement.addEventListener("click", function () {
            _this.toggleVisibility();
          });
        }
      }, {
        key: "toggleVisibility",
        value: function toggleVisibility() {
          var t = this.input.getAttribute("type");if ([this.types.password, this.types.text].indexOf(t.toLowerCase()) > -1) {
            var _e = t.toLowerCase() === this.types.password ? this.types.text : this.types.password;this.input.setAttribute("type", _e), this.triggerElement.classList.toggle(this.classes.isPasswordVisible);
          }
        }
      }, {
        key: "attributes",
        get: function get() {
          return { trigger: "data-rs-password-visibility-trigger" };
        }
      }, {
        key: "classes",
        get: function get() {
          return { isPasswordVisible: "is-password-visible" };
        }
      }, {
        key: "types",
        get: function get() {
          return { text: "text", password: "password" };
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[data-rs-password-visibility]";
        }
      }]);

      return i;
    }();
  } }));