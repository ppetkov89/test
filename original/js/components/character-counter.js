"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (t, e) {
  for (var r in e) {
    t[r] = e[r];
  }
}(exports, function (t) {
  var e = {};function r(n) {
    if (e[n]) return e[n].exports;var i = e[n] = { i: n, l: !1, exports: {} };return t[n].call(i.exports, i, i.exports, r), i.l = !0, i.exports;
  }return r.m = t, r.c = e, r.d = function (t, e, n) {
    r.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: n });
  }, r.r = function (t) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 });
  }, r.t = function (t, e) {
    if (1 & e && (t = r(t)), 8 & e) return t;if (4 & e && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t && t.__esModule) return t;var n = Object.create(null);if (r.r(n), Object.defineProperty(n, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t) for (var i in t) {
      r.d(n, i, function (e) {
        return t[e];
      }.bind(null, i));
    }return n;
  }, r.n = function (t) {
    var e = t && t.__esModule ? function () {
      return t.default;
    } : function () {
      return t;
    };return r.d(e, "a", e), e;
  }, r.o = function (t, e) {
    return Object.prototype.hasOwnProperty.call(t, e);
  }, r.p = "", r(r.s = 224);
}({ 0: function _(t, e) {
    t.exports = function () {
      function _class() {
        _classCallCheck(this, _class);
      }

      _createClass(_class, null, [{
        key: "triggerEvent",
        value: function triggerEvent(t, e) {
          var r = void 0;"function" == typeof Event ? r = new Event(e) : (r = document.createEvent("Event"), r.initEvent(e, !0, !0)), t.dispatchEvent(r);
        }
      }, {
        key: "getElementByAttribute",
        value: function getElementByAttribute(t, e) {
          return e ? document.querySelector("[" + t + "=\"" + e + "\"]") : document.querySelector("[" + t + "]");
        }
      }, {
        key: "getElementsByAttribute",
        value: function getElementsByAttribute(t, e) {
          return e ? document.querySelectorAll("[" + t + "=\"" + e + "\"]") : document.querySelectorAll("[" + t + "]");
        }
      }, {
        key: "getElementByAttributeWithinElement",
        value: function getElementByAttributeWithinElement(t, e, r) {
          return r ? t.querySelector("[" + e + "=\"" + r + "\"]") : t.querySelector("[" + e + "]");
        }
      }, {
        key: "isMouseOver",
        value: function isMouseOver(t) {
          return Array.prototype.slice.call(t.parentElement.querySelectorAll(":hover")).filter(function () {
            return t[0] == this;
          }).length > 0;
        }
      }, {
        key: "getElementsByAttributeWithinElement",
        value: function getElementsByAttributeWithinElement(t, e, r) {
          return r ? t.querySelectorAll("[" + e + "=\"" + r + "\"]") : t.querySelectorAll("[" + e + "]");
        }
      }, {
        key: "getElementByClassWithinElement",
        value: function getElementByClassWithinElement(t, e) {
          return t.querySelector("." + e);
        }
      }, {
        key: "maxWidthMobileViewports",
        value: function maxWidthMobileViewports() {
          return 940;
        }
      }, {
        key: "createEvent",
        value: function createEvent(t) {
          var e = void 0;return "function" == typeof Event ? e = new Event(t) : (e = document.createEvent("Event"), e.initEvent(t, !0, !0)), e;
        }
      }, {
        key: "isDesktop",
        value: function isDesktop() {
          return Math.max(document.documentElement.clientWidth, window.innerWidth || 0) > this.maxWidthMobileViewports();
        }
      }, {
        key: "isIE11",
        value: function isIE11() {
          return -1 !== navigator.userAgent.indexOf("MSIE") || navigator.appVersion.indexOf("Trident/") > -1;
        }
      }, {
        key: "getKeyCodeOnKeyDownEvent",
        value: function getKeyCodeOnKeyDownEvent(t) {
          var e = void 0;if (null != t) if (void 0 === t.code) switch (t.keyCode) {case 13:
              e = "Enter";break;case 38:
              e = "ArrowUp";break;case 40:
              e = "ArrowDown";break;case 9:
              e = "Tab";break;case 27:
              e = "Escape";break;case 33:
              e = "PageUp";break;case 34:
              e = "PageDown";break;default:
              e = void 0;} else e = t.code;return e;
        }
      }]);

      return _class;
    }();
  }, 224: function _(t, e, r) {
    t.exports = r(70);
  }, 70: function _(t, e, r) {
    "use strict";
    r.r(e), r.d(e, "CharacterCounter", function () {
      return o;
    });var n = r(0),
        i = r.n(n);var u = "data-rs-character-counter";
    var o = function () {
      function o(t) {
        _classCallCheck(this, o);

        this.element = t, this.counterOutput = i.a.getElementByAttribute(this.attributes.output, this.element.getAttribute(u)), this.maxLength = this.element.getAttribute(this.attributes.maxLength), this.setCounterOutput(), this.addEventHandlers();
      }

      _createClass(o, [{
        key: "addEventHandlers",
        value: function addEventHandlers() {
          var _this = this;

          this.element.addEventListener("input", function () {
            _this.setCounterOutput();
          });
        }
      }, {
        key: "setCounterOutput",
        value: function setCounterOutput() {
          if (this.counterOutput) {
            var _t = this.maxLength - this.element.value.length;_t >= 0 ? (this.counterOutput.classList.remove(this.classes.textNegative), this.counterOutput.innerText = _t + " characters left") : (this.counterOutput.classList.add(this.classes.textNegative), this.counterOutput.innerText = _t + " characters");
          }
        }
      }, {
        key: "attributes",
        get: function get() {
          return { output: u + "-output", maxLength: u + "-maxlength" };
        }
      }, {
        key: "classes",
        get: function get() {
          return { textNegative: "text--negative" };
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[" + u + "]";
        }
      }]);

      return o;
    }();
  } }));