"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (t, e) {
  for (var r in e) {
    t[r] = e[r];
  }
}(exports, function (t) {
  var e = {};function r(i) {
    if (e[i]) return e[i].exports;var n = e[i] = { i: i, l: !1, exports: {} };return t[i].call(n.exports, n, n.exports, r), n.l = !0, n.exports;
  }return r.m = t, r.c = e, r.d = function (t, e, i) {
    r.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: i });
  }, r.r = function (t) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 });
  }, r.t = function (t, e) {
    if (1 & e && (t = r(t)), 8 & e) return t;if (4 & e && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t && t.__esModule) return t;var i = Object.create(null);if (r.r(i), Object.defineProperty(i, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t) for (var n in t) {
      r.d(i, n, function (e) {
        return t[e];
      }.bind(null, n));
    }return i;
  }, r.n = function (t) {
    var e = t && t.__esModule ? function () {
      return t.default;
    } : function () {
      return t;
    };return r.d(e, "a", e), e;
  }, r.o = function (t, e) {
    return Object.prototype.hasOwnProperty.call(t, e);
  }, r.p = "", r(r.s = 283);
}({ 0: function _(t, e) {
    t.exports = function () {
      function _class() {
        _classCallCheck(this, _class);
      }

      _createClass(_class, null, [{
        key: "triggerEvent",
        value: function triggerEvent(t, e) {
          var r = void 0;"function" == typeof Event ? r = new Event(e) : (r = document.createEvent("Event"), r.initEvent(e, !0, !0)), t.dispatchEvent(r);
        }
      }, {
        key: "getElementByAttribute",
        value: function getElementByAttribute(t, e) {
          return e ? document.querySelector("[" + t + "=\"" + e + "\"]") : document.querySelector("[" + t + "]");
        }
      }, {
        key: "getElementsByAttribute",
        value: function getElementsByAttribute(t, e) {
          return e ? document.querySelectorAll("[" + t + "=\"" + e + "\"]") : document.querySelectorAll("[" + t + "]");
        }
      }, {
        key: "getElementByAttributeWithinElement",
        value: function getElementByAttributeWithinElement(t, e, r) {
          return r ? t.querySelector("[" + e + "=\"" + r + "\"]") : t.querySelector("[" + e + "]");
        }
      }, {
        key: "isMouseOver",
        value: function isMouseOver(t) {
          return Array.prototype.slice.call(t.parentElement.querySelectorAll(":hover")).filter(function () {
            return t[0] == this;
          }).length > 0;
        }
      }, {
        key: "getElementsByAttributeWithinElement",
        value: function getElementsByAttributeWithinElement(t, e, r) {
          return r ? t.querySelectorAll("[" + e + "=\"" + r + "\"]") : t.querySelectorAll("[" + e + "]");
        }
      }, {
        key: "getElementByClassWithinElement",
        value: function getElementByClassWithinElement(t, e) {
          return t.querySelector("." + e);
        }
      }, {
        key: "maxWidthMobileViewports",
        value: function maxWidthMobileViewports() {
          return 940;
        }
      }, {
        key: "createEvent",
        value: function createEvent(t) {
          var e = void 0;return "function" == typeof Event ? e = new Event(t) : (e = document.createEvent("Event"), e.initEvent(t, !0, !0)), e;
        }
      }, {
        key: "isDesktop",
        value: function isDesktop() {
          return Math.max(document.documentElement.clientWidth, window.innerWidth || 0) > this.maxWidthMobileViewports();
        }
      }, {
        key: "isIE11",
        value: function isIE11() {
          return -1 !== navigator.userAgent.indexOf("MSIE") || navigator.appVersion.indexOf("Trident/") > -1;
        }
      }, {
        key: "getKeyCodeOnKeyDownEvent",
        value: function getKeyCodeOnKeyDownEvent(t) {
          var e = void 0;if (null != t) if (void 0 === t.code) switch (t.keyCode) {case 13:
              e = "Enter";break;case 38:
              e = "ArrowUp";break;case 40:
              e = "ArrowDown";break;case 9:
              e = "Tab";break;case 27:
              e = "Escape";break;case 33:
              e = "PageUp";break;case 34:
              e = "PageDown";break;default:
              e = void 0;} else e = t.code;return e;
        }
      }]);

      return _class;
    }();
  }, 283: function _(t, e, r) {
    t.exports = r(89);
  }, 89: function _(t, e, r) {
    "use strict";
    r.r(e), r.d(e, "Slider", function () {
      return l;
    });var i = r(0),
        n = r.n(i);
    var l = function () {
      function l(t) {
        _classCallCheck(this, l);

        this.element = t, this.rangeSlider = null, this.init();
      }

      _createClass(l, [{
        key: "init",
        value: function init() {
          this.rangeSlider = n.a.getElementByAttributeWithinElement(this.element, this.attributes.rangeLine), this.bullet = n.a.getElementByAttributeWithinElement(this.element, this.attributes.bullet), this.label = n.a.getElementByAttributeWithinElement(this.element, this.attributes.output.label), this.tooltip = n.a.getElementByAttributeWithinElement(this.element, this.attributes.tooltip), this.format = this.element.getAttribute(this.attributes.format), this.addEventHandlers(), this.setOutputValues();
        }
      }, {
        key: "addEventHandlers",
        value: function addEventHandlers() {
          var _this = this;

          var t = n.a.isIE11() ? "change" : "input";this.rangeSlider.addEventListener(t, function () {
            _this.setBulletPosition();
          });
        }
      }, {
        key: "setOutputValues",
        value: function setOutputValues() {
          n.a.getElementByAttributeWithinElement(this.element, this.attributes.output.min).innerText = this.formatNumber(this.rangeSlider.min), n.a.getElementByAttributeWithinElement(this.element, this.attributes.output.max).innerText = this.formatNumber(this.rangeSlider.max), this.setBulletPosition();
        }
      }, {
        key: "setBulletPosition",
        value: function setBulletPosition() {
          var t = parseInt(this.rangeSlider.value);if (isNaN(t)) return void this.bullet.setAttribute("hidden", "");this.label.innerText = this.formatNumber(t);var e = this.rangeSlider.offsetWidth - 13,
              r = this.rangeSlider.min,
              i = this.rangeSlider.max;var n = (this.bullet.offsetWidth - 11) / 2,
              l = e * ((this.rangeSlider.value - r) / (i - r)) - n,
              s = e - this.bullet.offsetWidth + 13 + 11;l < -11 ? (this.tooltip.style.marginLeft = l + (this.bullet.offsetWidth + 11) / 2 + "px", l = -11) : l > s ? (this.tooltip.style.marginRight = e - l + 11 - this.bullet.offsetWidth / 2 + 5.5 + "px", l = s) : (this.tooltip.style.marginRight = "auto", this.tooltip.style.marginLeft = "auto"), this.bullet.style.left = l + "px";
        }
      }, {
        key: "formatNumber",
        value: function formatNumber(t) {
          var e = t;return void 0 === this.format || null === this.format || (e = "" === this.format.trim() ? new Intl.NumberFormat().format(t) : new Intl.NumberFormat(this.format).format(t)), e;
        }
      }, {
        key: "attributes",
        get: function get() {
          return { rangeLine: "data-rs-slider-range-line", bullet: "data-rs-slider-label", tooltip: "data-rs-slider-tooltip", format: "data-rs-slider-format", output: { min: "data-rs-slider-min", max: "data-rs-slider-max", label: "data-rs-slider-label-value" } };
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[data-rs-slider]";
        }
      }]);

      return l;
    }();
  } }));