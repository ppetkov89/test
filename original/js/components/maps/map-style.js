"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (e, t) {
  for (var i in t) {
    e[i] = t[i];
  }
}(exports, function (e) {
  var t = {};function i(r) {
    if (t[r]) return t[r].exports;var l = t[r] = { i: r, l: !1, exports: {} };return e[r].call(l.exports, l, l.exports, i), l.l = !0, l.exports;
  }return i.m = e, i.c = t, i.d = function (e, t, r) {
    i.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: r });
  }, i.r = function (e) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(e, "__esModule", { value: !0 });
  }, i.t = function (e, t) {
    if (1 & t && (e = i(e)), 8 & t) return e;if (4 & t && "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e && e.__esModule) return e;var r = Object.create(null);if (i.r(r), Object.defineProperty(r, "default", { enumerable: !0, value: e }), 2 & t && "string" != typeof e) for (var l in e) {
      i.d(r, l, function (t) {
        return e[t];
      }.bind(null, l));
    }return r;
  }, i.n = function (e) {
    var t = e && e.__esModule ? function () {
      return e.default;
    } : function () {
      return e;
    };return i.d(t, "a", t), t;
  }, i.o = function (e, t) {
    return Object.prototype.hasOwnProperty.call(e, t);
  }, i.p = "", i(i.s = 319);
}({ 28: function _(e, t, i) {
    "use strict";
    i.r(t), i.d(t, "MapStyle", function () {
      return r;
    });
    var r = function () {
      function r() {
        _classCallCheck(this, r);
      }

      _createClass(r, null, [{
        key: "custom",
        get: function get() {
          return [{ featureType: "administrative.land_parcel", stylers: [{ visibility: "off" }] }, { featureType: "administrative.locality", elementType: "labels", stylers: [{ visibility: "simplified" }] }, { featureType: "administrative.neighborhood", stylers: [{ visibility: "off" }] }, { featureType: "administrative.province", elementType: "labels", stylers: [{ visibility: "off" }] }, { featureType: "landscape.natural", elementType: "geometry.fill", stylers: [{ color: "#f7f5f0" }] }, { featureType: "poi", elementType: "labels.text", stylers: [{ visibility: "off" }] }, { featureType: "poi.business", stylers: [{ visibility: "off" }] }, { featureType: "road", elementType: "labels", stylers: [{ visibility: "off" }] }, { featureType: "road", elementType: "labels.icon", stylers: [{ visibility: "off" }] }, { featureType: "road.arterial", stylers: [{ visibility: "off" }] }, { featureType: "road.highway", elementType: "labels", stylers: [{ visibility: "off" }] }, { featureType: "road.local", stylers: [{ visibility: "off" }] }, { featureType: "transit", stylers: [{ visibility: "off" }] }, { featureType: "water", elementType: "labels.text", stylers: [{ visibility: "off" }] }];
        }
      }]);

      return r;
    }();
  }, 319: function _(e, t, i) {
    e.exports = i(28);
  } }));