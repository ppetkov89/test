"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (e, o) {
  for (var t in o) {
    e[t] = o[t];
  }
}(exports, function (e) {
  var o = {};function t(n) {
    if (o[n]) return o[n].exports;var r = o[n] = { i: n, l: !1, exports: {} };return e[n].call(r.exports, r, r.exports, t), r.l = !0, r.exports;
  }return t.m = e, t.c = o, t.d = function (e, o, n) {
    t.o(e, o) || Object.defineProperty(e, o, { enumerable: !0, get: n });
  }, t.r = function (e) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(e, "__esModule", { value: !0 });
  }, t.t = function (e, o) {
    if (1 & o && (e = t(e)), 8 & o) return e;if (4 & o && "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e && e.__esModule) return e;var n = Object.create(null);if (t.r(n), Object.defineProperty(n, "default", { enumerable: !0, value: e }), 2 & o && "string" != typeof e) for (var r in e) {
      t.d(n, r, function (o) {
        return e[o];
      }.bind(null, r));
    }return n;
  }, t.n = function (e) {
    var o = e && e.__esModule ? function () {
      return e.default;
    } : function () {
      return e;
    };return t.d(o, "a", o), o;
  }, t.o = function (e, o) {
    return Object.prototype.hasOwnProperty.call(e, o);
  }, t.p = "", t(t.s = 318);
}({ 13: function _(e, o, t) {
    "use strict";
    t.r(o), t.d(o, "GoogleMapsApi", function () {
      return n;
    });
    var n = function () {
      function n(e) {
        _classCallCheck(this, n);

        this.apiKey = e;
      }

      _createClass(n, [{
        key: "load",
        value: function load() {
          var _this = this;

          return new Promise(function (e) {
            if (_this.resolve = e, void 0 === window.humanForward && (window.humanForward = {}), window.humanForward.GoogleMapsApi || (_this.callbackName = "humanForward.GoogleMapsApi.mapReady", window.humanForward.GoogleMapsApi = _this, window.humanForward.GoogleMapsApi.mapReady = _this.mapReady.bind(_this)), void 0 !== window.google && void 0 !== window.google.maps || void 0 === _this.callbackName) _this.resolve();else {
              _this.loaded = !0;var _e = document.createElement("script");_e.src = "//maps.googleapis.com/maps/api/js?key=" + _this.apiKey + "&callback=" + _this.callbackName, _e.async = !0, document.body.appendChild(_e);
            }
          });
        }
      }, {
        key: "mapReady",
        value: function mapReady() {
          this.extendApi(), this.resolve();
        }
      }, {
        key: "extendApi",
        value: function extendApi() {
          google.maps.Map.prototype.panToWithOffset = function (e, o, t) {
            var n = this,
                r = new google.maps.OverlayView();r.onAdd = function () {
              var r = this.getProjection(),
                  i = r.fromLatLngToContainerPixel(e);i.x = i.x + o, i.y = i.y + t, n.panTo(r.fromContainerPixelToLatLng(i));
            }, r.draw = function () {}, r.setMap(this);
          };
        }
      }]);

      return n;
    }();
  }, 318: function _(e, o, t) {
    e.exports = t(13);
  } }));