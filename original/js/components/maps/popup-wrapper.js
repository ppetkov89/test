"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (t, e) {
  for (var n in e) {
    t[n] = e[n];
  }
}(exports, function (t) {
  var e = {};function n(i) {
    if (e[i]) return e[i].exports;var o = e[i] = { i: i, l: !1, exports: {} };return t[i].call(o.exports, o, o.exports, n), o.l = !0, o.exports;
  }return n.m = t, n.c = e, n.d = function (t, e, i) {
    n.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: i });
  }, n.r = function (t) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 });
  }, n.t = function (t, e) {
    if (1 & e && (t = n(t)), 8 & e) return t;if (4 & e && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t && t.__esModule) return t;var i = Object.create(null);if (n.r(i), Object.defineProperty(i, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t) for (var o in t) {
      n.d(i, o, function (e) {
        return t[e];
      }.bind(null, o));
    }return i;
  }, n.n = function (t) {
    var e = t && t.__esModule ? function () {
      return t.default;
    } : function () {
      return t;
    };return n.d(e, "a", e), e;
  }, n.o = function (t, e) {
    return Object.prototype.hasOwnProperty.call(t, e);
  }, n.p = "", n(n.s = 320);
}({ 29: function _(t, e, n) {
    "use strict";
    n.r(e), n.d(e, "PopupWrapper", function () {
      return i;
    });
    var i = function () {
      function i() {
        _classCallCheck(this, i);
      }

      _createClass(i, null, [{
        key: "popupClass",
        get: function get() {
          function t(t, e, n) {
            this.position = t, this.content = e, this.marker = n, this.content.classList.add("popup-bubble"), this.containerDiv = document.createElement("div"), this.containerDiv.classList.add("popup-bubble__container"), this.containerDiv.appendChild(e), google.maps.OverlayView.preventMapHitsAndGesturesFrom(this.containerDiv);
          }return t.prototype = Object.create(google.maps.OverlayView.prototype), t.prototype.onAdd = function () {
            this.getPanes().floatPane.appendChild(this.containerDiv), this.show();
          }, t.prototype.onRemove = function () {
            this.containerDiv.parentElement && this.containerDiv.parentElement.removeChild(this.containerDiv);
          }, t.prototype.draw = function () {
            var t = this.getProjection().fromLatLngToDivPixel(this.position),
                e = Math.abs(t.x) < 4e3 && Math.abs(t.y) < 4e3 ? "block" : "none";"block" === e && (this.containerDiv.style.left = t.x + "px", this.containerDiv.style.top = t.y + "px"), this.containerDiv.style.display !== e && (this.containerDiv.style.display = e);
          }, t.prototype.height = function () {
            return getComputedStyle(this.content).getPropertyValue("height");
          }, t.prototype.hide = function () {
            var _this = this;

            this.content.addEventListener("transitionend", function () {
              getComputedStyle(_this.content).getPropertyValue("opacity") <= .1 && _this.setMap(null);
            }), this.content.classList.remove("active");
          }, t.prototype.show = function () {
            var _this2 = this;

            setTimeout(function () {
              _this2.content.classList.add("active"), setTimeout(function () {
                var t = parseInt(getComputedStyle(_this2.content).getPropertyValue("height"));if (!isNaN(t)) {
                  var _e = -1 * (t / 2 + _this2.marker.icon.size.height);_this2.marker.map.panToWithOffset(_this2.marker.getPosition(), 0, _e);
                }
              });
            }, 50);
          }, t;
        }
      }]);

      return i;
    }();
  }, 320: function _(t, e, n) {
    t.exports = n(29);
  } }));