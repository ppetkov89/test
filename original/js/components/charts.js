"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (t, e) {
  for (var r in e) {
    t[r] = e[r];
  }
}(exports, function (t) {
  var e = {};function r(n) {
    if (e[n]) return e[n].exports;var i = e[n] = { i: n, l: !1, exports: {} };return t[n].call(i.exports, i, i.exports, r), i.l = !0, i.exports;
  }return r.m = t, r.c = e, r.d = function (t, e, n) {
    r.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: n });
  }, r.r = function (t) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 });
  }, r.t = function (t, e) {
    if (1 & e && (t = r(t)), 8 & e) return t;if (4 & e && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t && t.__esModule) return t;var n = Object.create(null);if (r.r(n), Object.defineProperty(n, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t) for (var i in t) {
      r.d(n, i, function (e) {
        return t[e];
      }.bind(null, i));
    }return n;
  }, r.n = function (t) {
    var e = t && t.__esModule ? function () {
      return t.default;
    } : function () {
      return t;
    };return r.d(e, "a", e), e;
  }, r.o = function (t, e) {
    return Object.prototype.hasOwnProperty.call(t, e);
  }, r.p = "", r(r.s = 227);
}({ 0: function _(t, e) {
    t.exports = function () {
      function _class() {
        _classCallCheck(this, _class);
      }

      _createClass(_class, null, [{
        key: "triggerEvent",
        value: function triggerEvent(t, e) {
          var r = void 0;"function" == typeof Event ? r = new Event(e) : (r = document.createEvent("Event"), r.initEvent(e, !0, !0)), t.dispatchEvent(r);
        }
      }, {
        key: "getElementByAttribute",
        value: function getElementByAttribute(t, e) {
          return e ? document.querySelector("[" + t + "=\"" + e + "\"]") : document.querySelector("[" + t + "]");
        }
      }, {
        key: "getElementsByAttribute",
        value: function getElementsByAttribute(t, e) {
          return e ? document.querySelectorAll("[" + t + "=\"" + e + "\"]") : document.querySelectorAll("[" + t + "]");
        }
      }, {
        key: "getElementByAttributeWithinElement",
        value: function getElementByAttributeWithinElement(t, e, r) {
          return r ? t.querySelector("[" + e + "=\"" + r + "\"]") : t.querySelector("[" + e + "]");
        }
      }, {
        key: "isMouseOver",
        value: function isMouseOver(t) {
          return Array.prototype.slice.call(t.parentElement.querySelectorAll(":hover")).filter(function () {
            return t[0] == this;
          }).length > 0;
        }
      }, {
        key: "getElementsByAttributeWithinElement",
        value: function getElementsByAttributeWithinElement(t, e, r) {
          return r ? t.querySelectorAll("[" + e + "=\"" + r + "\"]") : t.querySelectorAll("[" + e + "]");
        }
      }, {
        key: "getElementByClassWithinElement",
        value: function getElementByClassWithinElement(t, e) {
          return t.querySelector("." + e);
        }
      }, {
        key: "maxWidthMobileViewports",
        value: function maxWidthMobileViewports() {
          return 940;
        }
      }, {
        key: "createEvent",
        value: function createEvent(t) {
          var e = void 0;return "function" == typeof Event ? e = new Event(t) : (e = document.createEvent("Event"), e.initEvent(t, !0, !0)), e;
        }
      }, {
        key: "isDesktop",
        value: function isDesktop() {
          return Math.max(document.documentElement.clientWidth, window.innerWidth || 0) > this.maxWidthMobileViewports();
        }
      }, {
        key: "isIE11",
        value: function isIE11() {
          return -1 !== navigator.userAgent.indexOf("MSIE") || navigator.appVersion.indexOf("Trident/") > -1;
        }
      }, {
        key: "getKeyCodeOnKeyDownEvent",
        value: function getKeyCodeOnKeyDownEvent(t) {
          var e = void 0;if (null != t) if (void 0 === t.code) switch (t.keyCode) {case 13:
              e = "Enter";break;case 38:
              e = "ArrowUp";break;case 40:
              e = "ArrowDown";break;case 9:
              e = "Tab";break;case 27:
              e = "Escape";break;case 33:
              e = "PageUp";break;case 34:
              e = "PageDown";break;default:
              e = void 0;} else e = t.code;return e;
        }
      }]);

      return _class;
    }();
  }, 12: function _(t, e, r) {
    "use strict";
    r.r(e), r.d(e, "GoogleChartsApi", function () {
      return i;
    });var n = !1;
    var i = function () {
      function i() {
        _classCallCheck(this, i);

        this.api = "https://www.gstatic.com/charts/loader.js", this.scriptElement = this.getScriptElement();
      }

      _createClass(i, [{
        key: "load",
        value: function load() {
          var _this = this;

          return new Promise(function (t) {
            !1 === n ? (n = !0, _this.scriptElement = document.createElement("script"), document.body.appendChild(_this.scriptElement), _this.scriptElement.addEventListener("load", function () {
              _this.loadPackages().then(function () {
                t();
              });
            }), _this.scriptElement.async = !0, _this.scriptElement.src = _this.api) : _this.scriptElement.addEventListener("load", function () {
              _this.loadPackages().then(function () {
                t();
              });
            });
          });
        }
      }, {
        key: "loadPackages",
        value: function loadPackages() {
          return google.charts.load("current", { packages: ["corechart", "bar"] });
        }
      }, {
        key: "getScriptElement",
        value: function getScriptElement() {
          return document.querySelector("script[src='" + this.api + "']");
        }
      }]);

      return i;
    }();
  }, 227: function _(t, e, r) {
    t.exports = r(71);
  }, 71: function _(t, e, r) {
    "use strict";
    r.r(e), r.d(e, "Charts", function () {
      return o;
    });var n = r(0),
        i = r.n(n),
        s = r(12);
    var o = function () {
      function o(t) {
        _classCallCheck(this, o);

        this.element = t, this.type = this.getType(), this.initializeChart();
      }

      _createClass(o, [{
        key: "getType",
        value: function getType() {
          var t = null;var e = this.element.getAttribute("data-rs-charts");if ("string" == typeof e && void 0 !== this.types[e.toLowerCase()] && (t = this.types[e.toLowerCase()]), null === t) throw Error("Chart type is invalid");return t;
        }
      }, {
        key: "initializeChart",
        value: function initializeChart() {
          var _this2 = this;

          this.chartElement = i.a.getElementByAttributeWithinElement(this.element, this.attributes.placeholder);new s.GoogleChartsApi().load().then(function () {
            _this2.createChart(), _this2.createLegend();
          }).catch(function (t) {
            console.error(t);
          });
        }
      }, {
        key: "createChart",
        value: function createChart() {
          var t = i.a.getElementByAttributeWithinElement(this.element, this.attributes.source);switch (this.type) {case this.types.column:
              this.chart = new google.visualization.ColumnChart(this.chartElement), this.options = this.getColumnOptions();break;case this.types.donut:
              this.chart = new google.visualization.PieChart(this.chartElement), this.options = this.getDonutOptions();break;case this.types.line:
              this.chart = new google.visualization.LineChart(this.chartElement), this.options = this.getLineOptions();}this.data = this.getData(t), this.chart.draw(this.data, this.options);
        }
      }, {
        key: "getData",
        value: function getData(t) {
          return this.sourceData = this.parseData(t), google.visualization.arrayToDataTable(this.sourceData);
        }
      }, {
        key: "parseData",
        value: function parseData(t) {
          var _this3 = this;

          var e = [];var r = [].concat(_toConsumableArray(t.querySelectorAll("thead th"))).map(function (t) {
            return t.innerText;
          });return this.type !== this.types.line && r.push({ type: "string", role: "style" }), e.push(r), [].concat(_toConsumableArray(t.querySelectorAll("tbody tr"))).forEach(function (t, r) {
            var n = [].concat(_toConsumableArray(t.querySelectorAll("td"))).map(function (t, e) {
              return 0 === e ? t.innerText : parseInt(t.innerText);
            });_this3.type !== _this3.types.line && n.push("color: " + _this3.options.colors[r]), e.push(n);
          }), e;
        }
      }, {
        key: "getDefaultOptions",
        value: function getDefaultOptions() {
          return { backgroundColor: "transparent", colors: ["#f0f0f0", "#e8e8e8", "#e0e0e0", "#dcdcdc", "#d8d8d8", "#d3d3d3", "#d0d0d0", "#c8c8c8", "#c0c0c0", "#bebebe", "#b8b8b8", "#b0b0b0", "#a9a9a9", "#a8a8a8", "#a0a0a0"], fontSize: 18, legend: { position: "none" }, tooltip: { textStyle: { color: "#000000", bold: !1 }, isHtml: !1, trigger: "selection" } };
        }
      }, {
        key: "getColumnOptions",
        value: function getColumnOptions() {
          var t = this.getDefaultOptions();var e = this.chartElement.offsetWidth,
              r = this.chartElement.offsetHeight;return t.chartArea = { width: e - 50, left: 50, top: 20, height: r - 100 }, t.hAxis = { textPosition: "none" }, t;
        }
      }, {
        key: "getDonutOptions",
        value: function getDonutOptions() {
          var t = this.getDefaultOptions();var e = this.chartElement.offsetWidth;return t.chartArea = { width: e, left: 0, top: 0, height: e }, t.height = e + 70, t.width = e, t.pieHole = .5, t.pieSliceBorderColor = "transparent", t;
        }
      }, {
        key: "getLineOptions",
        value: function getLineOptions() {
          var t = this.getDefaultOptions();var e = this.chartElement.offsetWidth,
              r = this.chartElement.offsetHeight;return t.chartArea = { width: e - 50, left: 50, top: 20, height: r - 130 }, t.curveType = "function", t;
        }
      }, {
        key: "createLegend",
        value: function createLegend() {
          var t = i.a.getElementByAttributeWithinElement(this.element, this.attributes.legend);switch (this.type) {case this.types.donut:case this.types.column:
              this.createDonutOrColumnLegend(t);break;case this.types.line:
              this.createLineLegend(t);}
        }
      }, {
        key: "createDonutOrColumnLegend",
        value: function createDonutOrColumnLegend(t) {
          var _this4 = this;

          this.sourceData.shift(), this.sourceData.forEach(function (e, r) {
            null !== t && _this4.appendListItemsToList(r, t, e[0]);
          });
        }
      }, {
        key: "createLineLegend",
        value: function createLineLegend(t) {
          var _this5 = this;

          this.sourceData[0].shift(), this.sourceData[0].forEach(function (e, r) {
            null !== t && _this5.appendListItemsToList(r, t, e);
          });
        }
      }, {
        key: "appendListItemsToList",
        value: function appendListItemsToList(t, e, r) {
          var n = document.createElement("li");n.setAttribute(this.attributes.legendItem, this.options.colors[t]);var i = document.createTextNode(r);n.appendChild(i), e.appendChild(n);
        }
      }, {
        key: "types",
        get: function get() {
          return { column: "column", donut: "donut", line: "line" };
        }
      }, {
        key: "attributes",
        get: function get() {
          return { placeholder: "data-rs-charts-placeholder", source: "data-rs-charts-source", legend: "data-rs-charts-legend", legendItem: "data-rs-charts-legend-item" };
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[data-rs-charts]";
        }
      }]);

      return o;
    }();
  } }));