"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (t, e) {
  for (var s in e) {
    t[s] = e[s];
  }
}(exports, function (t) {
  var e = {};function s(i) {
    if (e[i]) return e[i].exports;var r = e[i] = { i: i, l: !1, exports: {} };return t[i].call(r.exports, r, r.exports, s), r.l = !0, r.exports;
  }return s.m = t, s.c = e, s.d = function (t, e, i) {
    s.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: i });
  }, s.r = function (t) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 });
  }, s.t = function (t, e) {
    if (1 & e && (t = s(t)), 8 & e) return t;if (4 & e && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t && t.__esModule) return t;var i = Object.create(null);if (s.r(i), Object.defineProperty(i, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t) for (var r in t) {
      s.d(i, r, function (e) {
        return t[e];
      }.bind(null, r));
    }return i;
  }, s.n = function (t) {
    var e = t && t.__esModule ? function () {
      return t.default;
    } : function () {
      return t;
    };return s.d(e, "a", e), e;
  }, s.o = function (t, e) {
    return Object.prototype.hasOwnProperty.call(t, e);
  }, s.p = "", s(s.s = 240);
}({ 240: function _(t, e, s) {
    t.exports = s(241);
  }, 241: function _(t, e, s) {
    "use strict";
    s.r(e);var i = s(76);var r = document.querySelectorAll(i.Conditional.getSelector());for (var _t = 0; _t < r.length; _t++) {
      new i.Conditional(r[_t]);
    }
  }, 76: function _(t, e, s) {
    "use strict";
    s.r(e), s.d(e, "Conditional", function () {
      return r;
    });var i = "data-rs-conditional";
    var r = function () {
      function r(t) {
        _classCallCheck(this, r);

        this.element = t, this.triggerElement = document.querySelector(this.element.getAttribute(this.attributes.trigger)), this.condition = this.element.getAttribute(this.attributes.condition), this.match = null, this.addEventHandlers(), this.validate();
      }

      _createClass(r, [{
        key: "getClassList",
        value: function getClassList(t) {
          var e = this.element.getAttribute(this.attributes[t].class);return null === e ? [] : e.split(",").map(function (t) {
            return t.trim();
          });
        }
      }, {
        key: "addEventHandlers",
        value: function addEventHandlers() {
          var _this = this;

          this.triggerElement.addEventListener("change", function () {
            _this.validate();
          }), window.addEventListener("load", function () {
            _this.validate();
          });
        }
      }, {
        key: "validate",
        value: function validate() {
          this.match = null !== document.querySelector(this.condition), !0 === this.match ? this.success() : this.failure();
        }
      }, {
        key: "success",
        value: function success() {
          this.addClassList(this.classes.success), this.removeClassList(this.classes.failure);
        }
      }, {
        key: "failure",
        value: function failure() {
          this.addClassList(this.classes.failure), this.removeClassList(this.classes.success);
        }
      }, {
        key: "addClassList",
        value: function addClassList(t) {
          for (var _e = 0; _e < t.length; _e++) {
            this.element.classList.add(t[_e]);
          }
        }
      }, {
        key: "removeClassList",
        value: function removeClassList(t) {
          for (var _e2 = 0; _e2 < t.length; _e2++) {
            this.element.classList.remove(t[_e2]);
          }
        }
      }, {
        key: "attributes",
        get: function get() {
          return { trigger: i + "-trigger", condition: i + "-condition", success: { class: i + "-success-class" }, failure: { class: i + "-failure-class" } };
        }
      }, {
        key: "classes",
        get: function get() {
          return { success: this.getClassList("success"), failure: this.getClassList("failure") };
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[" + i + "]";
        }
      }]);

      return r;
    }();
  } }));