"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (e, t) {
  for (var n in t) {
    e[n] = t[n];
  }
}(exports, function (e) {
  var t = {};function n(r) {
    if (t[r]) return t[r].exports;var s = t[r] = { i: r, l: !1, exports: {} };return e[r].call(s.exports, s, s.exports, n), s.l = !0, s.exports;
  }return n.m = e, n.c = t, n.d = function (e, t, r) {
    n.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: r });
  }, n.r = function (e) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(e, "__esModule", { value: !0 });
  }, n.t = function (e, t) {
    if (1 & t && (e = n(e)), 8 & t) return e;if (4 & t && "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e && e.__esModule) return e;var r = Object.create(null);if (n.r(r), Object.defineProperty(r, "default", { enumerable: !0, value: e }), 2 & t && "string" != typeof e) for (var s in e) {
      n.d(r, s, function (t) {
        return e[t];
      }.bind(null, s));
    }return r;
  }, n.n = function (e) {
    var t = e && e.__esModule ? function () {
      return e.default;
    } : function () {
      return e;
    };return n.d(t, "a", t), t;
  }, n.o = function (e, t) {
    return Object.prototype.hasOwnProperty.call(e, t);
  }, n.p = "", n(n.s = 236);
}({ 0: function _(e, t) {
    e.exports = function () {
      function _class() {
        _classCallCheck(this, _class);
      }

      _createClass(_class, null, [{
        key: "triggerEvent",
        value: function triggerEvent(e, t) {
          var n = void 0;"function" == typeof Event ? n = new Event(t) : (n = document.createEvent("Event"), n.initEvent(t, !0, !0)), e.dispatchEvent(n);
        }
      }, {
        key: "getElementByAttribute",
        value: function getElementByAttribute(e, t) {
          return t ? document.querySelector("[" + e + "=\"" + t + "\"]") : document.querySelector("[" + e + "]");
        }
      }, {
        key: "getElementsByAttribute",
        value: function getElementsByAttribute(e, t) {
          return t ? document.querySelectorAll("[" + e + "=\"" + t + "\"]") : document.querySelectorAll("[" + e + "]");
        }
      }, {
        key: "getElementByAttributeWithinElement",
        value: function getElementByAttributeWithinElement(e, t, n) {
          return n ? e.querySelector("[" + t + "=\"" + n + "\"]") : e.querySelector("[" + t + "]");
        }
      }, {
        key: "isMouseOver",
        value: function isMouseOver(e) {
          return Array.prototype.slice.call(e.parentElement.querySelectorAll(":hover")).filter(function () {
            return e[0] == this;
          }).length > 0;
        }
      }, {
        key: "getElementsByAttributeWithinElement",
        value: function getElementsByAttributeWithinElement(e, t, n) {
          return n ? e.querySelectorAll("[" + t + "=\"" + n + "\"]") : e.querySelectorAll("[" + t + "]");
        }
      }, {
        key: "getElementByClassWithinElement",
        value: function getElementByClassWithinElement(e, t) {
          return e.querySelector("." + t);
        }
      }, {
        key: "maxWidthMobileViewports",
        value: function maxWidthMobileViewports() {
          return 940;
        }
      }, {
        key: "createEvent",
        value: function createEvent(e) {
          var t = void 0;return "function" == typeof Event ? t = new Event(e) : (t = document.createEvent("Event"), t.initEvent(e, !0, !0)), t;
        }
      }, {
        key: "isDesktop",
        value: function isDesktop() {
          return Math.max(document.documentElement.clientWidth, window.innerWidth || 0) > this.maxWidthMobileViewports();
        }
      }, {
        key: "isIE11",
        value: function isIE11() {
          return -1 !== navigator.userAgent.indexOf("MSIE") || navigator.appVersion.indexOf("Trident/") > -1;
        }
      }, {
        key: "getKeyCodeOnKeyDownEvent",
        value: function getKeyCodeOnKeyDownEvent(e) {
          var t = void 0;if (null != e) if (void 0 === e.code) switch (e.keyCode) {case 13:
              t = "Enter";break;case 38:
              t = "ArrowUp";break;case 40:
              t = "ArrowDown";break;case 9:
              t = "Tab";break;case 27:
              t = "Escape";break;case 33:
              t = "PageUp";break;case 34:
              t = "PageDown";break;default:
              t = void 0;} else t = e.code;return t;
        }
      }]);

      return _class;
    }();
  }, 236: function _(e, t, n) {
    e.exports = n(74);
  }, 74: function _(e, t, n) {
    "use strict";
    n.r(t), n.d(t, "Closable", function () {
      return i;
    });var r = n(0),
        s = n.n(r);
    var i = function () {
      function i(e) {
        _classCallCheck(this, i);

        this.element = e, this.elements = s.a.getElementsByAttribute(this.element.getAttribute("data-rs-closable")), this.addEventHandlers();
      }

      _createClass(i, [{
        key: "addEventHandlers",
        value: function addEventHandlers() {
          var _this = this;

          this.element.addEventListener("click", function (e) {
            e.preventDefault(), _this.elements.length > 0 && _this.elements.forEach(function (e) {
              e.scrollHeight > 0 && !e.classList.contains(_this.classes.closableClosed) && (e.style.maxHeight = e.scrollHeight + "px"), e.classList.add(_this.classes.closableClosed), e.style.maxHeight = "0px";
            });
          }), window.addEventListener("load", function () {
            _this.setMaxHeightElements();
          }), window.addEventListener("resize", function () {
            _this.setMaxHeightElements();
          });
        }
      }, {
        key: "setMaxHeightElements",
        value: function setMaxHeightElements() {
          var _this2 = this;

          this.elements.length > 0 && this.elements.forEach(function (e) {
            e.scrollHeight > 0 && !e.classList.contains(_this2.classes.closableClosed) && (e.style.maxHeight = "none", e.style.maxHeight = e.scrollHeight + "px");
          });
        }
      }, {
        key: "classes",
        get: function get() {
          return { closableClosed: "closable--closed" };
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[data-rs-closable]";
        }
      }]);

      return i;
    }();
  } }));