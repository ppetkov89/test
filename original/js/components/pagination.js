"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (t, e) {
  for (var i in e) {
    t[i] = e[i];
  }
}(exports, function (t) {
  var e = {};function i(n) {
    if (e[n]) return e[n].exports;var s = e[n] = { i: n, l: !1, exports: {} };return t[n].call(s.exports, s, s.exports, i), s.l = !0, s.exports;
  }return i.m = t, i.c = e, i.d = function (t, e, n) {
    i.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: n });
  }, i.r = function (t) {
    "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 });
  }, i.t = function (t, e) {
    if (1 & e && (t = i(t)), 8 & e) return t;if (4 & e && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t && t.__esModule) return t;var n = Object.create(null);if (i.r(n), Object.defineProperty(n, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t) for (var s in t) {
      i.d(n, s, function (e) {
        return t[e];
      }.bind(null, s));
    }return n;
  }, i.n = function (t) {
    var e = t && t.__esModule ? function () {
      return t.default;
    } : function () {
      return t;
    };return i.d(e, "a", e), e;
  }, i.o = function (t, e) {
    return Object.prototype.hasOwnProperty.call(t, e);
  }, i.p = "", i(i.s = 265);
}({ 265: function _(t, e, i) {
    t.exports = i(82);
  }, 82: function _(t, e, i) {
    "use strict";
    i.r(e), i.d(e, "Pagination", function () {
      return n;
    });
    var n = function () {
      function n(t) {
        _classCallCheck(this, n);

        this.element = t, this.paginationList = this.element.querySelector("[" + this.attributes.paginationList + "]"), this.listItems = [].concat(_toConsumableArray(this.paginationList.getElementsByTagName("li"))), this.handleNextPreviousElements(), this.handleListItems();
      }

      _createClass(n, [{
        key: "handleListItems",
        value: function handleListItems() {
          var t = !1;this.listItems.length > 0 && (this.listItems[0].innerText.trim().length > 2 || this.listItems[this.listItems.length - 1].innerText.trim().length > 2) && (t = !0), t && this.paginationList.classList.add(this.classes.paginationListSmall);
        }
      }, {
        key: "handleNextPreviousElements",
        value: function handleNextPreviousElements() {
          var t = this.listItems[0];"undefined" !== t.getAttribute(this.attributes.paginationControl) && null !== t.getAttribute(this.attributes.paginationControl) && this.listItems.shift();var e = this.listItems[this.listItems.length - 1];"undefined" !== e.getAttribute(this.attributes.paginationControl) && null !== e.getAttribute(this.attributes.paginationControl) && this.listItems.pop();
        }
      }, {
        key: "attributes",
        get: function get() {
          return { paginationControl: "data-rs-pagination-control", paginationList: "data-rs-pagination-list" };
        }
      }, {
        key: "classes",
        get: function get() {
          return { paginationListSmall: "pagination__list--s" };
        }
      }], [{
        key: "getSelector",
        value: function getSelector() {
          return "[data-rs-pagination]";
        }
      }]);

      return n;
    }();
  } }));