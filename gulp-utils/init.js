const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const buildStyle = require('./utils/build-style');
const buildScripts = require('./utils/build-script');
const defaultConfig = require('../config.json');

module.exports = function (userConfig = {}, replace = false) {
  const config = replace ? userConfig : Object.assign({}, defaultConfig, userConfig)

  // build : icons
  gulp.task('build:icons', () => {
    return gulp.src(config.image.src + '/icons/**/*.svg')
      .pipe($.plumber())
      .pipe($.svgstore())
      .pipe(gulp.dest(config.image.build))
  });

  gulp.task("build:icon", () => {
    return gulp
      .src(config.image.src + "/*.svg")
      .pipe(gulp.dest(config.image.build));
  });

  // build : font
  gulp.task('build:font', () => {
    return gulp.src(config.fonts.src + '/**/*')
      .pipe($.plumber())
      .pipe(gulp.dest(config.fonts.build))
  });

  // build : css
  gulp.task('build:css', () => {
    const src = [config.css.src + "/*.scss"];

    return buildStyle(src, config.css.build);
  });

  // Build theme js.
  gulp.task('build:js', () => {
    const src = [config.js.src + "/**/*.js"];

    return buildScripts(src, config.js.build);
  });

	/**
	 * BUILD ALL
	 */
  gulp.task('build', gulp.series(gulp.parallel('build:js', 'build:css', 'build:icon', 'build:font')));

}
