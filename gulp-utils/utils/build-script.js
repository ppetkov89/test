const gulp = require("gulp")
const $ = require("gulp-load-plugins")()
const webpack = require("webpack-stream")
const named = require("vinyl-named")
const path = require("path")
const through2 = require("through2")

const fileStructure = []

module.exports = function(src, dest, configOverride = {}) {
	const defaultConfig = {
		mode: "production",
		// TODO: Add debug configuration.
		// optimization: {
		// 	minimize:false
		// },
	}

	// Assign new config object with overrides.
	config = Object.assign({}, defaultConfig, configOverride)

	return gulp
		.src(src)

		.pipe($.plumber())
		.pipe(
			through2.obj((file, etc, next) => {
				const fileName = path.basename(file.path, ".js")
				const filePath = path.relative(file.base, file.dirname)

				if (fileStructure.hasOwnProperty(fileName)) {
					console.error("There is two files with same. Fix this issue in order prevent file conflict!")
					console.error("path to file 1:", fileStructure[fileName])
					console.error("path to file 2:", filePath)

					return false
				}

				fileStructure[fileName] = filePath

				next(null, file)
			})
		)

		.pipe(named())
		.pipe(webpack(config))
		.pipe($.babel())
		.pipe(
			gulp.dest(function(file) {
				const fileName = path.basename(file.path, ".js")
				const dir = fileStructure[fileName] ? "/" + fileStructure[fileName] : ""
				return dest + dir
			})
		)
}
