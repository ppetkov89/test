const gulp = require('gulp');
const $ = require('gulp-load-plugins')();

module.exports = function (src, dest) {
  return gulp.src(src)
    .pipe($.plumber())
    .pipe($.sass().on('error', (error) => {
      console.log(error);
    }))
    .pipe($.autoprefixer({
      cascade: false
    }))
    .pipe($.csso())
    .pipe(gulp.dest(dest))
}
