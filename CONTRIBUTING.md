# Contributing
## Table of content
- [Requirements](#requirements)
- [Development](#development)
	- [Setup local development](#setup-local-development)
	- [Build assets](#build-assets)
	- [Link library with your app](#link-with-app)
- [Linting](#linting)
- [Pull request guideness](#pull-request-guideness)
- [Library guideness](#library-guideness)
	- [Overrides](#overrides)
		- [CSS](#css)
		- [Javascript](#javascript)
	- [New components](#new-components)
- [Gulp](#gulp)
	- [Setup gulp](#setup-gulp)
	- [Available utils](#available-utils)

## Requirements
- [Gulp](https://gulpjs.com/docs/en/getting-started/quick-start#install-the-gulp-command-line-utility)
- NPM Token for Orbit - Ask maintainers for it.
## Development
### Setup local development

```shell
git clone git@gitlab.workingpropeople.com:randstad-bluex/orbit.git

cd orbit
```
Create new file `.npmrc` and add inside
```
//registry.npmjs.org/:_authToken={TOKEN}
```
Ask maintainers of this library for `TOKEN` and then replace `{TOKEN}` with actual token.
```
npm install
```
### Build assets
```
npm start
```
### Link with app
#### Follow this guide to setup locally with your app
in `orbit`
```
npm link
```
in `app`
```
npm link @ffw/randstad-local-orbit
```

## Linting
## Pull request guideness
All PRs must: 

- have task id - If you are working on story
- have proper descriptive title

Good PR titles:

- RBD-12: Change title spacing.
- RBR-3: Change modal title font size
- Change navigation font size - If you are not working on story.

Bad PR titles:

- bug fixing
- change space
- RBD-12: fix

## Branching strategy
All branches should have the type and the story id in it.

Good branch names:
- feature/RBD-12
- hotfix/RBR-1
- nice-kebab-cased-titles - If you are not working on story.

Bad branch names:
- RBD-12
- feature-1
- featureFIX

## Library guideness

### Overrides
The main focus of this library is to extend functionality of `Orbit` library. Thats why overrides are big part of this library.

#### CSS
All files should follow `Orbit` file structure.
For example if you override `blocks` located in `03_molecules/_blocks.scss` must have same file structure

The proper way to override css is:
- Follow same folder structure as in `Orbit`.
- Use same file name as in `Orbit`.
- Use same class.
- Add JIRA ticker ID as line comment above.
- Import file in `human-forward`

#### Javascript
Folder structure:
- components - All components are in there
- libs - helper classes and functions
- overrides - All overrides of the original components are in there. 

Override steps.

Javascript should be override by extendting existing class.

- Create new file in `overrides`.
- Extend class.
- Replace path in `components`.

For example create in `overrides/auto-suggest.js` and export it.

```
export class AutoSuggest extends OrbitBase
```

Override the component path in `components/auto-suggest.js`

```
export * from "../overrides/auto-suggest";
```

### New components
All news components must with kebab case and have prefix `bluex`

Good names:

- bluex-modal
- bluex-job-list

Bad names:
- modal-big--form
- jobList
- BluexJobList

## Gulp
```
gulp build:icons
gulp build:css
gulp build:js

# Combine all above
gulp build
```
### Setup gulp.
To setup gulp for your app:

- Copy `example.config.json` and remove `example`
- Create new gulp file.
- Import `init.js` from `gulp-utils` and load it with `config.json` file

```
const gulp = require("gulp")
const buildScripts = require("./node_modules/@ffw/randstad-local-orbit/gulp-utils/utils/build-script") // path to your node_modules folder

const config = require("./config.json")
require("./gulp-utils/init")(config)

gulp.task("build:js", () => {

	const src = [config.js.src + "/**/*.js", "!" + config.js.src + "/overrides/*.js"]

	return buildScripts(src, config.js.build, webpackConfig)
})
```

#### Available utils.
They are located in `gulp-utils/utils`
* build-scripts - Process js files
* build-style - Process scss files





