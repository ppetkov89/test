'use strict';

var path = require('path');
var resolve = path.resolve;
var fs = require('fs');
var cp = require('child_process');
var format = require('util').format;

function Changelog() {
  this._target = resolve('CHANGELOG.md');
  this._cwd = process.cwd();
}

var proto = Changelog.prototype;

proto.gen = function (cb) {
  cb || (cb = () => {});

  var target = this._target;
  var self = this;
  cp.exec('git log --reverse --no-merges --pretty="format:- %s" --grep="RBD-"', (err, commits) => {
    if (err) return cb.call(self, err);

    var header = 'CHANGELOG';

    fs.writeFileSync(target, getContent(header, commits));
    cb.call(self, null, commits);
  });
};

function getContent(header, commits) {
  var template = '## %s\n\n%s';
  return format(template, header, commits);
}

new Changelog().gen(err => {
  if (err) return console.error(err.message)

  return console.log('Changelog was build successfully');

});
