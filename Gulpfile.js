const gulp = require("gulp")
const path = require("path")
const $ = require('gulp-load-plugins')();
const fs = require("fs")
const buildScripts = require("./gulp-utils/utils/build-script")
const pug = require('gulp-pug');

const config = require("./config.json")
const components = require("./js/components.json")

require("./gulp-utils/init")(config)

gulp.task("build:js", () => {
	// TODO: Implement as source array. After search refactor. To avoid export conflicts.
	// const files = getOverrides(components)

	const src = [config.js.src + "/**/*.js", "!" + config.js.src + "/overrides/*.js"]
	const webpackConfig = {
		output: {
			libraryTarget: "commonjs",
		},
	}

	return buildScripts(src, config.js.build, webpackConfig)
})

// Copy scss variables.
gulp.task("build:variables", () => {
	return gulp.src(["original/assets/scss/00_helpers/**/*"]).pipe(gulp.dest("build/scss-vars"))
})

gulp.task("build:original:scss", () => {
	const src = ["node_modules/@randstad-design/orbit/src/assets/scss/**/*"]
	return gulp.src(src).pipe(gulp.dest("original/scss"))
})

gulp.task("build:original:js", () => {
	const src = [
		"node_modules/@randstad-design/orbit/src/assets/js/components/**/*.js",
		"node_modules/@randstad-design/orbit/src/assets/js/helpers/element-helpers.js",
	]
	const webpackConfig = {
		output: {
			libraryTarget: "commonjs",
		},
	}

	return buildScripts(src, 'original/js/components', webpackConfig)
})

gulp.task('build:original:icons', () => {
	return gulp.src('node_modules/@randstad-design/orbit/src/assets/image/icons/**/*.svg')
		.pipe($.plumber())
		.pipe($.svgstore())
		.pipe(gulp.dest('original/image'))
});

// build : font
gulp.task('build:original:fonts', () => {
	return gulp.src('node_modules/@randstad-design/orbit/src/assets/font/**/*')
		.pipe($.plumber())
		.pipe(gulp.dest('original/fonts'))
});

gulp.task("build:original", gulp.series(gulp.parallel("build:original:js", "build:original:scss", "build:original:fonts", "build:original:icons")))
gulp.task("build", gulp.series(gulp.parallel("build:js", "build:css", "build:icons", "build:font", "build:variables")))

// -----------
// WATCH TASKS

gulp.task("watch", () => {
	gulp.watch(config.css.src + "/**/*.scss", gulp.series("build:css"))
})

gulp.task("start:playground", () => {
	gulp.watch("playground/*.pug", () => gulp.src('playground/*.pug')
	.pipe(pug())
	.pipe(gulp.dest('./playground/build/')))
})

// -----
// UTILS

// Replace original files with overrides.
function getOverrides(components) {
	orbit_paths = path.resolve("node_modules/@randstad-design/orbit/src/assets/js/components/")
	override_paths = path.resolve("js/overrides/")

	const orbit_files = fs
		.readdirSync(orbit_paths)
		.filter((file) => components.includes(file.replace(".js", "")))
		.reduce((obj, file) => {
			const name = file.replace(".js", "")
			obj[name] = path.join(orbit_paths, file)
			return obj
		}, {})

	const override_files = fs.readdirSync(override_paths).reduce((obj, file) => {
		const name = file.replace(".js", "")
		obj[name] = path.join(override_paths, file)
		return obj
	}, {})

	const files = {
		...orbit_files,
		...override_files,
	}

	return Object.values(files)
}
